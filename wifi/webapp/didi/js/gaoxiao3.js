var bone = true;
function styleJs(){
	var pingKuan=$(".m_wrapper").width(),//m_wrapper是最外层div的类名
		bili = function(mun){
			var oScale = mun/750;	//750是psd宽度
			return oScale.toFixed(6);
		};
	
	//例子
	$("header h1").css({
		"height":pingKuan * bili(100),
		"lineHeight":pingKuan * bili(100)+'px'
	});
	$("header h1 a").css({
		"left":pingKuan * bili(11),
		"top":pingKuan * bili(15)
	});
	$("header h1 a img").css({
		"width":pingKuan * bili(58),
		"height":pingKuan * bili(58)
	});
	$(".box").css({
		"width":pingKuan * bili(730)
	});
	$(".video").css({
		"width":pingKuan * bili(730),
		"height":pingKuan * bili(384)
	});
	$(".video video").css({
		"width":pingKuan * bili(730),
		"height":pingKuan * bili(384)
	});
	$(".box1").css({
		"width":pingKuan * bili(730),
		"paddingTop":pingKuan * bili(7),
		"paddingLeft":pingKuan * bili(17),
		"paddingRight":pingKuan * bili(17)
	});
	$(".box1 p").css({
		"width":pingKuan * bili(730),
		"height":pingKuan * bili(54),
		"lineHeight":pingKuan * bili(54)+'px'
	});
	$(".box1 p span").css({
		"paddingRight":pingKuan * bili(40)
	});
	$(".box1 p span img").css({
		"width":pingKuan * bili(100),
		"height":pingKuan * bili(20)
	});
	$(".box2").css({
		"width":pingKuan * bili(730),
		"height":pingKuan * bili(102),
		"marginBottom":pingKuan * bili(12)
	});
	$(".box2 .ul1").css({
		"width":pingKuan * bili(730),
		"height":pingKuan * bili(102)
	});
	$(".box2 .ul1 li").css({
		"width":pingKuan * bili(242),
		"height":pingKuan * bili(102),
		"lineHeight":pingKuan * bili(102)+'px'
	});
	$(".box2 .ul1 li img").css({
		"width":pingKuan * bili(40),
		"height":pingKuan * bili(40),
		"marginRight":pingKuan * bili(22)
	});
	$(".box3").css({
		"width":pingKuan * bili(730),
		"paddingBottom":pingKuan * bili(10)
	});
	$(".box3 h2").css({
		"width":pingKuan * bili(730),
		"height":pingKuan * bili(50),
		"marginLeft":pingKuan * bili(14),
		"textIndent":pingKuan * bili(15),
		"lineHeight":pingKuan * bili(50)+'px'
	});
	$(".box3 ul li").css({
		"width":pingKuan * bili(225),
		"paddingLeft":pingKuan * bili(14),
		"paddingBottom":pingKuan * bili(14)
	});
	$(".box3 ul li img").css({
		"width":pingKuan * bili(225),
		"height":pingKuan * bili(128)
	});
	$(".box3 ul li p").css({
		"width":pingKuan * bili(225),
		"height":pingKuan * bili(50),
		"lineHeight":pingKuan * bili(50)+'px'
	});
	
	
	
	
	
	
	
	
	//样式重置完成之后
	$(".load").hide();
	$(".m_wrapper").css({
		"opacity": 1
	});
	
};


function loaded(){
	setTimeout(function(){styleJs();},300);
}
function hengshuping(){
	if(window.orientation==180||window.orientation==0){loaded();};
	if(window.orientation==90||window.orientation==-90){loaded();}
}
window.addEventListener("onorientationchange" in window ? "orientationchange" : "resize", hengshuping, false);
window.addEventListener('load', loaded, false);