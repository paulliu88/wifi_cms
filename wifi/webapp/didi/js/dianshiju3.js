var bone = true;
function styleJs(){
	var pingKuan=$(".m_wrapper").width(),//m_wrapper是最外层div的类名
		bili = function(mun){
			var oScale = mun/750;	//750是psd宽度
			return oScale.toFixed(6);
		};
	
	//例子
	$("h1").css({
		"height":pingKuan * bili(100),
		"lineHeight":pingKuan * bili(100)+'px'
	});
	$("h1 a").css({
		"left":pingKuan * bili(11),
		"top":pingKuan * bili(15)
	});
	$("h1 a img").css({
		"width":pingKuan * bili(58),
		"height":pingKuan * bili(58)
	});
	$(".box").css({
		"paddingTop":pingKuan * bili(12),
		"paddingRight":pingKuan * bili(13),
		"paddingLeft":pingKuan * bili(10)
	});
	$(".box1").css({
		"paddingBottom":pingKuan * bili(12),
		"paddingRight":pingKuan * bili(10),
		"paddingLeft":pingKuan * bili(10)
	});
	$(".box1_left").css({
		"paddingRight":pingKuan * bili(22),
		"width":pingKuan * bili(200)
	});
	$(".box1_left img").css({
		"paddingTop":pingKuan * bili(12),
		"height":pingKuan * bili(260),
		"width":pingKuan * bili(200)
	});
	$(".box1_right").css({
		"paddingTop":pingKuan * bili(3),
		"height":pingKuan * bili(260),
		"width":pingKuan * bili(485)
	});
	$(".box1_right p").css({
		"height":pingKuan * bili(54),
		"width":pingKuan * bili(485),
		"lineHeight":pingKuan * bili(54)+'px'
	});
	$(".box1_right p span").css({
		"marginRight":pingKuan * bili(8)
	});
	$(".box1_right p img").css({
		"width":pingKuan * bili(100),
		"height":pingKuan * bili(20),
	});
	$(".box1_right p b").css({
		"width":pingKuan * bili(116),
		"height":pingKuan * bili(30),
		"lineHeight":pingKuan * bili(32)+'px'
	}); 
	$(".box2").css({
		"height":pingKuan * bili(102)
	});
	$(".box2 .ul1 li").css({
		"width":pingKuan * bili(242),
		"height":pingKuan * bili(102),
		"lineHeight":pingKuan * bili(102)+'px'
	});
	$(".box2 .ul1 li img").css({
		"width":pingKuan * bili(40),
		"height":pingKuan * bili(40),
		"marginRight":pingKuan * bili(22)
	});
	$(".box3").css({
		"paddingLeft":pingKuan * bili(14),
		"marginBottom":pingKuan * bili(12)
	});
	$(".box3 h2").css({
		"textIndent":pingKuan * bili(15),
		"marginBottom":pingKuan * bili(12),
		"lineHeight":pingKuan * bili(50)+'px',
		"paddingBottom":pingKuan * bili(8)
	});
	$(".box3 p").css({
		"paddingLeft":pingKuan * bili(15),
		"lineHeight":pingKuan * bili(40)+'px',
		"paddingBottom":pingKuan * bili(15)
	});
	$(".box3 p a").css({
		"width":pingKuan * bili(50),
		"height":pingKuan * bili(28),
		"top":pingKuan * bili(5)
	});
	$(".box4").css({
		"paddingLeft":pingKuan * bili(14)
	});
	$(".box4 h2").css({
		"textIndent":pingKuan * bili(15),
		"height":pingKuan * bili(58),
		"lineHeight":pingKuan * bili(58)+'px'
	});
	$(".box4 .ul1 li a").css({
		"marginLeft":pingKuan * bili(20),
		"marginRight":pingKuan * bili(20),
		"lineHeight":pingKuan * bili(40)+'px'
	});
	$(".box4 .con").css({
		"paddingLeft":pingKuan * bili(10),
		"paddingTop":pingKuan * bili(15)
	});
	$(".box4 .con li").css({
		"width":pingKuan * bili(64),
		"height":pingKuan * bili(46),
		"marginTop":pingKuan * bili(10),
		"marginRight":pingKuan * bili(34),
		"marginBottom":pingKuan * bili(20),
		"lineHeight":pingKuan * bili(46)+'px'
	});
	$(".box4 .con li:nth-child(7n)").css({
		"marginRight":pingKuan * bili(0)
	});
	$(".box4 .con li a").css({
		"width":pingKuan * bili(46),
		"height":pingKuan * bili(64)
	});
	$(".rdtj").css({
		"width":pingKuan * bili(730)
	});
	$(".tong").css({
		"marginBottom":pingKuan * bili(0)
	});
	$(".rdtj h2").css({
		"lineHeight":pingKuan * bili(50)+'px',
		"paddingLeft":pingKuan * bili(14),
		"paddingBottom":pingKuan * bili(8),
		"textIndent":pingKuan * bili(15)
	});
	$(".rdtj ul").css({
		"paddingLeft":pingKuan * bili(8),
		"width":pingKuan * bili(725)
	});
	$(".rdtj ul li").css({
		"paddingBottom":pingKuan * bili(20),
		"paddingRight":pingKuan * bili(2),
		"width":pingKuan * bili(170)
	});
	$(".rdtj ul li img").css({
		"width":pingKuan * bili(170),
		"height":pingKuan * bili(220)
	});
	$(".rdtj ul li h3").css({
		"lineHeight":pingKuan * bili(34)+'px',
		"paddingTop":pingKuan * bili(13)
	});
	$(".rdtj ul li p").css({
		"lineHeight":pingKuan * bili(34)+'px'
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//样式重置完成之后
	$(".load").hide();
	$(".m_wrapper").css({
		"opacity": 1
	});
	
	
	//字体超出隐藏
	var nNum = $('#p').html();
	var bBtn = true;
	
	xianzhi($('#p'),50);
	function xianzhi(obj,Max){
		var num = nNum;
		if( num.length > Max ){
			obj.html(num.substr(0,Max)+'...').append($('<a class="a1" href="javascript:;"></a>'));
		}else{
			obj.html(nNum).find('a').addClass('a2');	
		};
		
		$(".box3 p a").css({
			"width":pingKuan * bili(50),
			"height":pingKuan * bili(28),
			"top":pingKuan * bili(5)
		});
		
	};
	
	$('#p').live('click',function(){
		if(bBtn){
			xianzhi($('#p'),500);	
		}else{
			xianzhi($('#p'),50);
		};
		bBtn = !bBtn;
	});
	
};


function loaded(){
	setTimeout(function(){styleJs();},300);
}
function hengshuping(){
	if(window.orientation==180||window.orientation==0){loaded();};
	if(window.orientation==90||window.orientation==-90){loaded();}
}
window.addEventListener("onorientationchange" in window ? "orientationchange" : "resize", hengshuping, false);
window.addEventListener('load', loaded, false);