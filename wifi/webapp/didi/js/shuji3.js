var bone = true;
function styleJs(){
	var pingKuan=$(".m_wrapper").width(),//m_wrapper是最外层div的类名
		bili = function(mun){
			var oScale = mun/750;	//750是psd宽度
			return oScale.toFixed(6);
		};
	
	//例子
	$("header h1").css({
		"height":pingKuan * bili(100),
		"lineHeight":pingKuan * bili(100)+'px'
	});
	$("header h1 a").css({
		"left":pingKuan * bili(11),
		"top":pingKuan * bili(15)
	});
	$("header h1 a img").css({
		"width":pingKuan * bili(58),
		"height":pingKuan * bili(58)
	});
	$(".box").css({
		"paddingTop":pingKuan * bili(12),
		"paddingRight":pingKuan * bili(13),
		"paddingLeft":pingKuan * bili(13)
	});
	$(".box1").css({
		"paddingBottom":pingKuan * bili(12),
		"paddingRight":pingKuan * bili(10),
		"paddingLeft":pingKuan * bili(10)
	});
	$(".box1_left").css({
		"width":pingKuan * bili(200),
		"paddingRight":pingKuan * bili(15)
	});
	$(".box1_left img").css({
		"width":pingKuan * bili(200),
		"height":pingKuan * bili(260),
		"paddingTop":pingKuan * bili(12)
	});
	$(".box1_right").css({
		"width":pingKuan * bili(485),
		"height":pingKuan * bili(260),
		"paddingTop":pingKuan * bili(3)
	});
	$(".box1_right p").css({
		"width":pingKuan * bili(485),
		"height":pingKuan * bili(54),
		"lineHeight":pingKuan * bili(54)+'px'
	});
	$(".box1_right p span").css({
		"marginRight":pingKuan * bili(5)
	});
	$(".box1_right p img").css({
		"width":pingKuan * bili(100),
		"height":pingKuan * bili(20),
	});
	$(".box1_right p b").css({
		"width":pingKuan * bili(116),
		"height":pingKuan * bili(30),
		"lineHeight":pingKuan * bili(32)+'px'
	});
	$(".box2").css({
		"width":pingKuan * bili(730),
		"height":pingKuan * bili(102),
		"marginBottom":pingKuan * bili(12)
	});
	$(".box2 .ul1").css({
		"width":pingKuan * bili(730),
		"height":pingKuan * bili(102)
	});
	$(".box2 .ul1 li").css({
		"width":pingKuan * bili(242),
		"height":pingKuan * bili(102),
		"lineHeight":pingKuan * bili(102)+'px'
	});
	$(".box2 .ul1 li img").css({
		"width":pingKuan * bili(40),
		"height":pingKuan * bili(40),
		"marginRight":pingKuan * bili(22)
	});
	$(".box3").css({
		"paddingLeft":pingKuan * bili(14),
		"marginBottom":pingKuan * bili(12)
	});
	$(".box3 h2").css({
		"textIndent":pingKuan * bili(15),
		"paddingBottom":pingKuan * bili(8),
		"lineHeight":pingKuan * bili(50)+'px'
	});
	$(".box3 p").css({
		"paddingBottom":pingKuan * bili(15),
		"paddingLeft":pingKuan * bili(15),
		"lineHeight":pingKuan * bili(40)+'px'
	});
	$(".box3 p a").css({
		"width":pingKuan * bili(50),
		"height":pingKuan * bili(28),
		"top":pingKuan * bili(5)
	});
	$(".box4").css({
		"paddingLeft":pingKuan * bili(14),
		"marginBottom":pingKuan * bili(12)
	});
	$(".box4 h2").css({
		"paddingBottom":pingKuan * bili(8),
		"textIndent":pingKuan * bili(15),
		"lineHeight":pingKuan * bili(50)+'px'
	});
	$(".box4 .ul1 li a").css({
		"marginLeft":pingKuan * bili(20),
		"marginRight":pingKuan * bili(20),
		"height":pingKuan * bili(40),
		"lineHeight":pingKuan * bili(40)+'px'
	});
	$(".con").css({
		"paddingTop":pingKuan * bili(15),
		"height":pingKuan * bili(250)
	});
	$(".con li").css({
		"lineHeight":pingKuan * bili(50)+'px',
		"height":pingKuan * bili(50)
	});
	$(".con li span").css({
		"paddingLeft":pingKuan * bili(18)
	});
	$(".con li time").css({
		"paddingRight":pingKuan * bili(18)
	});
	$(".box4 p").css({
		"lineHeight":pingKuan * bili(40)+'px',
		"height":pingKuan * bili(40)
	});
	$(".box5").css({
		"paddingLeft":pingKuan * bili(14),
		"marginBottom":pingKuan * bili(12)
	});
	$(".box5 h2").css({
		"paddingBottom":pingKuan * bili(8),
		"textIndent":pingKuan * bili(15),
		"lineHeight":pingKuan * bili(50)+'px'
	});
	$(".ul li").css({
		"paddingBottom":pingKuan * bili(20),
		"marginBottom":pingKuan * bili(10)
	});
	$(".ul li img").css({
		"width":pingKuan * bili(168),
		"height":pingKuan * bili(220),
		"paddingRight":pingKuan * bili(23),
		"paddingTop":pingKuan * bili(18)
	});
	$(".ul li .right").css({
		"width":pingKuan * bili(496)
	});
	$(".ul li h3").css({
		"lineHeight":pingKuan * bili(58)+'px'
	});
	$(".ul li h3 span").css({
		"marginTop":pingKuan * bili(12),
		"width":pingKuan * bili(116),
		"height":pingKuan * bili(30),
		"marginRight":pingKuan * bili(40),
		"lineHeight":pingKuan * bili(30)+'px'
	});
	$(".ul li .an").css({
		"lineHeight":pingKuan * bili(34)+'px'
	});
	$(".ul li p").css({
		"lineHeight":pingKuan * bili(34)+'px',
		"paddingBottom":pingKuan * bili(10)
	});
	
	
	
	
	
	
	
	
	
	//样式重置完成之后
	$(".load").hide();
	$(".m_wrapper").css({
		"opacity": 1
	});
	
	
	
	//字体超出隐藏
	var nNum = $('#p').html();
	var bBtn = true;
	
	xianzhi($('#p'),50);
	function xianzhi(obj,Max){
		var num = nNum;
		if( num.length > Max ){
			obj.html(num.substr(0,Max)+'...').append($('<a class="a1" href="javascript:;"></a>'));
		}else{
			obj.html(nNum).find('a').addClass('a2');	
		};
		
		$(".box3 p a").css({
			"width":pingKuan * bili(50),
			"height":pingKuan * bili(28),
			"top":pingKuan * bili(5)
		});
	};
	
	$('#p').live('click',function(){
		if(bBtn){
			xianzhi($('#p'),500);	
		}else{
			xianzhi($('#p'),50);
		};
		bBtn = !bBtn;
	});
	
	
	var onOff = true;
	var allHeight = $('.con li').height()*$('.con li').length;
	
	$('#bo a').click(function(){
		var obj = $('.con');
		if( onOff ){
			obj.animate({'height':allHeight},function(){
				$('#bo a').removeClass('a1').addClass('a2');	
			});
		}else{
			obj.animate({'height':$('.con li').height()*5},function(){
				$('#bo a').removeClass('a2').addClass('a1');	
			});
		};
		onOff = !onOff;
	});
	
};


function loaded(){
	setTimeout(function(){styleJs();},300);
}
function hengshuping(){
	if(window.orientation==180||window.orientation==0){loaded();};
	if(window.orientation==90||window.orientation==-90){loaded();}
}
window.addEventListener("onorientationchange" in window ? "orientationchange" : "resize", hengshuping, false);
window.addEventListener('load', loaded, false);