﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR
		.addTemplates(
				'default',
				{
					imagesPath : CKEDITOR.getUrl(CKEDITOR.plugins
							.getPath('templates')
							+ 'templates/images/'),
					templates : 
						   [
								{
									title:'banner图和导航',
									image:'template1.gif',
									description:'顶部',
									html:'<div class="t_top"><div class="banner"><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/banner.jpg" alt="" /></a></div><div class="nav"><img src="/r/cms/tz/tz/images/NavBg.jpg" alt="" /><ul><li><a href="javascript:;" target="_blank">联想简介</a></li><li><a href="javascript:;" target="_blank">联想动态</a></li><li><a href="javascript:;" target="_blank">IBM简介</a></li><li><a href="javascript:;" target="_blank">IBM动态</a></li><li><a href="javascript:;" target="_blank">现场直击</a></li></ul></div></div>'
								}
								,
								{
									title:'轮播图',
									image:'template2.gif',
									description:'轮播图',
									html:'<div style="height:20px;"></div><div class="co3"><h2><img src="/r/cms/tz/tz/images/top.gif" /><span>现场直击</span></h2><div id="scroll_A"><div class="inner"><ul class="list"><li class="split"><a href="javascript:;"><img src="/r/cms/tz/tz/images/lunbo.jpg" alt="" /></a></li><li class="split"><a href="javascript:;"><img src="/r/cms/tz/tz/images/11.jpg" alt="" /></a></li><li class="split"><a href="javascript:;"><img src="/r/cms/tz/tz/images/112.jpg" alt="" /></a></li><li class="split"><a href="javascript:;"><img src="/r/cms/tz/tz/images/236.jpg" alt="" /></a></li></ul></div><img src="/r/cms/tz/tz/images/prev.gif" class="preNext prev"/><img src="/r/cms/tz/tz/images/next.gif" class="preNext next"/></div></div>'
								}
								,
								{
									title:'左中右布局',
									image:'template3.gif',
									description:'左中右布局',
									html:'<div style="height:20px;"></div><div class="pSimilar"><div class="left" id="focus"><ul class="focus_list"><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/111111.jpg" alt="" /></a></li><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/222222.jpg" alt="" /></a></li><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/333333.jpg" alt="" /></a></li><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/444444.jpg" alt="" /></a></li><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/555555.jpg" alt="" /></a></li></ul><div class="btn focus_dot"><span class="123123"><a class="active" href="javascript:;">1</a><a href="javascript:;">2</a><a href="javascript:;">3</a><a href="javascript:;">4</a><a href="javascript:;">5</a></span></div></div><div class="center"><h2><img src="/r/cms/tz/tz/images/top.gif" alt="" /><span>作者按</span></h2><div class="tbody"><h3><a href="javascript:;" target="_blank">联想正式宣布收购IBM x86服务器业务！</a></h3><p>2014年1月23日下午，在北京召开的联想新春联欢会上，正式宣布以23亿美元的价格收购IBM x86服务器业务。业内盛传已久的联想收购IBM出售低端于已久的联想收购IBM出售低端于已久的联想收购IBM出售低端于落下帷幕。交易完成后有7500名员工加入，X86服务器排名由第六上升到第三，营业额增加50亿美元。收购后，联想集团将主营PC、移动设备和企业设备三大业务…<a href="javascript:;">【详细】</a></p></div></div><div class="right"><h2><img src="/r/cms/tz/tz/images/top.gif" alt="" /><span>停牌收购</span></h2><div class="con"><a href="javascript:;" class="a" target="_blank"><img src="/r/cms/tz/tz/images/236.jpg" alt="" /></a><p>2014年1月23日早间，在港上市的联想集团发布停牌公告…<a href="javascript:;" target="_blank">【全文】</a></p></div></div></div>'
								}
								,
								{
									title:'1+4布局 ',
									image:'template4.gif',
									description:'1+4布局',
									html:'<div style="height:20px;"></div><div class="onefour"><h2><img src="/r/cms/tz/tz/images/top.gif" alt="" /><span>联想集团动态</span></h2><div class="con"><div class="left"><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/11.jpg" alt="" /></a></div><div class="right"><ul><li><h3><a href="javascript:;" target="_blank">联想ThinkServer双路机架式服务器</a></h3><div class="bot"><div class="bot_left"><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/112.jpg" alt="" /></a></div><p>联想ThinkServer 双路机架式服务器新品，助力客户成功应对全新挑战和机遇</p></div></li><li><h3><a href="javascript:;" target="_blank">联想ThinkServer双路机架式服务器</a></h3><div class="bot"><div class="bot_left"><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/112.jpg" alt="" /></a></div><p>联想ThinkServer 双路机架式服务器新品，助力客户成功应对全新挑战和机遇</p></div></li><li><h3><a href="javascript:;" target="_blank">联想ThinkServer双路机架式服务器</a></h3><div class="bot"><div class="bot_left"><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/112.jpg" alt="" /></a></div><p>联想ThinkServer 双路机架式服务器新品，助力客户成功应对全新挑战和机遇</p></div></li><li><h3><a href="javascript:;" target="_blank">联想ThinkServer双路机架式服务器</a></h3><div class="bot"><div class="bot_left"><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/112.jpg" alt="" /></a></div><p>联想ThinkServer 双路机架式服务器新品，助力客户成功应对全新挑战和机遇</p></div></li></ul></div></div></div>'
								}
								,
								{
									title:'1+6布局',
									image:'template5.gif',
									description:'1+6布局',
									html:'<div style="height:20px;"></div><div class="onesix"><h2><img src="/r/cms/tz/tz/images/top.gif" alt="" /><span>演讲嘉宾</span></h2><div class="con"><div class="left"><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/1.jpg" alt="" /></a><h3><a href="javascript:;" target="_blank">戴尔创始人、首席执行官：迈克尔·戴尔</a></h3><p>迈克尔·戴尔先生是戴尔公司董事会主席兼首席执行官。1984年，19岁的迈克尔用1,000美元创建戴尔公司。1992年，迈克尔成为有史以来最年轻的财富500强公司CEO……<a href="javascript:;" target="_blank">【全文】</a></p></div><div class="right"><ul><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/3.jpg" alt="" /></a><p><a href="javascript:;" target="_blank">戴尔公司总裁、首席商务官史蒂芬·J·菲利斯</a></p></li><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/3.jpg" alt="" /></a><p><a href="javascript:;" target="_blank">戴尔公司总裁、首席商务官史蒂芬·J·菲利斯</a></p></li><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/3.jpg" alt="" /></a><p><a href="javascript:;" target="_blank">戴尔公司总裁、首席商务官史蒂芬·J·菲利斯</a></p></li><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/3.jpg" alt="" /></a><p><a href="javascript:;" target="_blank">戴尔公司总裁、首席商务官史蒂芬·J·菲利斯</a></p></li><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/3.jpg" alt="" /></a><p><a href="javascript:;" target="_blank">戴尔公司总裁、首席商务官史蒂芬·J·菲利斯</a></p></li><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/3.jpg" alt="" /></a><p><a href="javascript:;" target="_blank">戴尔公司总裁、首席商务官史蒂芬·J·菲利斯</a></p></li></ul></div></div></div>'
								}
								,
								{
									title:'平分4块布局 ',
									image:'template6.gif',
									description:'平分4块布局 ',
									html:'<div style="height:20px;"></div><div class="four"><h2><img src="/r/cms/tz/tz/images/top.gif" alt="" /><span>案例下载</span></h2><div class="con"><ul><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/al1.jpg" alt="" /></a><h3><a href="javascript:;" target="_blank">马自达案例</a></h3><p>为了帮助马自达北美分公司简化其IT基础架构的复杂性，戴尔以智能数据管理和虚拟化技术搭建了一个支持10Gb以太网内加速性能的分层存储架构……<a href="javascript:;" target="_blank">【全文】</a></p></li><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/al1.jpg" alt="" /</a><h3><a href="javascript:;" target="_blank">马自达案例</a></h3><p>为了帮助马自达北美分公司简化其IT基础架构的复杂性，戴尔以智能数据管理和虚拟化技术搭建了一个支持10Gb以太网内加速性能的分层存储架构……<a href="javascript:;" target="_blank">【全文】</a></p></li><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/al1.jpg" alt="" /></a><h3><a href="javascript:;" target="_blank">马自达案例</a></h3><p>为了帮助马自达北美分公司简化其IT基础架构的复杂性，戴尔以智能数据管理和虚拟化技术搭建了一个支持10Gb以太网内加速性能的分层存储架构……<a href="javascript:;" target="_blank">【全文】</a></p></li><li><a href="javascript:;" target="_blank"><img src="/r/cms/tz/tz/images/al1.jpg" alt="" /></a><h3><a href="javascript:;" target="_blank">马自达案例</a></h3><p>为了帮助马自达北美分公司简化其IT基础架构的复杂性，戴尔以智能数据管理和虚拟化技术搭建了一个支持10Gb以太网内加速性能的分层存储架构……<a href="javascript:;" target="_blank">【全文】</a></p></li></ul></div></div>'
								}
							]
				});
