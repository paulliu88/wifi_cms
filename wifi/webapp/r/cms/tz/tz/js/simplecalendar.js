var calendar = {

    init: function() {

        /**
         * Get current date
         */
        var d = new Date();
        var strDate = d.getFullYear() + "/" + (d.getMonth() + 1) + "/" + d.getDate();

        /**
         * Get current month and set as '.current-month' in title
         */
        var monthNumber = d.getMonth() + 1;

        function GetMonthName(monthNumber) {
            var months = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
            return months[monthNumber - 1];
        }

        //$('.month').text(GetMonthName(monthNumber));


		//获得本月天数 30天-31天
		var daycount = DayNumOfMonth(d.getFullYear(),d.getMonth());
		
		//获得星期几，前面补几天
		
		var week = firstDayOfWeek(d.getFullYear(),d.getMonth());
		//补空前面的天数。
		$tableid = $("#idcal");
		
		days = 0;
		
		for(i=0;i<((daycount+week+3)/7);i++)
		{
			var tr = idcal.insertRow(i);
			for(j=0;j<7;j++)
			{
				
				var col = tr.insertCell(j);
				if(i==0&&j<week-1)
				{
					col.innerHTML = "&nbsp;";
				}
				else if(days>=daycount)
				{ 
					col.innerHTML = "&nbsp;";
				}
				else
				{
					days++;
					col.innerHTML = ""+days;
					$(col).attr("date-month",d.getMonth());
					$(col).attr("date-day",""+days);
				}
				
			}
			if(days>=daycount)break;
			
		}
		
		//画出所有天数



        /**
         * Get current day and set as '.current-day'
         */
        //$('tbody td[date-day="' + d.getDate() + '"]').addClass('current-day');
		if($("#dates").val()){
			var dates=$("#dates").val();
			var arr=dates.split(",");
			for ( var i = 0; i < arr.length; i++) {
				$("#idcal [date-day='"+arr[i]+"']").addClass("current-day");
			}
		};
        

        /**
         * Add '.event' class to all days that has an event
         */
        $('.day-event').each(function(i) {
            var eventMonth = $(this).attr('date-month');
            var eventDay = $(this).attr('date-day');
            $('tbody tr td[date-month="' + eventMonth + '"][date-day="' + eventDay + '"]').addClass('event');
        });



        /**
         * Save & Remove to/from personal list
         */
        $('.save').click(function() {
            if (this.checked) {
                $(this).next().text('Remove from personal list');
                var eventHtml = $(this).closest('.day-event').html();
                var eventMonth = $(this).closest('.day-event').attr('date-month');
                var eventDay = $(this).closest('.day-event').attr('date-day');
                var eventNumber = $(this).closest('.day-event').attr('data-number');
                $('.person-list').append('<div class="day" date-month="' + eventMonth + '" date-day="' + eventDay + '" data-number="' + eventNumber + '" style="display:none;">' + eventHtml + '</div>');
                $('.day[date-month="' + eventMonth + '"][date-day="' + eventDay + '"]').slideDown('fast');
                $('.day').find('.close').remove();
                $('.day').find('.save').removeClass('save').addClass('remove');
                $('.day').find('.remove').next().addClass('hidden-print');
                remove();
                sortlist();
            } else {
                $(this).next().text('Save to personal list');
                var eventMonth = $(this).closest('.day-event').attr('date-month');
                var eventDay = $(this).closest('.day-event').attr('date-day');
                var eventNumber = $(this).closest('.day-event').attr('data-number');
                $('.day[date-month="' + eventMonth + '"][date-day="' + eventDay + '"][data-number="' + eventNumber + '"]').slideUp('slow');
                setTimeout(function() {
                    $('.day[date-month="' + eventMonth + '"][date-day="' + eventDay + '"][data-number="' + eventNumber + '"]').remove();
                }, 1500);
            }
        });
		
		function DayNumOfMonth(Year,Month)
		{
			Month--;
			var d = new Date(Year,Month,1);
			d.setDate(d.getDate()+32-d.getDate());
			return (32-d.getDate());
		}
		function firstDayOfWeek(Year,Month)
		{
			var d = new Date(Year,Month,1);
			return (d.getDay());
		}

        function remove() {
            $('.remove').click(function() {
                if (this.checked) {
                    $(this).next().text('Remove from personal list');
                    var eventMonth = $(this).closest('.day').attr('date-month');
                    var eventDay = $(this).closest('.day').attr('date-day');
                    var eventNumber = $(this).closest('.day').attr('data-number');
                    $('.day[date-month="' + eventMonth + '"][date-day="' + eventDay + '"][data-number="' + eventNumber + '"]').slideUp('slow');
                    $('.day-event[date-month="' + eventMonth + '"][date-day="' + eventDay + '"][data-number="' + eventNumber + '"]').find('.save').attr('checked', false);
                    $('.day-event[date-month="' + eventMonth + '"][date-day="' + eventDay + '"][data-number="' + eventNumber + '"]').find('span').text('Save to personal list');
                    setTimeout(function() {
                        $('.day[date-month="' + eventMonth + '"][date-day="' + eventDay + '"][data-number="' + eventNumber + '"]').remove();
                    }, 1500);
                }
            });
			
			
        }
    }
};

$(document).ready(function() {

    calendar.init();

});

