var bone = true;
function styleJs(){
	var pingKuan=$(".m_wrapper").width(),//m_wrapper是最外层div的类名
		bili = function(mun){
			var oScale = mun/750;	//750是psd宽度
			return oScale.toFixed(6);
		};
	
	//例子
	$("header h1").css({
		"height":pingKuan * bili(100),
		"lineHeight":pingKuan * bili(100)+'px'
	});
	$("header h1 a").css({
		"left":pingKuan * bili(11),
		"top":pingKuan * bili(15)
	});
	$("header h1 a img").css({
		"width":pingKuan * bili(58),
		"height":pingKuan * bili(58)
	});
	$(".box").css({
		"paddingTop":pingKuan * bili(12),
		"paddingRight":pingKuan * bili(13),
		"paddingLeft":pingKuan * bili(10)
	});
	$(".box1").css({
		"paddingTop":pingKuan * bili(8),
		"paddingBottom":pingKuan * bili(22),
		"marginBottom":pingKuan * bili(12),
		"paddingLeft":pingKuan * bili(32)
	});
	$(".box1").css({
		"paddingTop":pingKuan * bili(8),
		"paddingBottom":pingKuan * bili(22),
		"marginBottom":pingKuan * bili(12),
		"paddingLeft":pingKuan * bili(14)
	});
	$(".box1 img").css({
		"paddingTop":pingKuan * bili(9),
		"paddingRight":pingKuan * bili(28),
		"width":pingKuan * bili(110),
		"height":pingKuan * bili(110)
	});
	$(".box1 .rr").css({
		"width":pingKuan * bili(557)
	});
	$(".box1 .rr h2").css({
		"lineHeight":pingKuan * bili(44)+'px'
	});
	$(".box1 .rr h3 span").css({
		"lineHeight":pingKuan * bili(40)+'px',
		"marginRight":pingKuan * bili(64)
	});
	$(".box1 .rr h3 b").css({
		"lineHeight":pingKuan * bili(40)+'px'
	});
	$(".box1 .rr p").css({
		"maxWidth":pingKuan * bili(438)
	});
	$(".download").css({
		"top":pingKuan * bili(55),
		"right":pingKuan * bili(18)
	});
	$(".download img").css({
		"width":pingKuan * bili(60),
		"height":pingKuan * bili(52)
	});
	$(".box2").css({
		"marginBottom":pingKuan * bili(12),
		"paddingLeft":pingKuan * bili(14)
	});
	$(".box2 h2").css({
		"textIndent":pingKuan * bili(15),
		"paddingBottom":pingKuan * bili(8),
		"lineHeight":pingKuan * bili(50)+'px'
	});
	$(".box2 h2").css({
		"textIndent":pingKuan * bili(15),
		"paddingBottom":pingKuan * bili(8),
		"lineHeight":pingKuan * bili(50)+'px'
	});
	$(".box2 h2 span").css({
		"paddingRight":pingKuan * bili(18),
		"lineHeight":pingKuan * bili(50)+'px'
	});
	$(".con").css({
		"paddingBottom":pingKuan * bili(20),
		"height":pingKuan * bili(320)
	}); 
	$(".con .uul li").css({
		"width":pingKuan * bili(480),
		"height":pingKuan * bili(320),
		"marginRight":pingKuan * bili(27)
	});
	$(".con .uul li").last().css({
		"marginRight":pingKuan * bili(14)
	});
	$(".con .uul li img").css({
		"width":pingKuan * bili(480),
		"height":pingKuan * bili(320)
	});
	
	$(".box3").last().css({
		"paddingLeft":pingKuan * bili(14),
		"marginBottom":pingKuan * bili(12)
	});
	$(".box3 h2").last().css({
		"textIndent":pingKuan * bili(15),
		"paddingBottom":pingKuan * bili(8),
		"lineHeight":pingKuan * bili(50)+'px'
	});
	$(".box3 p").last().css({
		"paddingBottom":pingKuan * bili(15),
		"paddingLeft":pingKuan * bili(15),
		"lineHeight":pingKuan * bili(40)+'px'
	});
	$(".box3 p a").last().css({
		"width":pingKuan * bili(50),
		"height":pingKuan * bili(28),
		"top":pingKuan * bili(5)
	});
	$(".box4 .h2").last().css({
		"marginLeft":pingKuan * bili(14),
		"textIndent":pingKuan * bili(15),
		"lineHeight":pingKuan * bili(50)+'px'
	});
	
	$(".ul3").css({
		"paddingTop":pingKuan * bili(10)
	});
	$(".ul3 li").css({
		"paddingTop":pingKuan * bili(8),
		"paddingBottom":pingKuan * bili(22),
		"paddingRight":pingKuan * bili(32)
	});
	$(".ul3 li").last().css({
		"paddingBottom":pingKuan * bili(10)
	});
	$(".ul3 li img").css({
		"width":pingKuan * bili(110),
		"height":pingKuan * bili(110),
		"paddingRight":pingKuan * bili(28),
		"paddingLeft":pingKuan * bili(13),
		"paddingTop":pingKuan * bili(9)
	});
	$(".ul3 li .rr").css({
		"width":pingKuan * bili(540)
	});
	$(".ul3 li .rr h2").css({
		"lineHeight":pingKuan * bili(44)+'px'
	});
	$(".ul3 li .rr h3 span").css({
		"lineHeight":pingKuan * bili(37)+'px',
		"marginRight":pingKuan * bili(64)
	});
	$(".ul3 li .rr h3 b").css({
		"lineHeight":pingKuan * bili(37)+'px'
	});
	$(".ul3 li .rr p").css({
		"lineHeight":pingKuan * bili(37)+'px',
		"maxWidth":pingKuan * bili(438)
	});
	$(".download").css({
		"top":pingKuan * bili(55),
		"right":pingKuan * bili(18)
	});
	$(".download img").css({
		"width":pingKuan * bili(60),
		"height":pingKuan * bili(52)
	});
	$(".jiazai").css({
		"lineHeight":pingKuan * bili(80)+'px'
	});
	
	
	
	
	
	
	
	//样式重置完成之后
	$(".load").hide();
	$(".m_wrapper").css({
		"opacity": 1
	});
	
	
	//字体超出隐藏
	var nNum = $('#p').html();
	var bBtn = true;
	xianzhi($('#p'),50);
	function xianzhi(obj,Max){
		var num = nNum;
		if( num.length > Max ){
			obj.html(num.substr(0,Max)+'...').append($('<a class="a1" href="javascript:;"></a>'));
		}else{
			obj.html(nNum).find('a').addClass('a2');	
		};
		
		$(".box3 p a").last().css({
			"width":pingKuan * bili(50),
			"height":pingKuan * bili(28),
			"top":pingKuan * bili(5)
		});
	};
	
	$('#p').live('click',function(){
		if(bBtn){
			xianzhi($('#p'),500);	
		}else{
			xianzhi($('#p'),50);
		};
		bBtn = !bBtn;
	});
	
	
	
	var num = $('.uul li').length;
	var zhi = $('.uul li').width();
	var marRight = parseInt($('.uul li').css("marginRight"));
	
	$('#scroller').css({'width':num*(zhi+marRight)-marRight/2});
	$('#scroller .uul').css({'width':num*(zhi+marRight)});
	
	var myScroll;
	function loaded () {
		myScroll = new IScroll('#wrapper', { scrollX: true, scrollY: false, mouseWheel: true, });
	};
	//document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
	loaded();
};


function loaded(){
	setTimeout(function(){styleJs();},300);
}
function hengshuping(){
	if(window.orientation==180||window.orientation==0){loaded();};
	if(window.orientation==90||window.orientation==-90){loaded();}
}
window.addEventListener("onorientationchange" in window ? "orientationchange" : "resize", hengshuping, false);
window.addEventListener('load', loaded, false);