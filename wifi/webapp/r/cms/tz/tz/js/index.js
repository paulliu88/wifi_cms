var bone = true;
function styleJs(){
	var pingKuan=$(".m_wrapper").width(),//m_wrapper是最外层div的类名
		bili = function(mun){
			var oScale = mun/750;	//750是psd宽度
			return oScale.toFixed(6);
		};
	
	//例子
	$(".m_wrapper").css({
		"paddingBottom":pingKuan * bili(93)
	});
	$(".top").css({
		"width":pingKuan * bili(750),	//240是在480宽度的psd中的width
		"height":pingKuan * bili(117),
		"paddingTop":pingKuan * bili(23),
		"paddingRight":pingKuan * bili(15),
		"paddingBottom":pingKuan * bili(23),
		"paddingLeft":pingKuan * bili(10)
	});
	$(".search").css({
		"height":pingKuan * bili(69),
		"lineHeight":pingKuan * bili(50)+'px',
		"width":pingKuan * bili(726),
		"textIndent":pingKuan * bili(55)
	});
	$(".top img").css({
		"top":pingKuan * bili(47),
		"left":pingKuan * bili(24),
		"width":pingKuan * bili(24),
		"height":pingKuan * bili(25)
	});
	$(".top span").css({
		"lineHeight":pingKuan * bili(80)+'px',
		"top":pingKuan * bili(23),
		"right":pingKuan * bili(30)
	});
	$(".daohang ul").css({
		//"width":pingKuan * bili(725)
	});
	/*$(".daohang ul li").css({
		"width":pingKuan * bili(86),
		"marginRight":pingKuan * bili(24)
	});*/
	$(".daohang ul li").last().css({
		"marginRight":pingKuan * bili(0)
	});
	$(".daohang ul li .img").css({
		"height":pingKuan * bili(65),
		"lineHeight":pingKuan * bili(65)+'px'
	});
	$(".daohang img").css({
		"width":pingKuan * bili(40)
	});
	$(".daohang img").eq(5).css({
		//"width":pingKuan * bili(65)
	});
	$(".daohang ul li .p").css({
		"lineHeight":pingKuan * bili(28)+'px'
	});
	$(".tanchu").css({
		"width":pingKuan * bili(570),
		"height":pingKuan * bili(263),
		"marginTop":pingKuan * bili(-181),
		"marginLeft":pingKuan * bili(-285)
	});
	$(".tanchu p").css({
		"paddingTop":pingKuan * bili(55),
		"paddingBottom":pingKuan * bili(55),
		"lineHeight":pingKuan * bili(50)+'px'
	});
	$(".tanchu .div a").css({
		"paddingTop":pingKuan * bili(25),
		"paddingBottom":pingKuan * bili(25),
		"lineHeight":pingKuan * bili(50)+'px'
	});
	$("header").css({
		"paddingTop":pingKuan * bili(15),
		"paddingRight":pingKuan * bili(14),
		"paddingBottom":pingKuan * bili(15),
		"paddingLeft":pingKuan * bili(14)
	});
	$("header .box1").css({
		"height":pingKuan * bili(56)
	});
	$("header .box1_left span").css({
		"paddingRight":pingKuan * bili(5)
	});
	$("header .box1_left .span1").css({
		"lineHeight":pingKuan * bili(44)+'px'
	});
	$("header .box1_left .span2 a p").css({
		"lineHeight":pingKuan * bili(15)+'px'
	});
	$("header .box1_left img").css({
		"width":pingKuan * bili(47)
	});
	$("header .box1_right").css({
		"height":pingKuan * bili(56)
	});
	$("header .box1_right span").css({
		"lineHeight":pingKuan * bili(38)+'px'
	});
	$("header .box1_right img").css({
		"width":pingKuan * bili(34),
		"top":pingKuan * bili(7)
	});
	$("header .box2").css({
		"height":pingKuan * bili(73)
	});
	$("header .box2 .search").css({
		"height":pingKuan * bili(71),
		"textIndent":pingKuan * bili(26),
		"lineHeight":pingKuan * bili(40)+'px'
	});
	$("header .box2 .sousuo").css({ 
		"width":pingKuan * bili(133),
		"height":pingKuan * bili(72),
		"lineHeight":pingKuan * bili(72)+'px'
	});
	$("header .box2 .xiala").css({
		"right":pingKuan * bili(133)
	});
	$("header .box2 span").css({
		"paddingRight":pingKuan * bili(48),
		"height":pingKuan * bili(72),
		"lineHeight":pingKuan * bili(62)+'px'
	});
	$("header .box2 span i").css({
		"width":pingKuan * bili(13),
		"height":pingKuan * bili(10),
		"top":pingKuan * bili(35),
		"right":pingKuan * bili(20)
	});
	/*$("header .box2 span img").css({
		"width":pingKuan * bili(127),
		"height":pingKuan * bili(43)
	});
	$("header .box2 nav img").css({
		"width":pingKuan * bili(127),
		"height":pingKuan * bili(43),
		"marginBottom":pingKuan * bili(10)
	});*/
	//新闻头条
	$(".news").css({
		"padding":pingKuan * bili(14),
		"paddingBottom":pingKuan * bili(0)
	});
	$(".news .xiugai").css({
		"width":pingKuan * bili(320)
	});
	$(".news .xiugai p").css({
		"width":pingKuan * bili(320),
		"height":pingKuan * bili(94),
		"lineHeight":pingKuan * bili(94)+'px',
		"textIndent":pingKuan * bili(32)
	});
	$(".news .xiugai p a").css({
		"width":pingKuan * bili(320),
		"height":pingKuan * bili(94)
	});
	$(".news h2").css({
		"lineHeight":pingKuan * bili(34)+'px',
		"textIndent":pingKuan * bili(20),
		"marginBottom":pingKuan * bili(14)
	});
	$(".news h2 i").css({
		"lineHeight":pingKuan * bili(28)+'px',
		"height":pingKuan * bili(28)
	});
	$(".news h2 i img").css({
		"width":pingKuan * bili(29)
	});
	$(".news .box .ul").css({
		"height":pingKuan * bili(257)
	});
	$(".news .box .ul li").css({
		"height":pingKuan * bili(257)
	});
	$(".news .box .ul li img").css({
		"height":pingKuan * bili(257)
	});
	$(".news .box .ul li p").css({
		"lineHeight":pingKuan * bili(55)+'px',
		"height":pingKuan * bili(50)
	});
	$(".btn").css({
		"paddingBottom":pingKuan * bili(7)
	});
	$(".btn li").css({
		"width":pingKuan * bili(15),
		"height":pingKuan * bili(15),
		"marginLeft":pingKuan * bili(6),
		"marginRight":pingKuan * bili(6)
	});
	$(".mmm").css({
		"height":pingKuan * bili(257)
	});
	$(".nav").css({
		"paddingRight":pingKuan * bili(14),
		"paddingLeft":pingKuan * bili(14)
	});
	$(".nav li").css({
		"lineHeight":pingKuan * bili(35)+'px',
		"paddingLeft":pingKuan * bili(14),
		"marginBottom":pingKuan * bili(10)
	});
	$(".nav li a").css({
		"paddingLeft":pingKuan * bili(30),
		"marginBottom":pingKuan * bili(10)
	});
	$(".more").css({
		"lineHeight":pingKuan * bili(52)+'px',
		"paddingBottom":pingKuan * bili(8)
	});
	
	//在线影视
	$(".player").css({
		"padding":pingKuan * bili(14),
		"paddingBottom":pingKuan * bili(0)
	});
	$(".player h2").css({
		"lineHeight":pingKuan * bili(34)+'px',
		"paddingBottom":pingKuan * bili(0),
		"marginBottom":pingKuan * bili(14),
		"textIndent":pingKuan * bili(20)
	});
	$(".player h2 i").css({
		"lineHeight":pingKuan * bili(28)+'px',
		"height":pingKuan * bili(28)
	});
	$(".player h2 i img").css({
		"width":pingKuan * bili(29)
	});
	$(".player .ul1").css({
		"width":pingKuan * bili(725)
	});
	$(".player .ul1 li").css({
		//"width":pingKuan * bili(57),
		"lineHeight":pingKuan * bili(72)+'px',
		"paddingRight":pingKuan * bili(25)
	});
	$(".player .ul1 li").last().css({
		//"width":pingKuan * bili(57),
		//"lineHeight":pingKuan * bili(72)+'px',
		"paddingRight":pingKuan * bili(1)
	});
	$(".player .ul2").css({
		"width":pingKuan * bili(720),
		"marginBottom":pingKuan * bili(10)
	});
	$(".player .ul2 li").css({
		"width":pingKuan * bili(168),
		"marginRight":pingKuan * bili(10)
	});
	$(".player .ul2 li div").css({
		"paddingTop":pingKuan * bili(10),
		"paddingBottom":pingKuan * bili(10)
	});
	$(".player .ul2 li h3").css({
		"lineHeight":pingKuan * bili(30)+'px',
		"textIndent":pingKuan * bili(3)
	});
	$(".player .ul2 li p").css({
		"lineHeight":pingKuan * bili(30)+'px',
		"textIndent":pingKuan * bili(3)
	});
	$(".tuiguang").css({
		"marginBottom":pingKuan * bili(10)
	});
	$(".tuiguang span").css({
		"lineHeight":pingKuan * bili(40)+'px',
		"height":pingKuan * bili(36),
		"width":pingKuan * bili(102)
	});
	$(".tuiguang p").css({
		"lineHeight":pingKuan * bili(36)+'px',
		"paddingLeft":pingKuan * bili(10)
	});
	$(".tuiguang p a span").css({
		"width":pingKuan * bili(460),
		"paddingLeft":pingKuan * bili(10)
	});
	$(".qianggou").css({
		"marginBottom":pingKuan * bili(10)
	});
	$(".qianggou span").css({
		"lineHeight":pingKuan * bili(40)+'px',
		"height":pingKuan * bili(36),
		"width":pingKuan * bili(102)
	});
	$(".qianggou p").css({
		"lineHeight":pingKuan * bili(36)+'px',
		"paddingLeft":pingKuan * bili(10)
	});

	//美女主播
	$(".meinv").css({
		"padding":pingKuan * bili(14),
		"paddingBottom":pingKuan * bili(0)
	});
	$(".meinv h2").css({
		//"lineHeight":pingKuan * bili(28)+'px',
		"paddingBottom":pingKuan * bili(0),
		"marginBottom":pingKuan * bili(14),
		"textIndent":pingKuan * bili(20)
	});
	$(".meinv h2 i").css({
		"lineHeight":pingKuan * bili(28)+'px',
		"height":pingKuan * bili(28)
	});
	$(".meinv h2 i img").css({
		"width":pingKuan * bili(29)
	});
	$(".meinv .ul1").css({
		"width":pingKuan * bili(750)
	});
	$(".meinv .ul1 li").css({
		//"width":pingKuan * bili(118),
		"lineHeight":pingKuan * bili(72)+'px',
		"marginRight":pingKuan * bili(15)
	});
	$(".meinv .ul2").css({
		"width":pingKuan * bili(720)
	});
	$(".meinv .ul2 li").css({
		"marginRight":pingKuan * bili(10),
		"marginBottom":pingKuan * bili(10)
	});
	$(".meinv .ul2 li").first().css({
		"width":pingKuan * bili(346)
	});
	$(".meinv .ul2 li .img1").css({
		"width":pingKuan * bili(169),
		"height":pingKuan * bili(116)
	});
	$(".meinv .ul2 li .img2").css({
		"width":pingKuan * bili(346),
		"height":pingKuan * bili(260)
	});
	$(".meinv .ul2 li div").css({
		"lineHeight":pingKuan * bili(22)+'px',
		"height":pingKuan * bili(22),
		"paddingRight":pingKuan * bili(8),
		"paddingLeft":pingKuan * bili(6)
	});
	$(".meinv .ul2 li div i img").css({
		"paddingRight":pingKuan * bili(2),
		"width":pingKuan * bili(11),
		"height":pingKuan * bili(13)
	});
	
	//焦点图
	$(".second").css({
		"padding":pingKuan * bili(0),
		"height":pingKuan * bili(260)
	});
	$(".nnav").css({
		"padding":pingKuan * bili(0),
		"width":pingKuan * bili(1730),
		"height":pingKuan * bili(260)
	});
	$(".firth").css({
		"padding":pingKuan * bili(0),
		"height":pingKuan * bili(260)
	});
	$(".firth a").css({
		"padding":pingKuan * bili(0),
		"width":pingKuan * bili(346),
		"height":pingKuan * bili(260)
	});
	$(".second .js").css({
		"paddingLeft":pingKuan * bili(0),
		"width":pingKuan * bili(346),
		"height":pingKuan * bili(54)
	});
	$(".second .js h4").css({
		"height":pingKuan * bili(22),
		"lineHeight":pingKuan * bili(22)+'px',
		"textIndent":pingKuan * bili(8),
		"paddingTop":pingKuan * bili(6)
	});
	$(".second .js p").css({
		"height":pingKuan * bili(22),
		"textIndent":pingKuan * bili(8),
		"lineHeight":pingKuan * bili(22)+'px',
		"paddingTop":pingKuan * bili(5)
	});
	$(".second .js p b").css({
		"top":pingKuan * bili(-3),
		"paddingRight":pingKuan * bili(18)
	});
	$(".second .js p b img").css({
		"paddingRight":pingKuan * bili(5),
		"width":pingKuan * bili(18),
		"height":pingKuan * bili(20)
	});
	$(".second .bbtn").css({
		"width":pingKuan * bili(140),
		//"height":pingKuan * bili(8),
		"bottom":pingKuan * bili(26)
	});
	$(".second .bbtn a").css({
		"width":pingKuan * bili(8),
		"height":pingKuan * bili(8),
		"marginRight":pingKuan * bili(9)
	});
	
	
	
	//游戏热荐
	$(".game").css({
		"padding":pingKuan * bili(14),
		"paddingBottom":pingKuan * bili(0)
	});
	$(".game h2").css({
		//"lineHeight":pingKuan * bili(28)+'px',
		"paddingBottom":pingKuan * bili(0),
		"marginBottom":pingKuan * bili(14),
		"textIndent":pingKuan * bili(20)
	});
	$(".game h2 i").css({
		"lineHeight":pingKuan * bili(28)+'px',
		"height":pingKuan * bili(28)
	});
	$(".game h2 i img").css({
		"width":pingKuan * bili(29)
	});
	$(".game .ul1").css({
		"paddingTop":pingKuan * bili(10),
		"width":pingKuan * bili(710)
	});
	$(".game .ul1 li").css({
		"width":pingKuan * bili(344),
		"marginRight":pingKuan * bili(10)
	});
	$(".game .ul1 li img").css({
		"width":pingKuan * bili(344)
	});
	$(".game .ul2").css({
		"width":pingKuan * bili(740),
		"paddingTop":pingKuan * bili(10)
	});
	$(".game .ul2 li").css({
		"paddingRight":pingKuan * bili(30),
		"width":pingKuan * bili(118)
	});
	$(".game .ul2 li p").css({
		"lineHeight":pingKuan * bili(54)+'px'
	});
	$(".game .ul2 li img").css({
		"width":pingKuan * bili(110)
	});
	
	//小说推荐
	$(".xiaoshuo .ul1").css({
		"width":pingKuan * bili(750)
	});
	$(".xiaoshuo .ul1 li").css({
		"width":pingKuan * bili(80),
		"lineHeight":pingKuan * bili(72)+'px',
		"paddingRight":pingKuan * bili(26)
	});
	//样式重置完成之后
	$(".load").hide();
	$(".m_wrapper").css({
		"opacity": 1
	});
	
	
	TouchSlide({ 
		slideCell:"#slideBox1",
		titCell:".bbtn", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
		mainCell:".lunbo .nnav", 
		effect:"leftLoop", 
		autoPage:true,//自动分页
		autoPlay:true //自动播放
	});
};
function loaded(){
	setTimeout(function(){styleJs();},300);
}
function hengshuping(){
	if(window.orientation==180||window.orientation==0){loaded();};
	if(window.orientation==90||window.orientation==-90){loaded();}
}
window.addEventListener("onorientationchange" in window ? "orientationchange" : "resize", hengshuping, false);
window.addEventListener('load', loaded, false);
$(function(){
    $.ajax({
    	type:"post",
    	url:"/user/weather.htm",
    	success:function(data){
    		if(data){
	    		$(".span1").text(data.showapi_res_body.now.temperature+"℃");
	    		$(".span2 p").eq(0).text(data.showapi_res_body.now.aqiDetail.area);
	    		$("#weatherimg").attr("src",data.showapi_res_body.now.weather_pic);
    		}
    	}
    });
$.ajax({
    	type:"post",
    	url:"http://ts.mobile.sogou.com/query?pid=sogou-waps-b71155d90aef3bc3&num=50&length=30",
    	success:function(data){
    		var json=eval(data);
    		var num=Math.floor(Math.random()*json.length);
    		$("#searchHot").val(json[num].kwd);
    	}
    });
});