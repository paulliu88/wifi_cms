var bone = true;
function styleJs(){
	var pingKuan=$(".m_wrapper").width(),//m_wrapper是最外层div的类名
		bili = function(mun){
			var oScale = mun/750;	//750是psd宽度
			return oScale.toFixed(6);
		};
	
	//例子
	$("header h1").css({
		"height":pingKuan * bili(66),
		"lineHeight":pingKuan * bili(66)+'px'
	});
	$(".top").css({
		"height":pingKuan * bili(76),
		"paddingRight":pingKuan * bili(13),
		"paddingLeft":pingKuan * bili(11)
	});
	$(".top input").css({
		"width":pingKuan * bili(726),
		"height":pingKuan * bili(76),
		"lineHeight":pingKuan * bili(80)+'px',
		"textIndent":pingKuan * bili(80)
	});
	$(".top img").css({
		"left":pingKuan * bili(35),
		"top":pingKuan * bili(27),
		"width":pingKuan * bili(23),
		"height":pingKuan * bili(25)
	});
	$(".top a").css({
		"right":pingKuan * bili(13),
		"marginRight":pingKuan * bili(17),
		"lineHeight":pingKuan * bili(80)+'px'
	});
	$("header nav").css({
		"height":pingKuan * bili(76),
		"paddingTop":pingKuan * bili(10),
		"lineHeight":pingKuan * bili(66)+'px'
	});
	$("header nav a").css({
		"width":pingKuan * bili(242),
		"height":pingKuan * bili(76),
		"lineHeight":pingKuan * bili(76)+'px'
	});
	$("header nav a").last().css({
		"width":pingKuan * bili(240)
	});
	$("header nav a img").css({
		"width":pingKuan * bili(28),
		"height":pingKuan * bili(30),
		"marginRight":pingKuan * bili(15),
		"width":pingKuan * bili(28),
		"height":pingKuan * bili(35)
	});
	$("header nav a span").css({
		"lineHeight":pingKuan * bili(52)+'px',
		"top":pingKuan * bili(5)
	});
	$(".box .ul1").css({
		"width":pingKuan * bili(734),
		"height":pingKuan * bili(169)
	});
	$(".box .ul1 li").css({
		"width":pingKuan * bili(357),
		"height":pingKuan * bili(169),
		"marginRight":pingKuan * bili(10)
	});
	$(".box .ul1 li img").css({
		"width":pingKuan * bili(357),
		"height":pingKuan * bili(169)
	});
	$(".ul2").css({
		"paddingTop":pingKuan * bili(10)
	});
	$(".ul2 li").css({
		"paddingTop":pingKuan * bili(12),
		"paddingBottom":pingKuan * bili(22),
		"paddingRight":pingKuan * bili(32)
	});
	$(".ul2 li img").css({
		"width":pingKuan * bili(110),
		"height":pingKuan * bili(110),
		"paddingRight":pingKuan * bili(28),
		"paddingLeft":pingKuan * bili(13),
		"paddingTop":pingKuan * bili(9)
	});
	$(".ul2 li .rr").css({
		"width":pingKuan * bili(540)
	});
	$(".ul2 li .rr h2").css({
		"lineHeight":pingKuan * bili(44)+'px'
	});
	$(".ul2 li .rr h3 span").css({
		"lineHeight":pingKuan * bili(37)+'px',
		"marginRight":pingKuan * bili(64)
	});
	$(".ul2 li .rr h3 b").css({
		"lineHeight":pingKuan * bili(37)+'px'
	});
	$(".ul2 li .rr p").css({
		"lineHeight":pingKuan * bili(37)+'px',
		"maxWidth":pingKuan * bili(438)
	});
	$(".download").css({
		"top":pingKuan * bili(55),
		"right":pingKuan * bili(18)
	});
	$(".download img").css({
		"width":pingKuan * bili(60),
		"height":pingKuan * bili(52)
	});
	$(".jiazai").css({
		"lineHeight":pingKuan * bili(80)+'px'
	});
	
	
	
	
	
	
	
	
	
	
	//样式重置完成之后
	$(".load").hide();
	$(".m_wrapper").css({
		"opacity": 1
	});
	
};


function loaded(){
	setTimeout(function(){styleJs();},300);
}
function hengshuping(){
	if(window.orientation==180||window.orientation==0){loaded();};
	if(window.orientation==90||window.orientation==-90){loaded();}
}
window.addEventListener("onorientationchange" in window ? "orientationchange" : "resize", hengshuping, false);
window.addEventListener('load', loaded, false);