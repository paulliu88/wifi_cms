var bone = true;
function styleJs(){
	var pingKuan=$(".m_wrapper").width(),//m_wrapper是最外层div的类名
		bili = function(mun){
			var oScale = mun/750;	//750是psd宽度
			return oScale.toFixed(6);
		};
	
	//例子
	$("header h1").css({
		"height":pingKuan * bili(100),
		"lineHeight":pingKuan * bili(100)+'px',
		"textIndent":pingKuan * bili(40)
	});
	$("header h1 a").css({
		"left":pingKuan * bili(11),
		"top":pingKuan * bili(15)
	});
	$("header h1 a img").css({
		"width":pingKuan * bili(58),
		"height":pingKuan * bili(58)
	});
	$(".box").css({
		"width":pingKuan * bili(730)
	});
	$(".video").css({
		"width":pingKuan * bili(730),
		"height":pingKuan * bili(384)
	});
	$(".video video").css({
		"width":pingKuan * bili(730),
		"height":pingKuan * bili(384)
	});
	$(".box1").css({
		"paddingTop":pingKuan * bili(7),
		"paddingLeft":pingKuan * bili(17),
		"paddingRight":pingKuan * bili(17)
	});
	$(".box1 p").css({
		"height":pingKuan * bili(54),
		"lineHeight":pingKuan * bili(54)+'px'
	});
	$(".box1 p span").css({
		"paddingRight":pingKuan * bili(40)
	});
	$(".box1 p span img").css({
		"width":pingKuan * bili(100),
		"height":pingKuan * bili(20)
	});
	$(".box2").css({
		"width":pingKuan * bili(730),
		"height":pingKuan * bili(102),
		"marginBottom":pingKuan * bili(12)
	});
	$(".box2 .ul1").css({
		"width":pingKuan * bili(730),
		"height":pingKuan * bili(102)
	});
	$(".box2 .ul1 li").css({
		"width":pingKuan * bili(242),
		"height":pingKuan * bili(102),
		"lineHeight":pingKuan * bili(102)+'px'
	});
	$(".box2 .ul1 li img").css({
		"width":pingKuan * bili(40),
		"height":pingKuan * bili(40),
		"marginRight":pingKuan * bili(22)
	});
	$(".box3").css({
		"paddingLeft":pingKuan * bili(14),
		"marginBottom":pingKuan * bili(12)
	});
	$(".box3 h2").css({
		"textIndent":pingKuan * bili(15),
		"paddingBottom":pingKuan * bili(8),
		"lineHeight":pingKuan * bili(50)+'px'
	});
	$(".box3 p").css({
		"paddingBottom":pingKuan * bili(15),
		"paddingLeft":pingKuan * bili(15),
		"lineHeight":pingKuan * bili(40)+'px'
	});
	$(".box3 p a").css({
		"width":pingKuan * bili(50),
		"height":pingKuan * bili(28),
		"top":pingKuan * bili(5)
	});
	$(".rdtj").css({
		"marginBottom":pingKuan * bili(12)
	});
	$(".rdtj h2").css({
		"paddingBottom":pingKuan * bili(8),
		"marginLeft":pingKuan * bili(14),
		"textIndent":pingKuan * bili(15),
		"lineHeight":pingKuan * bili(50)+'px'
	});
	$(".rdtj ul").css({
		"paddingLeft":pingKuan * bili(14),
		"paddingRight":pingKuan * bili(14),
		"width":pingKuan * bili(725)
	});
	$(".rdtj ul li").css({
		"paddingBottom":pingKuan * bili(20),
		"width":pingKuan * bili(170),
		"paddingRight":pingKuan * bili(10)
	});
	$(".rdtj ul li").last().css({
		"paddingRight":pingKuan * bili(0)
	});
	$(".rdtj ul li img").css({
		"width":pingKuan * bili(170),
		"height":pingKuan * bili(220)
	});
	$(".rdtj ul li h3").css({
		"paddingTop":pingKuan * bili(13),
		"lineHeight":pingKuan * bili(34)+'px'
	});
	$(".rdtj ul li p").css({
		"lineHeight":pingKuan * bili(30)+'px'
	});
	
	
	
	
	
	
	
	
	//样式重置完成之后
	$(".load").hide();
	$(".m_wrapper").css({
		"opacity": 1
	});
	
	
	
	
	//字体超出隐藏
	var nNum = $('#p').html();
	var bBtn = true;
	
	xianzhi($('#p'),50);
	function xianzhi(obj,Max){
		var num = nNum;
		if( num.length > Max ){
			obj.html(num.substr(0,Max)+'...').append($('<a class="a1" href="javascript:;"></a>'));
		}else{
			obj.html(nNum).find('a').addClass('a2');	
		};
		
		
		$(".box3 p a").css({
			"width":pingKuan * bili(50),
			"height":pingKuan * bili(28),
			"top":pingKuan * bili(5)
		});
	};
	
	$('#p').live('click',function(){
		if(bBtn){
			xianzhi($('#p'),500);	
		}else{
			xianzhi($('#p'),50);
		};
		bBtn = !bBtn;
	});	
	
};


function loaded(){
	setTimeout(function(){styleJs();},300);
}
function hengshuping(){
	if(window.orientation==180||window.orientation==0){loaded();};
	if(window.orientation==90||window.orientation==-90){loaded();}
}
window.addEventListener("onorientationchange" in window ? "orientationchange" : "resize", hengshuping, false);
window.addEventListener('load', loaded, false);