(function($, f) {
	function testProps(a) {
		var i;
		for (i in a) {
			if (mod[a[i]] !== f) {
				return true
			}
		}
		return false
	}

	function testPrefix() {
		var a = ['Webkit', 'Moz', 'O', 'ms'],
			p;
		for (p in a) {
			if (testProps([a[p] + 'Transform'])) {
				return '-' + a[p].toLowerCase() + '-'
			}
		}
		return ''
	}

	function init(a, b, c) {
		var d = a;
		if (typeof b === 'object') {
			return a.each(function() {
				if (!this.id) {
					this.id = 'mobiscroll' + (++g)
				}
				if (instances[this.id]) {
					instances[this.id].destroy()
				}
				new $.mobiscroll.classes[b.component || 'Scroller'](this, b)
			})
		}
		if (typeof b === 'string') {
			a.each(function() {
				var r, inst = instances[this.id];
				if (inst && inst[b]) {
					r = inst[b].apply(this, Array.prototype.slice.call(c, 1));
					if (r !== f) {
						d = r;
						return false
					}
				}
			})
		}
		return d
	}
	var g = +new Date(),
		instances = {},
		extend = $.extend,
		mod = document.createElement('modernizr').style,
		has3d = testProps(['perspectiveProperty', 'WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective']),
		hasFlex = testProps(['flex', 'msFlex', 'WebkitBoxDirection']),
		prefix = testPrefix(),
		pr = prefix.replace(/^\-/, '').replace(/\-$/, '').replace('moz', 'Moz');
	$.fn.mobiscroll = function(a) {
		extend(this, $.mobiscroll.components);
		return init(this, a, arguments)
	};
	$.mobiscroll = $.mobiscroll || {
		version: '2.14.4',
		util: {
			prefix: prefix,
			jsPrefix: pr,
			has3d: has3d,
			hasFlex: hasFlex,
			testTouch: function(e, a) {
				if (e.type == 'touchstart') {
					$(a).attr('data-touch', '1')
				} else if ($(a).attr('data-touch')) {
					$(a).removeAttr('data-touch');
					return false
				}
				return true
			},
			objectToArray: function(a) {
				var b = [],
					i;
				for (i in a) {
					b.push(a[i])
				}
				return b
			},
			arrayToObject: function(a) {
				var b = {},
					i;
				if (a) {
					for (i = 0; i < a.length; i++) {
						b[a[i]] = a[i]
					}
				}
				return b
			},
			isNumeric: function(a) {
				return a - parseFloat(a) >= 0
			},
			isString: function(s) {
				return typeof s === 'string'
			},
			getCoord: function(e, c) {
				var a = e.originalEvent || e;
				return a.changedTouches ? a.changedTouches[0]['page' + c] : e['page' + c]
			},
			getPosition: function(t, a) {
				var b = window.getComputedStyle ? getComputedStyle(t[0]) : t[0].style,
					matrix, px;
				if (has3d) {
					$.each(['t', 'webkitT', 'MozT', 'OT', 'msT'], function(i, v) {
						if (b[v + 'ransform'] !== f) {
							matrix = b[v + 'ransform'];
							return false
						}
					});
					matrix = matrix.split(')')[0].split(', ');
					px = a ? (matrix[13] || matrix[5]) : (matrix[12] || matrix[4])
				} else {
					px = a ? b.top.replace('px', '') : b.left.replace('px', '')
				}
				return px
			},
			constrain: function(a, b, c) {
				return Math.max(b, Math.min(a, c))
			}
		},
		tapped: false,
		presets: {
			scroller: {},
			numpad: {}
		},
		themes: {
			listview: {},
			menustrip: {}
		},
		i18n: {},
		instances: instances,
		classes: {},
		components: {},
		defaults: {
			theme: 'mobiscroll',
			context: 'body'
		},
		userdef: {},
		setDefaults: function(o) {
			extend(this.userdef, o)
		},
		presetShort: function(a, c, p) {
			this.components[a] = function(s) {
				return init(this, extend(s, {
					component: c,
					preset: p === false ? f : a
				}), arguments)
			}
		}
	}
})(jQuery);
(function($, m, n, o) {
	var p, preventShow, extend = $.extend,
		ms = $.mobiscroll,
		instances = ms.instances,
		userdef = ms.userdef,
		util = ms.util,
		pr = util.jsPrefix,
		has3d = util.has3d,
		getCoord = util.getCoord,
		constrain = util.constrain,
		isString = util.isString,
		isOldAndroid = /android [1-3]/i.test(navigator.userAgent),
		animEnd = 'webkitAnimationEnd animationend',
		empty = function() {},
		prevdef = function(a) {
			a.preventDefault()
		};
	ms.classes.Widget = function(g, h, j) {
		var k, $ctx, $header, $markup, $overlay, $persp, $popup, $wnd, $wrapper, buttons, btn, doAnim, hasButtons, isModal, lang, modalWidth, modalHeight, posEvents, preset, preventPos, s, scrollLock, setReadOnly, theme, wasReadOnly, wndWidth, wndHeight, that = this,
			$elm = $(g),
			elmList = [],
			posDebounce = {};

		function onBtnStart(a) {
			if (btn) {
				btn.removeClass('dwb-a')
			}
			btn = $(this);
			if (!btn.hasClass('dwb-d') && !btn.hasClass('dwb-nhl')) {
				btn.addClass('dwb-a')
			}
			if (a.type === 'mousedown') {
				$(n).on('mouseup', onBtnEnd)
			}
		}

		function onBtnEnd(a) {
			if (btn) {
				btn.removeClass('dwb-a');
				btn = null
			}
			if (a.type === 'mouseup') {
				$(n).off('mouseup', onBtnEnd)
			}
		}

		function onWndKeyDown(a) {
			if (a.keyCode == 13) {
				that.select()
			} else if (a.keyCode == 27) {
				that.cancel()
			}
		}

		function onShow(a) {
			if (!a) {
				$popup.focus()
			}
			that.ariaMessage(s.ariaMessage)
		}

		function onHide(a) {
			var b, value, type, focus = s.focusOnClose;
			$markup.remove();
			if (p && !a) {
				setTimeout(function() {
					if (focus === o || focus === true) {
						preventShow = true;
						b = p[0];
						type = b.type;
						value = b.value;
						try {
							b.type = 'button'
						} catch (ex) {}
						p.focus();
						b.type = type;
						//alert(b.text());
						//alert(value);
						b.value = value
					} else if (focus) {
						if (instances[$(focus).attr('id')]) {
							ms.tapped = false
						}
						$(focus).focus()
					}
				}, 200)
			}
			that._isVisible = false;
			event('onHide', [])
		}

		function onPosition(b) {
			clearTimeout(posDebounce[b.type]);
			posDebounce[b.type] = setTimeout(function() {
				var a = b.type == 'scroll';
				if (a && !scrollLock) {
					return
				}
				that.position(!a)
			}, 200)
		}

		function show(a) {
			if (!ms.tapped) {
				if (a) {
					a()
				}
				if ($(n.activeElement).is('input,textarea')) {
					$(n.activeElement).blur()
				}
				p = $elm;
				that.show()
			}
			setTimeout(function() {
				preventShow = false
			}, 300)
		}

		function event(a, b) {
			var c;
			b.push(that);
			$.each([userdef, theme, preset, h], function(i, v) {
				if (v && v[a]) {
					c = v[a].apply(g, b)
				}
			});
			return c
		}
		that.position = function(a) {
			var w, l, t, anchor, aw, ah, ap, at, al, arr, arrw, arrl, dh, scroll, sl, st, totalw = 0,
				minw = 0,
				css = {},
				nw = Math.min($wnd[0].innerWidth || $wnd.innerWidth(), $persp.width()),
				nh = $wnd[0].innerHeight || $wnd.innerHeight();
			if ((wndWidth === nw && wndHeight === nh && a) || preventPos) {
				return
			}
			if (that._isFullScreen || /top|bottom/.test(s.display)) {
				$popup.width(nw)
			}
			if (event('onPosition', [$markup, nw, nh]) === false || !isModal) {
				return
			}
			sl = $wnd.scrollLeft();
			st = $wnd.scrollTop();
			anchor = s.anchor === o ? $elm : $(s.anchor);
			if (that._isLiquid && s.layout !== 'liquid') {
				if (nw < 400) {
					$markup.addClass('dw-liq')
				} else {
					$markup.removeClass('dw-liq')
				}
			}
			if (!that._isFullScreen && /modal|bubble/.test(s.display)) {
				$wrapper.width('');
				$('.mbsc-w-p', $markup).each(function() {
					w = $(this).outerWidth(true);
					totalw += w;
					minw = (w > minw) ? w : minw
				});
				w = totalw > nw ? minw : totalw;
				$wrapper.width(w).css('white-space', totalw > nw ? '' : 'nowrap')
			}
			modalWidth = that._isFullScreen ? nw : $popup.outerWidth();
			modalHeight = that._isFullScreen ? nh : $popup.outerHeight(true);
			scrollLock = modalHeight <= nh && modalWidth <= nw;
			that.scrollLock = scrollLock;
			if (s.display == 'modal') {
				l = Math.max(0, sl + (nw - modalWidth) / 2);
				t = st + (nh - modalHeight) / 2
			} else if (s.display == 'bubble') {
				scroll = true;
				arr = $('.dw-arrw-i', $markup);
				ap = anchor.offset();
				at = Math.abs($ctx.offset().top - ap.top);
				al = Math.abs($ctx.offset().left - ap.left);
				aw = anchor.outerWidth();
				ah = anchor.outerHeight();
				l = constrain(al - ($popup.outerWidth(true) - aw) / 2, sl + 3, sl + nw - modalWidth - 3);
				t = at - modalHeight;
				if ((t < st) || (at > st + nh)) {
					$popup.removeClass('dw-bubble-top').addClass('dw-bubble-bottom');
					t = at + ah
				} else {
					$popup.removeClass('dw-bubble-bottom').addClass('dw-bubble-top')
				}
				arrw = arr.outerWidth();
				arrl = constrain(al + aw / 2 - (l + (modalWidth - arrw) / 2), 0, arrw);
				$('.dw-arr', $markup).css({
					left: arrl
				})
			} else {
				l = sl;
				if (s.display == 'top') {
					t = st
				} else if (s.display == 'bottom') {
					t = st + nh - modalHeight
				}
			}
			t = t < 0 ? 0 : t;
			css.top = t;
			css.left = l;
			$popup.css(css);
			$persp.height(0);
			dh = Math.max(t + modalHeight, s.context == 'body' ? $(n).height() : $ctx[0].scrollHeight);
			$persp.css({
				height: dh
			});
			if (scroll && ((t + modalHeight > st + nh) || (at > st + nh))) {
				preventPos = true;
				setTimeout(function() {
					preventPos = false
				}, 300);
				$wnd.scrollTop(Math.min(t + modalHeight - nh, dh - nh))
			}
			wndWidth = nw;
			wndHeight = nh
		};
		that.attachShow = function(b, c) {
			elmList.push(b);
			if (s.display !== 'inline') {
				if (setReadOnly) {
					b.on('mousedown.dw', function(a) {
						a.preventDefault()
					})
				}
				if (s.showOnFocus) {
					b.on('focus.dw', function() {
						if (!preventShow) {
							show(c)
						}
					})
				}
				if (s.showOnTap) {
					that.tap(b, function() {
						show(c)
					})
				}
			}
		};
		that.select = function() {
			if (!isModal || that.hide(false, 'set') !== false) {
				that._fillValue();
				event('onSelect', [that._value])
			}
		};
		that.cancel = function() {
			if (!isModal || that.hide(false, 'cancel') !== false) {
				event('onCancel', [that._value])
			}
		};
		that.clear = function() {
			event('onClear', [$markup]);
			if (isModal && !that.live) {
				that.hide(false, 'clear')
			}
			that.setValue(null, true)
		};
		that.enable = function() {
			s.disabled = false;
			if (that._isInput) {
				$elm.prop('disabled', false)
			}
		};
		that.disable = function() {
			s.disabled = true;
			if (that._isInput) {
				$elm.prop('disabled', true)
			}
		};
		that.show = function(c, d) {
			var e;
			if (s.disabled || that._isVisible) {
				return
			}
			if (doAnim !== false) {
				if (s.display == 'top') {
					doAnim = 'slidedown'
				}
				if (s.display == 'bottom') {
					doAnim = 'slideup'
				}
			}
			that._readValue();
			event('onBeforeShow', []);
			e = '<div lang="' + s.lang + '" class="mbsc-' + s.theme + (s.baseTheme ? ' mbsc-' + s.baseTheme : '') + ' dw-' + s.display + ' ' + (s.cssClass || '') + (that._isLiquid ? ' dw-liq' : '') + (isOldAndroid ? ' mbsc-old' : '') + (hasButtons ? '' : ' dw-nobtn') + '">' + '<div class="dw-persp">' + (isModal ? '<div class="dwo"></div>' : '') + '<div' + (isModal ? ' role="dialog" tabindex="-1"' : '') + ' class="dw' + (s.rtl ? ' dw-rtl' : ' dw-ltr') + '">' + (s.display === 'bubble' ? '<div class="dw-arrw"><div class="dw-arrw-i"><div class="dw-arr"></div></div></div>' : '') + '<div class="dwwr">' + '<div aria-live="assertive" class="dw-aria dw-hidden"></div>' + (s.headerText ? '<div class="dwv">' + (isString(s.headerText) ? s.headerText : '') + '</div>' : '') + '<div class="dwcc">';
			e += that._generateContent();
			e += '</div>';
			if (hasButtons) {
				e += '<div class="dwbc">';
				$.each(buttons, function(i, b) {
					b = isString(b) ? that.buttons[b] : b;
					if (b.handler === 'set') {
						b.parentClass = 'dwb-s'
					}
					if (b.handler === 'cancel') {
						b.parentClass = 'dwb-c'
					}
					b.handler = isString(b.handler) ? that.handlers[b.handler] : b.handler;
					e += '<div' + (s.btnWidth ? ' style="width:' + (100 / buttons.length) + '%"' : '') + ' class="dwbw ' + (b.parentClass || '') + '"><div tabindex="0" role="button" class="dwb' + i + ' dwb-e ' + (b.cssClass === o ? s.btnClass : b.cssClass) + (b.icon ? ' mbsc-ic mbsc-ic-' + b.icon : '') + '">' + (b.text || '') + '</div></div>'
				});
				e += '</div>'
			}
			e += '</div></div></div></div>';
			$markup = $(e);
			$persp = $('.dw-persp', $markup);
			$overlay = $('.dwo', $markup);
			$wrapper = $('.dwwr', $markup);
			$header = $('.dwv', $markup);
			$popup = $('.dw', $markup);
			k = $('.dw-aria', $markup);
			that._markup = $markup;
			that._header = $header;
			that._isVisible = true;
			posEvents = 'orientationchange resize';
			that._markupReady();
			event('onMarkupReady', [$markup]);
			if (isModal) {
				$(m).on('keydown', onWndKeyDown);
				if (s.scrollLock) {
					$markup.on('touchmove mousewheel DOMMouseScroll', function(a) {
						if (scrollLock) {
							a.preventDefault()
						}
					})
				}
				if (pr !== 'Moz') {
					$('input,select,button', $ctx).each(function() {
						if (!this.disabled) {
							$(this).addClass('dwtd').prop('disabled', true)
						}
					})
				}
				posEvents += ' scroll';
				ms.activeInstance = that;
				$markup.appendTo($ctx);
				if (has3d && doAnim && !c) {
					$markup.addClass('dw-in dw-trans').on(animEnd, function() {
						$markup.off(animEnd).removeClass('dw-in dw-trans').find('.dw').removeClass('dw-' + doAnim);
						onShow(d)
					}).find('.dw').addClass('dw-' + doAnim)
				}
			} else if ($elm.is('div')) {
				$elm.html($markup)
			} else {
				$markup.insertAfter($elm)
			}
			event('onMarkupInserted', [$markup]);
			that.position();
			$wnd.on(posEvents, onPosition);
			$markup.on('selectstart mousedown', prevdef).on('click', '.dwb-e', prevdef).on('keydown', '.dwb-e', function(a) {
				if (a.keyCode == 32) {
					a.preventDefault();
					a.stopPropagation();
					$(this).click()
				}
			});
			setTimeout(function() {
				$.each(buttons, function(i, b) {
					that.tap($('.dwb' + i, $markup), function(a) {
						b = isString(b) ? that.buttons[b] : b;
						b.handler.call(this, a, that)
					}, true)
				});
				if (s.closeOnOverlay) {
					that.tap($overlay, function() {
						that.cancel()
					})
				}
				if (isModal && !doAnim) {
					onShow(d)
				}
				$markup.on('touchstart mousedown', '.dwb-e', onBtnStart).on('touchend', '.dwb-e', onBtnEnd);
				that._attachEvents($markup)
			}, 300);
			event('onShow', [$markup, that._tempValue])
		};
		that.hide = function(a, b, c) {
			if (!that._isVisible || (!c && !that._isValid && b == 'set') || (!c && event('onClose', [that._tempValue, b]) === false)) {
				return false
			}
			if ($markup) {
				if (pr !== 'Moz') {
					$('.dwtd', $ctx).each(function() {
						$(this).prop('disabled', false).removeClass('dwtd')
					})
				}
				if (has3d && isModal && doAnim && !a && !$markup.hasClass('dw-trans')) {
					$markup.addClass('dw-out dw-trans').find('.dw').addClass('dw-' + doAnim).on(animEnd, function() {
						onHide(a)
					})
				} else {
					onHide(a)
				}
				$wnd.off(posEvents, onPosition)
			}
			if (isModal) {
				$(m).off('keydown', onWndKeyDown);
				delete ms.activeInstance
			}
		};
		that.ariaMessage = function(a) {
			k.html('');
			setTimeout(function() {
				k.html(a)
			}, 100)
		};
		that.isVisible = function() {
			return that._isVisible
		};
		that.setValue = empty;
		that._generateContent = empty;
		that._attachEvents = empty;
		that._readValue = empty;
		that._fillValue = empty;
		that._markupReady = empty;
		that._processSettings = empty;
		that.tap = function(c, d, e) {
			var f, startY, moved;
			if (s.tap) {
				c.on('touchstart.dw', function(a) {
					if (e) {
						a.preventDefault()
					}
					f = getCoord(a, 'X');
					startY = getCoord(a, 'Y');
					moved = false
				}).on('touchmove.dw', function(a) {
					if (Math.abs(getCoord(a, 'X') - f) > 20 || Math.abs(getCoord(a, 'Y') - startY) > 20) {
						moved = true
					}
				}).on('touchend.dw', function(a) {
					var b = this;
					if (!moved) {
						a.preventDefault();
						d.call(b, a)
					}
					ms.tapped = true;
					setTimeout(function() {
						ms.tapped = false
					}, 500)
				})
			}
			c.on('click.dw', function(a) {
				if (!ms.tapped) {
					d.call(this, a)
				}
				a.preventDefault()
			})
		};
		that.option = function(a, b) {
			var c = {};
			if (typeof a === 'object') {
				c = a
			} else {
				c[a] = b
			}
			that.init(c)
		};
		that.destroy = function() {
			that.hide(true, false, true);
			$.each(elmList, function(i, v) {
				v.off('.dw')
			});
			if (that._isInput && setReadOnly) {
				g.readOnly = wasReadOnly
			}
			event('onDestroy', []);
			delete instances[g.id];
			that = null
		};
		that.getInst = function() {
			return that
		};
		that.trigger = event;
		that.init = function(a) {
			that.settings = s = {};
			extend(h, a);
			extend(s, ms.defaults, that._defaults, userdef, h);
			theme = ms.themes[s.theme] || ms.themes.mobiscroll;
			lang = ms.i18n[s.lang];
			event('onThemeLoad', [lang, h]);
			extend(s, theme, lang, userdef, h);
			preset = ms.presets[that._class][s.preset];
			s.buttons = s.buttons || (s.display !== 'inline' ? ['set', 'cancel'] : []);
			s.headerText = s.headerText === o ? (s.display !== 'inline' ? '{value}' : false) : s.headerText;
			if (preset) {
				preset = preset.call(g, that);
				extend(s, preset, h)
			}
			if (!ms.themes[s.theme]) {
				s.theme = 'mobiscroll'
			}
			that._isLiquid = (s.layout || (/top|bottom/.test(s.display) ? 'liquid' : '')) === 'liquid';
			that._processSettings();
			$elm.off('.dw');
			doAnim = isOldAndroid ? false : s.animate;
			buttons = s.buttons;
			isModal = s.display !== 'inline';
			setReadOnly = s.showOnFocus || s.showOnTap;
			$wnd = $(s.context == 'body' ? m : s.context);
			$ctx = $(s.context);
			that.context = $wnd;
			that.live = true;
			$.each(buttons, function(i, b) {
				if (b === 'set' || b.handler === 'set') {
					that.live = false;
					return false
				}
			});
			that.buttons.set = {
				text: s.setText,
				handler: 'set'
			};
			that.buttons.cancel = {
				text: (that.live) ? s.closeText : s.cancelText,
				handler: 'cancel'
			};
			that.buttons.clear = {
				text: s.clearText,
				handler: 'clear'
			};
			that._isInput = $elm.is('input');
			hasButtons = buttons.length > 0;
			if (that._isVisible) {
				that.hide(true, false, true)
			}
			if (isModal) {
				that._readValue();
				if (that._isInput && setReadOnly) {
					if (wasReadOnly === o) {
						wasReadOnly = g.readOnly
					}
					g.readOnly = true
				}
				that.attachShow($elm)
			} else {
				that.show()
			}
			$elm.on('change.dw', function() {
				if (!that._preventChange) {
					that.setVal($elm.val(), true, false)
				}
				that._preventChange = false
			});
			event('onInit', [])
		};
		that.buttons = {};
		that.handlers = {
			set: that.select,
			cancel: that.cancel,
			clear: that.clear
		};
		that._value = null;
		that._isValid = true;
		that._isVisible = false;
		if (!j) {
			instances[g.id] = that;
			that.init(h)
		}
	};
	ms.classes.Widget.prototype._defaults = {
		lang: 'en',
		setText: 'Set',
		selectedText: 'Selected',
		closeText: 'Close',
		cancelText: 'Cancel',
		clearText: 'Clear',
		disabled: false,
		closeOnOverlay: true,
		showOnFocus: true,
		showOnTap: true,
		display: 'modal',
		scrollLock: true,
		tap: true,
		btnClass: 'dwb',
		btnWidth: true,
		focusOnClose: false
	};
	ms.themes.mobiscroll = {
		rows: 5,
		showLabel: false,
		headerText: false,
		btnWidth: false,
		selectedLineHeight: true,
		selectedLineBorder: 1,
		dateOrder: 'MMddyy',
		weekDays: 'min',
		checkIcon: 'ion-ios7-checkmark-empty',
		btnPlusClass: 'mbsc-ic mbsc-ic-arrow-down5',
		btnMinusClass: 'mbsc-ic mbsc-ic-arrow-up5',
		btnCalPrevClass: 'mbsc-ic mbsc-ic-arrow-left5',
		btnCalNextClass: 'mbsc-ic mbsc-ic-arrow-right5'
	};
	$(m).on('focus', function() {
		if (p) {
			preventShow = true
		}
	});
	$(n).on('mouseover mouseup mousedown click', function(a) {
		if (ms.tapped) {
			a.stopPropagation();
			a.preventDefault();
			return false
		}
	})
})(jQuery, window, document);
(function($, n, q, r) {
	var u, ms = $.mobiscroll,
		classes = ms.classes,
		instances = ms.instances,
		util = ms.util,
		pr = util.jsPrefix,
		has3d = util.has3d,
		hasFlex = util.hasFlex,
		getCoord = util.getCoord,
		constrain = util.constrain,
		testTouch = util.testTouch;
	classes.Scroller = function(f, g, h) {
		var m, btn, isScrollable, itemHeight, s, trigger, click, moved, start, startTime, stop, p, min, max, target, index, lines, timer, that = this,
			$elm = $(f),
			iv = {},
			pos = {},
			pixels = {},
			wheels = [];

		function onStart(a) {
			if (testTouch(a, this) && !u && !click && !btn && !isReadOnly(this)) {
				a.preventDefault();
				a.stopPropagation();
				u = true;
				isScrollable = s.mode != 'clickpick';
				target = $('.dw-ul', this);
				setGlobals(target);
				moved = iv[index] !== r;
				p = moved ? getCurrentPosition(target) : pos[index];
				start = getCoord(a, 'Y');
				startTime = new Date();
				stop = start;
				scroll(target, index, p, 0.001);
				if (isScrollable) {
					target.closest('.dwwl').addClass('dwa')
				}
				if (a.type === 'mousedown') {
					$(q).on('mousemove', onMove).on('mouseup', onEnd)
				}
			}
		}

		function onMove(a) {
			if (u) {
				if (isScrollable) {
					a.preventDefault();
					a.stopPropagation();
					stop = getCoord(a, 'Y');
					if (Math.abs(stop - start) > 3 || moved) {
						scroll(target, index, constrain(p + (start - stop) / itemHeight, min - 1, max + 1));
						moved = true
					}
				}
			}
		}

		function onEnd(a) {
			if (u) {
				var b = new Date() - startTime,
					val = constrain(p + (start - stop) / itemHeight, min - 1, max + 1),
					speed, dist, tindex, ttop = target.offset().top;
				a.stopPropagation();
				if (has3d && b < 300) {
					speed = (stop - start) / b;
					dist = (speed * speed) / s.speedUnit;
					if (stop - start < 0) {
						dist = -dist
					}
				} else {
					dist = stop - start
				}
				tindex = Math.round(p - dist / itemHeight);
				if (!moved) {
					var c = Math.floor((stop - ttop) / itemHeight),
						li = $($('.dw-li', target)[c]),
						valid = li.hasClass('dw-v'),
						hl = isScrollable;
					if (trigger('onValueTap', [li]) !== false && valid) {
						tindex = c
					} else {
						hl = true
					}
					if (hl && valid) {
						li.addClass('dw-hl');
						setTimeout(function() {
							li.removeClass('dw-hl')
						}, 100)
					}
				}
				if (isScrollable) {
					calc(target, tindex, 0, true, Math.round(val))
				}
				if (a.type === 'mouseup') {
					$(q).off('mousemove', onMove).off('mouseup', onEnd)
				}
				u = false
			}
		}

		function onBtnStart(a) {
			btn = $(this);
			if (testTouch(a, this)) {
				step(a, btn.closest('.dwwl'), btn.hasClass('dwwbp') ? plus : minus)
			}
			if (a.type === 'mousedown') {
				$(q).on('mouseup', onBtnEnd)
			}
		}

		function onBtnEnd(a) {
			btn = null;
			if (click) {
				clearInterval(timer);
				click = false
			}
			if (a.type === 'mouseup') {
				$(q).off('mouseup', onBtnEnd)
			}
		}

		function onKeyDown(a) {
			if (a.keyCode == 38) {
				step(a, $(this), minus)
			} else if (a.keyCode == 40) {
				step(a, $(this), plus)
			}
		}

		function onKeyUp() {
			if (click) {
				clearInterval(timer);
				click = false
			}
		}

		function onScroll(a) {
			if (!isReadOnly(this)) {
				a.preventDefault();
				a = a.originalEvent || a;
				var b = a.wheelDelta ? (a.wheelDelta / 120) : (a.detail ? (-a.detail / 3) : 0),
					t = $('.dw-ul', this);
				setGlobals(t);
				calc(t, Math.round(pos[index] - b), b < 0 ? 1 : 2)
			}
		}

		function step(a, w, b) {
			a.stopPropagation();
			a.preventDefault();
			if (!click && !isReadOnly(w) && !w.hasClass('dwa')) {
				click = true;
				var t = w.find('.dw-ul');
				setGlobals(t);
				clearInterval(timer);
				timer = setInterval(function() {
					b(t)
				}, s.delay);
				b(t)
			}
		}

		function isReadOnly(a) {
			if ($.isArray(s.readonly)) {
				var i = $('.dwwl', m).index(a);
				return s.readonly[i]
			}
			return s.readonly
		}

		function generateWheelItems(i) {
			var a = '<div class="dw-bf">',
				w = wheels[i],
				l = 1,
				labels = w.labels || [],
				values = w.values,
				keys = w.keys || values;
			$.each(values, function(j, v) {
				if (l % 20 === 0) {
					a += '</div><div class="dw-bf">'
				}
				a += '<div role="option" aria-selected="false" class="dw-li dw-v" data-val="' + keys[j] + '"' + (labels[j] ? ' aria-label="' + labels[j] + '"' : '') + ' style="height:' + itemHeight + 'px;line-height:' + itemHeight + 'px;">' + '<div class="dw-i"' + (lines > 1 ? ' style="line-height:' + Math.round(itemHeight / lines) + 'px;font-size:' + Math.round(itemHeight / lines * 0.8) + 'px;"' : '') + '>' + v + '</div></div>';
				l++
			});
			a += '</div>';
			return a
		}

		function setGlobals(t) {
			var a = t.closest('.dwwl').hasClass('dwwms');
			min = $('.dw-li', t).index($(a ? '.dw-li' : '.dw-v', t).eq(0));
			max = Math.max(min, $('.dw-li', t).index($(a ? '.dw-li' : '.dw-v', t).eq(-1)) - (a ? s.rows - 1 : 0));
			index = $('.dw-ul', m).index(t)
		}

		function formatHeader(v) {
			var t = s.headerText;
			return t ? (typeof t === 'function' ? t.call(f, v) : t.replace(/\{value\}/i, v)) : ''
		}

		function getCurrentPosition(t) {
			return Math.round(-util.getPosition(t, true) / itemHeight)
		}

		function ready(t, i) {
			clearTimeout(iv[i]);
			delete iv[i];
			t.closest('.dwwl').removeClass('dwa')
		}

		function scroll(t, a, b, c, d) {
			var e = -b * itemHeight,
				style = t[0].style;
			if (e == pixels[a] && iv[a]) {
				return
			}
			pixels[a] = e;
			if (has3d) {
				style[pr + 'Transition'] = util.prefix + 'transform ' + (c ? c.toFixed(3) : 0) + 's ease-out';
				style[pr + 'Transform'] = 'translate3d(0,' + e + 'px,0)'
			} else {
				style.top = e + 'px'
			}
			if (iv[a]) {
				ready(t, a)
			}
			if (c && d) {
				t.closest('.dwwl').addClass('dwa');
				iv[a] = setTimeout(function() {
					ready(t, a)
				}, c * 1000)
			}
			pos[a] = b
		}

		function getValid(a, t, b, c) {
			var d = $('.dw-li[data-val="' + a + '"]', t),
				cells = $('.dw-li', t),
				v = cells.index(d),
				l = cells.length;
			if (c) {
				setGlobals(t)
			} else if (!d.hasClass('dw-v')) {
				var e = d,
					cell2 = d,
					dist1 = 0,
					dist2 = 0;
				while (v - dist1 >= 0 && !e.hasClass('dw-v')) {
					dist1++;
					e = cells.eq(v - dist1)
				}
				while (v + dist2 < l && !cell2.hasClass('dw-v')) {
					dist2++;
					cell2 = cells.eq(v + dist2)
				}
				if (((dist2 < dist1 && dist2 && b !== 2) || !dist1 || (v - dist1 < 0) || b == 1) && cell2.hasClass('dw-v')) {
					d = cell2;
					v = v + dist2
				} else {
					d = e;
					v = v - dist1
				}
			}
			return {
				cell: d,
				v: c ? constrain(v, min, max) : v,
				val: d.hasClass('dw-v') ? d.attr('data-val') : null
			}
		}

		function scrollToPos(a, b, c, d, e) {
			if (trigger('validate', [m, b, a, d]) !== false) {
				$('.dw-ul', m).each(function(i) {
					var t = $(this),
						multiple = t.closest('.dwwl').hasClass('dwwms'),
						sc = i == b || b === r,
						res = getValid(that._tempWheelArray[i], t, d, multiple),
						cell = res.cell;
					if (!(cell.hasClass('dw-sel')) || sc) {
						that._tempWheelArray[i] = res.val;
						if (!multiple) {
							$('.dw-sel', t).removeAttr('aria-selected');
							cell.attr('aria-selected', 'true')
						}
						$('.dw-sel', t).removeClass('dw-sel');
						cell.addClass('dw-sel');
						scroll(t, i, res.v, sc ? a : 0.1, sc ? e : false)
					}
				});
				trigger('onValidated', []);
				that._tempValue = s.formatResult(that._tempWheelArray);
				//alert("that._tempValue:"+that._tempValue);
				if (that.live) {
					that._hasValue = c || that._hasValue;
					setValue(c, c, 0, true)
				}
				//alert(that._header.html());
				//alert(formatHeader(that._tempValue));
				that._header.html(formatHeader(that._tempValue));
				if (c) {
					trigger('onChange', [that._tempValue])
				}
			}
		}

		function calc(t, a, b, c, d) {
			a = constrain(a, min, max);
			var e = $('.dw-li', t).eq(a),
				o = d === r ? a : d,
				active = d !== r,
				idx = index,
				dist = Math.abs(a - o),
				time = c ? (a == o ? 0.1 : dist * s.timeUnit * Math.max(0.5, (100 - dist) / 100)) : 0;
			that._tempWheelArray[idx] = e.attr('data-val');
			scroll(t, idx, a, time, active);
			setTimeout(function() {
				scrollToPos(time, idx, true, b, active)
			}, 10)
		}

		function plus(t) {
			var a = pos[index] + 1;
			calc(t, a > max ? min : a, 1, true)
		}

		function minus(t) {
			var a = pos[index] - 1;
			calc(t, a < min ? max : a, 2, true)
		}

		function setValue(a, b, c, d, e) {
			if (that._isVisible && !d) {
				scrollToPos(c)
			}
			that._tempValue = s.formatResult(that._tempWheelArray);
			if (!e) {
				that._wheelArray = that._tempWheelArray.slice(0);
				that._value = that._hasValue ? that._tempValue : null
			}
			if (a) {
				trigger('onValueFill', [that._hasValue ? that._tempValue : '', b]);
				if (that._isInput) {
					$elm.val(that._hasValue ? that._tempValue : '')
				}
				if (b) {
					that._preventChange = true;
					$elm.change()
				}
			}
		}
		classes.Widget.call(this, f, g, true);
		that.setVal = that._setVal = function(a, b, c, d, e) {
			that._hasValue = a !== null && a !== r;
			that._tempWheelArray = $.isArray(a) ? a.slice(0) : s.parseValue.call(f, a, that) || [];
			setValue(b, c === r ? b : c, e, false, d)
		};
		that.getVal = that._getVal = function(a) {
			var b = that._hasValue ? that[a ? '_tempValue' : '_value'] : null;
			return util.isNumeric(b) ? +b : b
		};
		that.setArrayVal = that.setVal;
		that.getArrayVal = function(a) {
			return a ? that._tempWheelArray : that._wheelArray
		};
		that.setValue = function(a, b, c, d, e) {
			that.setVal(a, b, e, d, c)
		};
		that.getValue = that.getArrayVal;
		that.changeWheel = function(b, c, d) {
			if (m) {
				var i = 0,
					nr = b.length;
				$.each(s.wheels, function(j, a) {
					$.each(a, function(k, w) {
						if ($.inArray(i, b) > -1) {
							wheels[i] = w;
							$('.dw-ul', m).eq(i).html(generateWheelItems(i));
							nr--;
							if (!nr) {
								that.position();
								scrollToPos(c, r, d);
								return false
							}
						}
						i++
					});
					if (!nr) {
						return false
					}
				})
			}
		};
		that.getValidCell = getValid;
		that._generateContent = function() {
			var b, html = '',
				l = 0;
			$.each(s.wheels, function(i, a) {
				html += '<div class="mbsc-w-p dwc' + (s.mode != 'scroller' ? ' dwpm' : ' dwsc') + (s.showLabel ? '' : ' dwhl') + '">' + '<div class="dwwc"' + (s.maxWidth ? '' : ' style="max-width:600px;"') + '>' + (hasFlex ? '' : '<table class="dw-tbl" cellpadding="0" cellspacing="0"><tr>');
				$.each(a, function(j, w) {
					wheels[l] = w;
					b = w.label !== r ? w.label : j;
					html += '<' + (hasFlex ? 'div' : 'td') + ' class="dwfl"' + ' style="' + (s.fixedWidth ? ('width:' + (s.fixedWidth[l] || s.fixedWidth) + 'px;') : (s.minWidth ? ('min-width:' + (s.minWidth[l] || s.minWidth) + 'px;') : 'min-width:' + s.width + 'px;') + (s.maxWidth ? ('max-width:' + (s.maxWidth[l] || s.maxWidth) + 'px;') : '')) + '">' + '<div class="dwwl dwwl' + l + (w.multiple ? ' dwwms' : '') + '">' + (s.mode != 'scroller' ? '<div class="dwb-e dwwb dwwbp ' + (s.btnPlusClass || '') + '" style="height:' + itemHeight + 'px;line-height:' + itemHeight + 'px;"><span>+</span></div>' + '<div class="dwb-e dwwb dwwbm ' + (s.btnMinusClass || '') + '" style="height:' + itemHeight + 'px;line-height:' + itemHeight + 'px;"><span>&ndash;</span></div>' : '') + '<div class="dwl">' + b + '</div>' + '<div tabindex="0" aria-live="off" aria-label="' + b + '" role="listbox" class="dwww">' + '<div class="dww" style="height:' + (s.rows * itemHeight) + 'px;">' + '<div class="dw-ul" style="margin-top:' + (w.multiple ? 0 : s.rows / 2 * itemHeight - itemHeight / 2) + 'px;">';
					html += generateWheelItems(l) + '</div></div><div class="dwwo"></div></div><div class="dwwol"' + (s.selectedLineHeight ? ' style="height:' + itemHeight + 'px;margin-top:-' + (itemHeight / 2 + (s.selectedLineBorder || 0)) + 'px;"' : '') + '></div></div>' + (hasFlex ? '</div>' : '</td>');
					l++
				});
				html += (hasFlex ? '' : '</tr></table>') + '</div></div>'
			});
			return html
		};
		that._attachEvents = function(a) {
			a.on('DOMMouseScroll mousewheel', '.dwwl', onScroll).on('keydown', '.dwwl', onKeyDown).on('keyup', '.dwwl', onKeyUp).on('touchstart mousedown', '.dwwl', onStart).on('touchmove', '.dwwl', onMove).on('touchend', '.dwwl', onEnd).on('touchstart mousedown', '.dwwb', onBtnStart).on('touchend', '.dwwb', onBtnEnd)
		};
		that._markupReady = function() {
			m = that._markup;
			scrollToPos()
		};
		that._fillValue = function() {
			that._hasValue = true;
			setValue(true, true, 0, true)
		};
		that._readValue = function() {
			var v = $elm.val() || '';
			that._hasValue = v !== '';
			that._tempWheelArray = that._wheelArray ? that._wheelArray.slice(0) : s.parseValue(v, that) || [];
			setValue()
		};
		that._processSettings = function() {
			s = that.settings;
			trigger = that.trigger;
			itemHeight = s.height;
			lines = s.multiline;
			that._isLiquid = (s.layout || (/top|bottom/.test(s.display) && s.wheels.length == 1 ? 'liquid' : '')) === 'liquid';
			if (lines > 1) {
				s.cssClass = (s.cssClass || '') + ' dw-ml'
			}
		};
		that._selectedValues = {};
		if (!h) {
			instances[f.id] = that;
			that.init(g)
		}
	};
	classes.Scroller.prototype._class = 'scroller';
	classes.Scroller.prototype._defaults = $.extend({}, classes.Widget.prototype._defaults, {
		minWidth: 80,
		height: 40,
		rows: 3,
		multiline: 1,
		delay: 300,
		readonly: false,
		showLabel: true,
		wheels: [],
		mode: 'scroller',
		preset: '',
		speedUnit: 0.0012,
		timeUnit: 0.08,
		formatResult: function(d) {
			return d.join(' ')
		},
		parseValue: function(b, c) {
			var d = [],
				ret = [],
				i = 0,
				keys;
			if (b !== null && b !== r) {
				d = (b + '').split(' ')
			}
			$.each(c.settings.wheels, function(j, a) {
				$.each(a, function(k, w) {
					keys = w.keys || w.values;
					if ($.inArray(d[i], keys) !== -1) {
						ret.push(d[i])
					} else {
						ret.push(keys[0])
					}
					i++
				})
			});
			return ret
		}
	})
})(jQuery, window, document);
(function($) {
	var b = $.mobiscroll.themes,
		theme = {
			display: 'bottom',
			dateOrder: 'MMdyy',
			rows: 5,
			height: 34,
			minWidth: 55,
			headerText: false,
			showLabel: false,
			btnWidth: false,
			selectedLineHeight: true,
			selectedLineBorder: 1,
			useShortLabels: true,
			deleteIcon: 'backspace3',
			checkIcon: 'ion-ios7-checkmark-empty',
			btnCalPrevClass: 'mbsc-ic mbsc-ic-arrow-left5',
			btnCalNextClass: 'mbsc-ic mbsc-ic-arrow-right5',
			btnPlusClass: 'mbsc-ic mbsc-ic-arrow-down5',
			btnMinusClass: 'mbsc-ic mbsc-ic-arrow-up5',
			onThemeLoad: function(a, s) {
				if (s.theme) {
					s.theme = s.theme.replace('ios7', 'ios')
				}
			}
		};
	b.ios = theme;
	b.ios7 = theme
})(jQuery);
(function(a, j) {
	var q = a.mobiscroll,
		t = q.util,
		k = t.constrain,
		h = t.jsPrefix,
		c = t.prefix,
		f = t.has3d,
		H = t.getCoord,
		w = t.getPosition,
		g = t.testTouch,
		n = t.isNumeric,
		N = t.isString,
		b = a.extend,
		O = window.requestAnimationFrame ||
	function(a) {
		a()
	}, z = window.cancelAnimationFrame ||
	function() {}, G = {
		speedUnit: 0.0022,
		timeUnit: 0.8,
		axis: "Y",
		momentum: !0,
		elastic: !0
	};
	q.classes.ScrollView = function(t, E, x) {
		function o(d) {
			if (jQuery.mobiscroll.multiInst && g(d, this) && !L && ("mousedown" == d.type && d.preventDefault(), r && r.removeClass("mbsc-btn-a"), s = !1, r = a(d.target).closest(".mbsc-btn", this), r.length && !r.hasClass("mbsc-btn-d") && (s = !0, K = setTimeout(function() {
				r.addClass("mbsc-btn-a")
			}, 100)), L = !0, X = !1, aa.scrolled = i, D = H(d, "X"), la = H(d, "Y"), $ = D, p = T = A = 0, ea = new Date, ca = +w(ba, fa) || 0, J(ca, 1), "mousedown" === d.type)) a(document).on("mousemove", l).on("mouseup", I)
		}

		function l(a) {
			if (L) {
				$ = H(a, "X");
				P = H(a, "Y");
				A = $ - D;
				T = P - la;
				p = fa ? T : A;
				if (s && (5 < Math.abs(T) || 5 < Math.abs(A))) clearTimeout(K), r.removeClass("mbsc-btn-a"), s = !1;
				!X && 5 < Math.abs(p) && (aa.scrolled = !0, S || (S = !0, Q = O(C)));
				fa || (aa.scrolled ? a.preventDefault() : 7 < Math.abs(T) && (X = !0, ia.trigger("touchend")))
			}
		}

		function C() {
			da.snap && da.maxSnapScroll && (p = k(p, -d, d));
			J(k(ca + p, Y - y, F + y));
			S = !1
		}

		function I(b) {
			if (L) {
				var c;
				c = new Date - ea;
				z(Q);
				S = !1;
				if (!X && aa.scrolled) {
					da.momentum && f && 300 > c && (c = p / c, p = Math.max(Math.abs(p), c * c / da.speedUnit) * (0 > p ? -1 : 1));
					da.snap && da.maxSnapScroll && (p = k(p, -d, d));
					m = Math.round((ca + p) / d);
					e = k(m * d, Y, F);
					if (R) {
						if (0 > p) for (c = R.length - 1; 0 <= c; c--) {
							if (Math.abs(e) + M >= R[c].breakpoint) {
								m = c;
								ja = 2;
								e = R[c].snap2;
								break
							}
						} else if (0 <= p) for (c = 0; c < R.length; c++) if (Math.abs(e) <= R[c].breakpoint) {
							m = c;
							ja = 1;
							e = R[c].snap1;
							break
						}
						e = k(e, Y, F)
					}
					J(e, da.time || (ta < Y || ta > F ? 200 : Math.max(200, Math.abs(e - ta) * da.timeUnit)))
				}
				s && (clearTimeout(K), r.addClass("mbsc-btn-a"), setTimeout(function() {
					r.removeClass("mbsc-btn-a")
				}, 100), !X && !aa.scrolled && U("onBtnTap", [r]));
				"mouseup" == b.type && a(document).off("mousemove", l).off("mouseup", I);
				L = !1
			}
		}

		function J(a, d) {
			ta = a;
			ha = ba[0].style;
			f ? (ha[h + "Transform"] = "translate3d(" + (fa ? "0," + a + "px," : a + "px,0,") + "0)", ha[h + "Transition"] = c + "transform " + Math.round(d || 0) + "ms ease-out") : ha[B] = a + "px";
			i = !0;
			setTimeout(function() {
				i = false
			}, d || 0)
		}

		function U(d, b) {
			var c;
			b.push(aa);
			a.each([E], function(a, e) {
				e && e[d] && (c = e[d].apply(t, b))
			});
			return c
		}
		var r, K, M, A, T, p, B, e, y, $, P, s, F, Y, X, L, i, Q, S, d, R, ca, ea, D, la, ha, ba, fa, aa = this,
			ta = 0,
			m = 0,
			ja = 1,
			da = E,
			ia = a(t);
		aa.scrolled = !1;
		aa.scroll = function(b, c) {
			b = n(b) ? Math.round(b / d) * d : Math.ceil((a(b, t).length ? Math.round(ba.offset()[B] - a(b, t).offset()[B]) : ta) / d) * d;
			J(k(b, Y, F), c)
		};
		aa.refresh = function() {
			var a;
			M = da.contSize === j ? fa ? ia.height() : ia.width() : da.contSize;
			Y = da.minScroll === j ? fa ? M - ba.height() : M - ba.width() : da.minScroll;
			F = da.maxScroll === j ? 0 : da.maxScroll;
			!fa && da.rtl && (a = F, F = -Y, Y = -a);
			N(da.snap) && (R = [], ba.find(da.snap).each(function() {
				var a = fa ? this.offsetTop : this.offsetLeft,
					d = fa ? this.offsetHeight : this.offsetWidth;
				R.push({
					breakpoint: a + d / 2,
					snap1: -a,
					snap2: M - a - d
				})
			}));
			d = n(da.snap) ? da.snap : 1;
			y = da.elastic ? n(da.snap) ? d : n(da.elastic) ? da.elastic : 0 : 0;
			aa.scroll(da.snap ? R ? R[m]["snap" + ja] : m * d : ta)
		};
		aa.init = function(a) {
			aa.settings = da = {};
			b(E, a);
			b(da, q.defaults, G, E);
			B = (fa = "Y" == da.axis) ? "top" : "left";
			ba = da.moveElement || ia.children().eq(0);
			aa.refresh();
			ia.on("touchstart mousedown", o).on("touchmove", l).on("touchend touchcancel", I);
			t.addEventListener && t.addEventListener("click", function(a) {
				aa.scrolled && (a.stopPropagation(), a.preventDefault())
			}, !0)
		};
		aa.destroy = function() {
			ia.off("touchstart mousedown", o).off("touchmove", l).off("touchend touchcancel", I);
			aa = null
		};
		aa.getInst = function() {
			return aa
		};
		x || aa.init(E)
	};
	q.presetShort("scrollview", "ScrollView", !1)
})(jQuery);
(function(a, j, q, t) {
	var k = a.mobiscroll,
		h = k.presets.scroller,
		c = k.util,
		f = c.has3d,
		H = c.jsPrefix,
		w = c.getCoord,
		g = c.testTouch,
		n = "webkitTransitionEnd transitionend",
		N = {
			controls: ["calendar"],
			firstDay: 0,
			weekDays: "short",
			maxMonthWidth: 170,
			months: 1,
			preMonths: 1,
			highlight: !0,
			swipe: !0,
			liveSwipe: !0,
			divergentDayChange: !0,
			quickNav: !0,
			navigation: "yearMonth",
			dateText: "Date",
			timeText: "Time",
			calendarText: "Calendar",
			todayText: "Today",
			prevMonthText: "Previous Month",
			nextMonthText: "Next Month",
			prevYearText: "Previous Year",
			nextYearText: "Next Year",
			btnCalPrevClass: "mbsc-ic mbsc-ic-arrow-left6",
			btnCalNextClass: "mbsc-ic mbsc-ic-arrow-right6"
		};
	h.calbase = function(b) {
		function O(b, d, e) {
			var c, f, i, g, h = {},
				j = ka + pb;
			b && a.each(b, function(a, b) {
				c = b.d || b.start || b;
				f = c + "";
				if (b.start && b.end) for (g = new Date(b.start); g <= b.end;) i = new Date(g.getFullYear(), g.getMonth(), g.getDate()), h[i] = h[i] || [], h[i].push(b), g.setDate(g.getDate() + 1);
				else if (c.getTime) i = new Date(c.getFullYear(), c.getMonth(), c.getDate()), h[i] = h[i] || [], h[i].push(b);
				else if (f.match(/w/i)) {
					var k = +f.replace("w", ""),
						l = 0,
						p = u.getDate(d, e - ka - va, 1).getDay();
					1 < u.firstDay - p + 1 && (l = 7);
					for (F = 0; F < 5 * Ra; F++) i = u.getDate(d, e - ka - va, 7 * F - l - p + 1 + k), h[i] = h[i] || [], h[i].push(b)
				} else if (f = f.split("/"), f[1]) 11 <= e + j && (i = u.getDate(d + 1, f[0] - 1, f[1]), h[i] = h[i] || [], h[i].push(b)), 1 >= e - j && (i = u.getDate(d - 1, f[0] - 1, f[1]), h[i] = h[i] || [], h[i].push(b)), i = u.getDate(d, f[0] - 1, f[1]), h[i] = h[i] || [], h[i].push(b);
				else for (F = 0; F < Ra; F++) i = u.getDate(d, e - ka - va + F, f[0]), h[i] = h[i] || [], h[i].push(b)
			});
			return h
		}

		function z(a, d) {
			sb = O(u.invalid, a, d);
			bb = O(u.valid, a, d);
			b.onGenMonth(a, d)
		}

		function G(a, b, d, e, c, i, f) {
			var g = '<div class="dw-cal-h dw-cal-sc-c dw-cal-' + a + "-c " + (u.calendarClass || "") + '"><div class="dw-cal-sc"><div class="dw-cal-sc-p"><div class="dw-cal-sc-tbl"><div class="dw-cal-sc-row">';
			for (s = 1; s <= b; s++) g = 12 >= s || s > d ? g + '<div class="dw-cal-sc-m-cell dw-cal-sc-cell dw-cal-sc-empty"><div class="dw-i">&nbsp;</div></div>' : g + ('<div tabindex="0" role="button"' + (i ? ' aria-label="' + i[s - 13] + '"' : "") + ' class="dwb-e dwb-nhl dw-cal-sc-m-cell dw-cal-sc-cell dw-cal-' + a + '-s" data-val=' + (e + s - 13) + '><div class="dw-i dw-cal-sc-tbl"><div class="dw-cal-sc-cell">' + (f ? f[s - 13] : e + s - 13 + c) + "</div></div></div>"), s < b && (0 === s % 12 ? g += '</div></div></div><div class="dw-cal-sc-p" style="' + (ra ? "top" : $a ? "right" : "left") + ":" + 100 * Math.round(s / 12) + '%"><div class="dw-cal-sc-tbl"><div class="dw-cal-sc-row">' : 0 === s % 3 && (g += '</div><div class="dw-cal-sc-row">'));
			return g + "</div></div></div></div></div>"
		}

		function v(d, e) {
			var c, i, f, g, h, j, k, l, p, s, m, D, ia, F, o = 1,
				B = 0;
			c = u.getDate(d, e, 1);
			var y = u.getYear(c),
				r = u.getMonth(c),
				R = null === u.defaultValue && !b._hasValue ? null : b.getDate(!0),
				n = u.getDate(y, r, 1).getDay(),
				xa = '<div class="dw-cal-table">',
				q = '<div class="dw-week-nr-c">';
			1 < u.firstDay - n + 1 && (B = 7);
			for (F = 0; 42 > F; F++) ia = F + u.firstDay - B, c = u.getDate(y, r, ia - n + 1), i = c.getFullYear(), f = c.getMonth(), g = c.getDate(), h = u.getMonth(c), j = u.getDay(c), D = u.getMaxDayOfMonth(i, f), k = i + "-" + f + "-" + g, f = a.extend({
				valid: c < new Date(lb.getFullYear(), lb.getMonth(), lb.getDate()) || c > xb ? !1 : sb[c] === t || bb[c] !== t,
				selected: R && R.getFullYear() === i && R.getMonth() === f && R.getDate() === g
			}, b.getDayProps(c, R)), l = f.valid, p = f.selected, i = f.cssClass, s = c.getTime() === (new Date).setHours(0, 0, 0, 0), m = h !== r, zb[k] = f, 0 === F % 7 && (xa += (F ? "</div>" : "") + '<div class="dw-cal-row' + (u.highlight && R && 0 <= R - c && 6048E5 > R - c ? " dw-cal-week-hl" : "") + '">', V && ("month" == V && m && F ? o = 1 == g ? 1 : 2 : "year" == V && (o = u.getWeekNumber(c)), q += '<div class="dw-week-nr"><div class="dw-week-nr-i">' + o + "</div></div>", o++)), xa += '<div role="button" tabindex="-1" aria-label="' + (s ? u.todayText + ", " : "") + u.dayNames[c.getDay()] + ", " + u.monthNames[h] + " " + j + " " + (f.ariaLabel ? ", " + f.ariaLabel : "") + '"' + (m && !eb ? ' aria-hidden="true"' : "") + (p ? ' aria-selected="true"' : "") + (l ? "" : ' aria-disabled="true"') + ' data-day="' + ia % 7 + '" data-full="' + k + '"class="dw-cal-day ' + (s ? " dw-cal-today" : "") + (u.dayClass || "") + (p ? " dw-sel" : "") + (i ? " " + i : "") + (1 == j ? " dw-cal-day-first" : "") + (j == D ? " dw-cal-day-last" : "") + (m ? " dw-cal-day-diff" : "") + (l ? " dw-cal-day-v dwb-e dwb-nhl" : " dw-cal-day-inv") + '"><div class="dw-i ' + (p ? Ta : "") + " " + (u.innerDayClass || "") + '"><div class="dw-cal-day-fg">' + j + "</div>" + (f.markup || "") + '<div class="dw-cal-day-frame"></div></div></div>';
			return xa + ("</div>" + q + "</div></div>")
		}

		function E(b, d, e) {
			var c = u.getDate(b, d, 1),
				f = u.getYear(c),
				c = u.getMonth(c),
				i = f + Wa;
			if (La) {
				Qa && Qa.removeClass("dw-sel").removeAttr("aria-selected").find(".dw-i").removeClass(Ta);
				cb && cb.removeClass("dw-sel").removeAttr("aria-selected").find(".dw-i").removeClass(Ta);
				Qa = a('.dw-cal-year-s[data-val="' + f + '"]', L).addClass("dw-sel").attr("aria-selected", "true");
				cb = a('.dw-cal-month-s[data-val="' + c + '"]', L).addClass("dw-sel").attr("aria-selected", "true");
				Qa.find(".dw-i").addClass(Ta);
				cb.find(".dw-i").addClass(Ta);
				Pa && Pa.scroll(Qa, e);
				a(".dw-cal-month-s", L).removeClass("dwb-d");
				if (f === ia) for (s = 0; s < Va; s++) a('.dw-cal-month-s[data-val="' + s + '"]', L).addClass("dwb-d");
				if (f === xa) for (s = Ba + 1; 12 >= s; s++) a('.dw-cal-month-s[data-val="' + s + '"]', L).addClass("dwb-d")
			}
			1 == ja.length && ja.attr("aria-label", f).html(i);
			for (s = 0; s < sa; ++s) c = u.getDate(b, d - va + s, 1), f = u.getYear(c), c = u.getMonth(c), i = f + Wa, a(ta[s]).attr("aria-label", u.monthNames[c] + (db ? "" : " " + f)).html((!db && da < m ? i + " " : "") + fa[c] + (!db && da > m ? " " + i : "")), 1 < ja.length && a(ja[s]).html(i);
			u.getDate(b, d - va - 1, 1) < qa ? o(a(".dw-cal-prev-m", L)) : x(a(".dw-cal-prev-m", L));
			u.getDate(b, d + sa - va, 1) > Ga ? o(a(".dw-cal-next-m", L)) : x(a(".dw-cal-next-m", L));
			u.getDate(b, d, 1).getFullYear() <= qa.getFullYear() ? o(a(".dw-cal-prev-y", L)) : x(a(".dw-cal-prev-y", L));
			u.getDate(b, d, 1).getFullYear() >= Ga.getFullYear() ? o(a(".dw-cal-next-y", L)) : x(a(".dw-cal-next-y", L))
		}

		function x(a) {
			a.removeClass(rb).find(".dw-cal-btn-txt").removeAttr("aria-disabled")
		}

		function o(a) {
			a.addClass(rb).find(".dw-cal-btn-txt").attr("aria-disabled", "true")
		}

		function l(d, c) {
			if (pa.calendar && ("calendar" === Ha || c)) {
				var e, f, i = u.getDate(ga, ma, 1),
					g = Math.abs(12 * (u.getYear(d) - u.getYear(i)) + u.getMonth(d) - u.getMonth(i));
				b.needsSlide && g && (ga = u.getYear(d), ma = u.getMonth(d), d > i ? (f = g > ka - va + sa - 1, ma -= f ? 0 : g - ka, e = "next") : d < i && (f = g > ka + va, ma += f ? 0 : g - ka, e = "prev"), I.call(this, ga, ma, e, Math.min(g, ka), f, !0));
				c || (b.trigger("onDayHighlight", [d]), u.highlight && (a(".dw-sel .dw-i", Q).removeClass(Ta), a(".dw-sel", Q).removeClass("dw-sel").removeAttr("aria-selected"), a(".dw-cal-week-hl", Q).removeClass("dw-cal-week-hl"), (null !== u.defaultValue || b._hasValue) && a('.dw-cal-day[data-full="' + d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + '"]', Q).addClass("dw-sel").attr("aria-selected", "true").find(".dw-i").addClass(Ta).closest(".dw-cal-row").addClass("dw-cal-week-hl")));
				b.needsSlide = !0
			}
		}

		function C(a, d) {
			z(a, d);
			for (s = 0; s < Ra; s++) na[s].html(v(a, d - va - ka + s));
			b.needsRefresh = !1
		}

		function I(c, e, f, i, g, h, j) {
			c && yb.push({
				y: c,
				m: e,
				dir: f,
				slideNr: i,
				load: g,
				active: h,
				callback: j
			});
			if (!Da) {
				var k = yb.shift(),
					c = k.y,
					e = k.m,
					f = "next" === k.dir,
					i = k.slideNr,
					g = k.load,
					h = k.active,
					j = k.callback || fb,
					k = u.getDate(c, e, 1),
					c = u.getYear(k),
					e = u.getMonth(k);
				Da = !0;
				b.changing = !0;
				b.trigger("onMonthChange", [c, e]);
				z(c, e);
				if (g) for (s = 0; s < sa; s++) na[f ? Ra - sa + s : s].html(v(c, e - va + s));
				h && nb.addClass("dw-cal-slide-a");
				setTimeout(function() {
					b.ariaMessage(u.monthNames[e] + " " + c);
					E(c, e, 200);
					U(f ? ca - d * i * Za : ca + d * i * Za, ba.hasClass("dw-cal-v") ? 200 : 0, function() {
						var d;
						nb.removeClass("dw-cal-slide-a").attr("aria-hidden", "true");
						if (f) {
							d = na.splice(0, i);
							for (s = 0; s < i; s++) na.push(d[s]), r(na[na.length - 1], +na[na.length - 2].data("curr") + 100 * Za)
						} else {
							d = na.splice(Ra - i, i);
							for (s = i - 1; 0 <= s; s--) na.unshift(d[s]), r(na[0], +na[1].data("curr") - 100 * Za)
						}
						for (s = 0; s < i; s++) na[f ? Ra - i + s : s].html(v(c, e - va - ka + s + (f ? Ra - i : 0))), g && na[f ? s : Ra - i + s].html(v(c, e - va - ka + s + (f ? 0 : Ra - i)));
						for (s = 0; s < sa; s++) na[ka + s].addClass("dw-cal-slide-a").removeAttr("aria-hidden");
						Da = !1;
						yb.length ? setTimeout(function() {
							I()
						}, 10) : (ga = c, ma = e, b.changing = !1, V && ha.html(a(".dw-week-nr-c", na[ka]).html()), a(".dw-cal-day", L).attr("tabindex", -1), a(".dw-cal-slide-a .dw-cal-day", L).attr("tabindex", 0), b.needsRefresh && K(), b.trigger("onMonthLoaded", [c, e]), j())
					})
				}, 10)
			}
		}

		function J() {
			var d = a(this),
				c = b.live,
				e = b.getDate(!0),
				f = d.attr("data-full"),
				i = f.split("-"),
				i = new Date(i[0], i[1], i[2]),
				e = new Date(i.getFullYear(), i.getMonth(), i.getDate(), e.getHours(), e.getMinutes(), e.getSeconds()),
				g = d.hasClass("dw-sel");
			if ((eb || !d.hasClass("dw-cal-day-diff")) && !1 !== b.trigger("onDayChange", [a.extend(zb[f], {
				date: e,
				cell: this,
				selected: g
			})])) b.needsSlide = !1, ea = !0, b.setDate(e, c, 0.2, !c, !0), u.divergentDayChange && (Ca = !0, i < u.getDate(ga, ma - va, 1) ? A() : i > u.getDate(ga, ma - va + sa, 0) && M(), Ca = !1)
		}

		function U(a, b, c) {
			var e = S[0].style,
				a = Math.max(ca - d * ka, Math.min(a, ca + d * ka));
			e[H + "Transition"] = "all " + (b || 0) + "ms";
			if (f) {
				if (c) if (R == a || !b) setTimeout(c, 200);
				else S.on(n, function() {
					S.off(n);
					e[H + "Transition"] = "";
					c()
				});
				e[H + "Transform"] = "translate3d(" + (ra ? "0," + a + "px," : a + "px,0,") + "0)"
			} else c && setTimeout(c, b || 200), e[ra ? "top" : "left"] = a + "px";
			if (b || c) ca = a;
			R = a
		}

		function r(a, d) {
			a.data("curr", d);
			f ? a[0].style[H + "Transform"] = "translate3d(" + (ra ? "0," + d + "%," : d + "%,0,") + "0)" : a[0].style[ra ? "top" : "left"] = d + "%"
		}

		function K() {
			b.isVisible() && pa.calendar && C(ga, ma)
		}

		function M() {
			Ca && u.getDate(ga, ma + sa - va, 1) <= Ga && I(ga, ++ma, "next", 1, !1, !0, M)
		}

		function A() {
			Ca && u.getDate(ga, ma - va - 1, 1) >= qa && I(ga, --ma, "prev", 1, !1, !0, A)
		}

		function T(a) {
			Ca && u.getDate(ga, ma, 1) <= u.getDate(u.getYear(Ga) - 1, u.getMonth(Ga) - pb, 1) ? I(++ga, ma, "next", ka, !0, !0, function() {
				T(a)
			}) : Ca && !a.hasClass("dwb-d") && I(u.getYear(Ga), u.getMonth(Ga) - pb, "next", ka, !0, !0)
		}

		function p(a) {
			Ca && u.getDate(ga, ma, 1) >= u.getDate(u.getYear(qa) + 1, u.getMonth(qa) + va, 1) ? I(--ga, ma, "prev", ka, !0, !0, function() {
				p(a)
			}) : Ca && !a.hasClass("dwb-d") && I(u.getYear(qa), u.getMonth(qa) + va, "prev", ka, !0, !0)
		}

		function B(a, d) {
			a.hasClass("dw-cal-v") || (a.addClass("dw-cal-v" + (d ? "" : " dw-cal-p-in")).removeClass("dw-cal-p-out dw-cal-h"), b.trigger("onSelectShow", []))
		}

		function e(a, d) {
			a.hasClass("dw-cal-v") && a.removeClass("dw-cal-v dw-cal-p-in").addClass("dw-cal-h" + (d ? "" : " dw-cal-p-out"))
		}

		function y(a, d) {
			(d || a).hasClass("dw-cal-v") ? e(a) : B(a)
		}

		function $() {
			a(this).removeClass("dw-cal-p-out dw-cal-p-in")
		}
		var P, s, F, Y, X, L, i, Q, S, d, R, ca, ea, D, la, ha, ba, fa, aa, ta, m, ja, da, ia, xa, Va, Ba, qa, Ga, lb, xb, ua, ga, ma, Fa, mb, gb, tb, Ea, Xa, Bb, bb, sb, Aa, Ha, Na, Da, Ca, sa, Ra, pb, va, eb, Pa, Qa, cb, Ib = this,
			nb = [],
			na = [],
			yb = [],
			pa = {},
			zb = {},
			fb = function() {},
			Sa = a.extend({}, b.settings),
			u = a.extend(b.settings, N, Sa),
			ib = "full" == u.weekDays ? "" : "min" == u.weekDays ? "Min" : "Short",
			V = u.weekCounter,
			za = u.layout || (/top|bottom/.test(u.display) ? "liquid" : ""),
			oa = "liquid" == za && "bubble" !== u.display,
			ub = "modal" == u.display,
			$a = u.rtl,
			Za = $a ? -1 : 1,
			kb = oa ? null : u.calendarWidth,
			ra = "vertical" == u.swipeDirection,
			La = u.quickNav,
			ka = u.preMonths,
			db = "yearMonth" == u.navigation,
			ob = u.controls.join(","),
			Ja = (!0 === u.tabs || !1 !== u.tabs && oa) && 1 < u.controls.length,
			ya = !Ja && u.tabs === t && !oa && 1 < u.controls.length,
			Wa = u.yearSuffix || "",
			Ta = u.activeClass || "",
			hb = "dw-sel " + (u.activeTabClass || ""),
			wb = u.activeTabInnerClass || "",
			rb = "dwb-d " + (u.disabledClass || ""),
			Z = "",
			Ia = "";
		ob.match(/calendar/) ? pa.calendar = 1 : La = !1;
		ob.match(/date/) && (pa.date = 1);
		ob.match(/time/) && (pa.time = 1);
		pa.calendar && pa.date && (Ja = !0, ya = !1);
		u.layout = za;
		u.preset = (pa.date || pa.calendar ? "date" : "") + (pa.time ? "time" : "");
		if ("inline" == u.display) a(this).closest('[data-role="page"]').on("pageshow", function() {
			b.position()
		});
		b.changing = !1;
		b.needsRefresh = !1;
		b.needsSlide = !0;
		b.getDayProps = fb;
		b.onGenMonth = fb;
		b.prepareObj = O;
		b.refresh = function() {
			b.changing ? b.needsRefresh = true : K()
		};
		b.navigate = function(a, d) {
			var c = b.isVisible();
			if (d && c) l(a, true);
			else {
				ga = u.getYear(a);
				ma = u.getMonth(a);
				if (c) {
					E(ga, ma);
					C(ga, ma)
				}
			}
		};
		Y = h.datetime.call(this, b);
		m = u.dateOrder.search(/m/i);
		da = u.dateOrder.search(/y/i);
		a.extend(Y, {
			ariaMessage: u.calendarText,
			onMarkupReady: function(h) {
				var o, n, H = "";
				L = h;
				i = u.display == "inline" ? a(this).is("div") ? a(this) : a(this).parent() : b.context;
				ua = b.getDate(true);
				if (!ga) {
					ga = u.getYear(ua);
					ma = u.getMonth(ua)
				}
				R = ca = 0;
				la = true;
				Da = false;
				fa = u.monthNames;
				Ha = "calendar";
				if (u.minDate) {
					qa = new Date(u.minDate.getFullYear(), u.minDate.getMonth(), 1);
					lb = u.minDate
				} else lb = qa = new Date(u.startYear, 0, 1);
				if (u.maxDate) {
					Ga = new Date(u.maxDate.getFullYear(), u.maxDate.getMonth(), 1);
					xb = u.maxDate
				} else xb = Ga = new Date(u.endYear, 11, 31, 23, 59, 59);
				h.addClass("dw-calendar" + (f ? "" : " dw-cal-no3d"));
				X = a(".dw", h);
				Aa = a(".dwcc", h);
				pa.date ? pa.date = a(".dwc", L).eq(0) : pa.calendar && a(".dwc", L).eq(0).addClass("dwc-hh");
				if (pa.time) pa.time = a(".dwc", L).eq(1);
				if (pa.calendar) {
					sa = u.months == "auto" ? Math.max(1, Math.min(3, Math.floor((kb || i[0].innerWidth || i.innerWidth()) / 280))) : u.months;
					Ra = sa + 2 * ka;
					pb = Math.floor(sa / 2);
					va = Math.round(sa / 2) - 1;
					eb = u.showDivergentDays === t ? sa < 2 : u.showDivergentDays;
					ra = ra && sa < 2;
					n = '<div class="dw-cal-btnw"><div class="' + ($a ? "dw-cal-next-m" : "dw-cal-prev-m") + ' dw-cal-prev dw-cal-btn dwb dwb-e"><div role="button" tabindex="0" class="dw-cal-btn-txt ' + (u.btnCalPrevClass || "") + '" aria-label="' + u.prevMonthText + '"></div></div>';
					for (s = 0; s < sa; ++s) n = n + ('<div class="dw-cal-btnw-m" style="width: ' + 100 / sa + '%"><span role="button" class="dw-cal-month"></span></div>');
					n = n + ('<div class="' + ($a ? "dw-cal-prev-m" : "dw-cal-next-m") + ' dw-cal-next dw-cal-btn dwb dwb-e"><div role="button" tabindex="0" class="dw-cal-btn-txt ' + (u.btnCalNextClass || "") + '" aria-label="' + u.nextMonthText + '"></div></div></div>');
					db && (H = '<div class="dw-cal-btnw"><div class="' + ($a ? "dw-cal-next-y" : "dw-cal-prev-y") + ' dw-cal-prev dw-cal-btn dwb dwb-e"><div role="button" tabindex="0" class="dw-cal-btn-txt ' + (u.btnCalPrevClass || "") + '" aria-label="' + u.prevYearText + '"></div></div><span role="button" class="dw-cal-year"></span><div class="' + ($a ? "dw-cal-prev-y" : "dw-cal-next-y") + ' dw-cal-next dw-cal-btn dwb dwb-e"><div role="button" tabindex="0" class="dw-cal-btn-txt ' + (u.btnCalNextClass || "") + '" aria-label="' + u.nextYearText + '"></div></div></div>');
					if (La) {
						ia = u.getYear(qa);
						xa = u.getYear(Ga);
						Va = u.getMonth(qa);
						Ba = u.getMonth(Ga);
						Bb = Math.ceil((xa - ia + 1) / 12) + 2;
						Z = G("month", 36, 24, 0, "", u.monthNames, u.monthNamesShort);
						Ia = G("year", Bb * 12, xa - ia + 13, ia, Wa)
					}
					D = '<div class="mbsc-w-p dw-cal-c"><div class="dw-cal ' + (sa > 1 ? " dw-cal-multi " : "") + (V ? " dw-weeks " : "") + (eb ? "" : " dw-hide-diff ") + (u.calendarClass || "") + '"><div class="dw-cal-header"><div class="dw-cal-btnc ' + (db ? "dw-cal-btnc-ym" : "dw-cal-btnc-m") + '">' + (da < m || sa > 1 ? H + n : n + H) + '</div></div><div class="dw-cal-body"><div class="dw-cal-m-c dw-cal-v"><div class="dw-cal-days-c">';
					for (F = 0; F < sa; ++F) {
						D = D + ('<div aria-hidden="true" class="dw-cal-days" style="width: ' + 100 / sa + '%"><table cellpadding="0" cellspacing="0"><tr>');
						for (s = 0; s < 7; s++) D = D + ("<th>" + u["dayNames" + ib][(s + u.firstDay) % 7] + "</th>");
						D = D + "</tr></table></div>"
					}
					D = D + ('</div><div class="dw-cal-anim-c ' + (u.calendarClass || "") + '"><div class="dw-week-nrs-c ' + (u.weekNrClass || "") + '"><div class="dw-week-nrs"></div></div><div class="dw-cal-anim">');
					for (s = 0; s < sa + 2 * ka; s++) D = D + '<div class="dw-cal-slide" aria-hidden="true"></div>';
					D = D + ("</div></div></div>" + Z + Ia + "</div></div></div>");
					pa.calendar = a(D)
				}
				a.each(u.controls, function(d, b) {
					pa[b] = a('<div class="dw-cal-pnl" id="' + (Ib.id + "_dw_pnl_" + d) + '"></div>').append(a('<div class="dw-cal-pnl-i"></div>').append(pa[b])).appendTo(Aa)
				});
				o = '<div class="dw-cal-tabs"><ul role="tablist">';
				a.each(u.controls, function(a, d) {
					pa[d] && (o = o + ('<li role="tab" aria-controls="' + (Ib.id + "_dw_pnl_" + a) + '" class="dw-cal-tab ' + (a ? "" : hb) + '" data-control="' + d + '"><a href="#" class="dwb-e dwb-nhl dw-i ' + (!a ? wb : "") + '">' + u[d + "Text"] + "</a></li>"))
				});
				o = o + "</ul></div>";
				Aa.before(o);
				Q = a(".dw-cal-anim-c", L);
				S = a(".dw-cal-anim", L);
				if (pa.calendar) {
					nb = a(".dw-cal-slide", L).each(function(d, b) {
						na.push(a(b))
					});
					nb.slice(ka, ka + sa).addClass("dw-cal-slide-a").removeAttr("aria-hidden");
					for (s = 0; s < Ra; s++) r(na[s], 100 * (s - ka) * Za);
					C(ga, ma);
					a(".dw-cal-slide-a .dw-cal-day", L).attr("tabindex", 0);
					ha = a(".dw-week-nrs", L).html(a(".dw-week-nr-c", na[ka]).html())
				}
				ta = a(".dw-cal-month", L);
				ja = a(".dw-cal-year", L);
				ba = a(".dw-cal-m-c", L);
				if (La) {
					ba.on("webkitAnimationEnd animationend", $);
					Z = a(".dw-cal-month-c", L).on("webkitAnimationEnd animationend", $);
					Ia = a(".dw-cal-year-c", L).on("webkitAnimationEnd animationend", $);
					a(".dw-cal-sc-p", L);
					Fa = {
						axis: ra ? "Y" : "X",
						contSize: 0,
						snap: 0,
						maxScroll: 0,
						maxSnapScroll: 1,
						rtl: u.rtl,
						time: 200
					};
					Pa = new k.classes.ScrollView(Ia[0], Fa);
					aa = new k.classes.ScrollView(Z[0], Fa)
				}
				setTimeout(function() {
					b.tap(Q, function(d) {
						d = a(d.target);
						if (!Da && !Xa) {
							d = d.closest(".dw-cal-day", this);
							d.hasClass("dw-cal-day-v") && J.call(d[0])
						}
					});
					a(".dw-cal-btn", L).on("touchstart mousedown keydown", function(d) {
						var b = a(this);
						if (d.type !== "keydown") {
							d.preventDefault();
							d = g(d, this)
						} else d = d.keyCode === 32;
						if (!Ca && d && !b.hasClass("dwb-d")) {
							Ca = true;
							b.hasClass("dw-cal-prev-m") ? A() : b.hasClass("dw-cal-next-m") ? M() : b.hasClass("dw-cal-prev-y") ? p(b) : b.hasClass("dw-cal-next-y") && T(b);
							a(q).on("mouseup.dwbtn", function() {
								a(q).off(".dwbtn");
								Ca = false
							})
						}
					}).on("touchend touchcancel keyup", function() {
						Ca = false
					});
					a(".dw-cal-tab", L).on("touchstart click", function(d) {
						var c = a(this);
						if (g(d, this) && !c.hasClass("dw-sel")) {
							Ha = c.attr("data-control");
							a(".dw-cal-pnl", L).removeClass("dw-cal-p-in").addClass("dw-cal-pnl-h");
							a(".dw-cal-tab", L).removeClass(hb).removeAttr("aria-selected").find(".dw-i").removeClass(wb);
							c.addClass(hb).attr("aria-selected", "true").find(".dw-i").addClass(wb);
							pa[Ha].removeClass("dw-cal-pnl-h").addClass("dw-cal-p-in");
							if (Ha === "calendar") {
								P = b.getDate(true);
								(P.getFullYear() !== ua.getFullYear() || P.getMonth() !== ua.getMonth() || P.getDate() !== ua.getDate()) && l(P)
							} else {
								ua = b.getDate(true);
								b.setDate(ua, false, 0, true)
							}
							if (La) {
								e(Ia, true);
								e(Z, true);
								B(ba, true)
							}
							b.trigger("onTabChange", [Ha])
						}
					});
					if (La) {
						b.tap(a(".dw-cal-month", L), function() {
							Ia.hasClass("dw-cal-v") || y(ba);
							y(Z);
							e(Ia)
						});
						b.tap(a(".dw-cal-year", L), function() {
							Ia.hasClass("dw-cal-v") || Pa.scroll(Qa);
							Z.hasClass("dw-cal-v") || y(ba);
							y(Ia);
							e(Z)
						});
						b.tap(a(".dw-cal-month-s", L), function() {
							!aa.scrolled && !a(this).hasClass("dwb-d") && b.navigate(u.getDate(ga, a(this).attr("data-val"), 1))
						});
						b.tap(a(".dw-cal-year-s", L), function() {
							if (!Pa.scrolled) {
								P = u.getDate(a(this).attr("data-val"), ma, 1);
								b.navigate(new Date(c.constrain(P, qa, Ga)))
							}
						});
						b.tap(Ia, function() {
							if (!Pa.scrolled) {
								e(Ia);
								B(ba)
							}
						});
						b.tap(Z, function() {
							if (!aa.scrolled) {
								e(Z);
								B(ba)
							}
						})
					}
				}, 300);
				oa ? h.addClass("dw-cal-liq") : a(".dw-cal", L).width(kb || 280 * sa);
				u.calendarHeight && a(".dw-cal-anim-c", L).height(u.calendarHeight);
				if (u.swipe) {
					var v, z, x, O, E, N, Y, K, ea, bb = u.liveSwipe,
						sb = j.requestAnimationFrame ||
					function(a) {
						a()
					}, za = j.cancelAnimationFrame || fb, wa = function(a) {
						if (!ea && !Da) {
							Na = true;
							Xa = false;
							ea = true;
							O = new Date;
							ca = R;
							mb = w(a, "X");
							gb = w(a, "Y")
						}
					}, ya = function() {
						N = ea = false;
						if (K) {
							K = false;
							za(z);
							x = false;
							v = ra ? Ea - gb : tb - mb;
							E = new Date - O;
							Y = (E < 300 && Math.abs(v) > 50 ? v < 0 ? -ka : ka : Math.round((R - ca) / d)) * Za;
							Y > 0 && u.getDate(ga, ma - Y - va, 1) >= qa ? I(ga, ma - Y, "prev", Y) : Y < 0 && u.getDate(ga, ma - Y + sa - va - 1, 1) <= Ga ? I(ga, ma - Y, "next", -Y) : bb && U(ca, 200)
						}
					}, cb = function(a) {
						Da && a.preventDefault();
						if (ea) {
							tb = w(a, "X");
							Ea = w(a, "Y");
							v = ra ? Ea - gb : tb - mb;
							if (!K && !N) if (Math.abs(v) > 7) Xa = K = true;
							else if (!b.scrollLock && !ra && Math.abs(Ea - gb) > 10) {
								Xa = N = true;
								a.type === "touchmove" && Q.trigger("touchend")
							}
							K && a.preventDefault();
							if (K && bb && !x) {
								x = true;
								z = sb(Ja)
							}
						}
					}, Ja = function() {
						U(ca + v);
						x = false
					};
					Q.on("touchstart", wa).on("touchmove", cb).on("touchend touchcancel", ya).on("mousedown", function(d) {
						if (!Na) {
							wa(d);
							a(q).on("mousemove.dwsw", cb).on("mouseup.dwsw", function() {
								ya();
								a(q).off(".dwsw")
							})
						}
						Na = false
					})
				}
			},
			onShow: function() {
				if (pa.calendar) {
					E(ga, ma);
					b.trigger("onMonthLoaded", [ga, ma])
				}
			},
			onPosition: function(c, e, f) {
				var g, h, j, k = 0,
					l = 0,
					p = 0;
				if (oa) {
					ub && Q.height("");
					Aa.height("");
					S.width("")
				}
				d && (j = d);
				if (d = Math.round(Math.round(parseInt(Q.css(ra ? "height" : "width"))) / sa)) {
					L.removeClass("mbsc-cal-m mbsc-cal-l");
					d > 1024 ? L.addClass("mbsc-cal-l") : d > 640 && L.addClass("mbsc-cal-m")
				}
				if (Ja && (la || oa) || ya) {
					a(".dw-cal-pnl", L).removeClass("dw-cal-pnl-h");
					a.each(pa, function(a, d) {
						g = d.outerWidth();
						k = Math.max(k, g);
						l = Math.max(l, d.outerHeight());
						p = p + g
					});
					if (Ja || ya && p > (i[0].innerWidth || i.innerWidth())) {
						h = true;
						Ha = a(".dw-cal-tabs .dw-sel", L).attr("data-control");
						X.addClass("dw-cal-tabbed")
					} else {
						Ha = "calendar";
						l = k = "";
						X.removeClass("dw-cal-tabbed");
						Aa.css({
							width: "",
							height: ""
						})
					}
				}
				if (oa && ub) {
					b._isFullScreen = true;
					h && Aa.height(pa.calendar.outerHeight());
					c = X.outerHeight();
					f >= c && Q.height(f - c + Q.outerHeight());
					l = Math.max(l, pa.calendar.outerHeight())
				}
				if (h) {
					Aa.css({
						width: oa ? "" : k,
						height: l
					});
					d = Math.round(Math.round(parseInt(Q.css(ra ? "height" : "width"))) / sa)
				}
				if (d) {
					if (db) {
						fa = u.maxMonthWidth > a(".dw-cal-btnw-m", L).width() ? u.monthNamesShort : u.monthNames;
						for (s = 0; s < sa; ++s) a(ta[s]).text(fa[u.getMonth(u.getDate(ga, ma - va + s, 1))])
					}
					S[ra ? "height" : "width"](d);
					if (La) {
						f = Ia[ra ? "height" : "width"]();
						a.extend(Pa.settings, {
							contSize: f,
							snap: f,
							minScroll: (2 - Bb) * f,
							maxScroll: -f
						});
						a.extend(aa.settings, {
							contSize: f,
							snap: f,
							minScroll: -f,
							maxScroll: -f
						});
						Pa.refresh();
						aa.refresh();
						Ia.hasClass("dw-cal-v") && Pa.scroll(Qa)
					}
					if (oa && !la && j) {
						j = ca / j;
						ca = j * d;
						U(ca, 0)
					}
				}
				if (h) {
					a(".dw-cal-pnl", L).addClass("dw-cal-pnl-h");
					pa[Ha].removeClass("dw-cal-pnl-h")
				}
				b.trigger("onCalResize", []);
				la = false
			},
			onHide: function() {
				na = [];
				ma = ga = Ha = null;
				Da = true;
				if (La && Pa && aa) {
					Pa.destroy();
					aa.destroy()
				}
			},
			onValidated: function() {
				var a, d, c;
				d = b.getDate(true);
				if (ea) a = "calendar";
				else for (c in b.order) c && b.order[c] === s && (a = /mdy/.test(c) ? "date" : "time");
				b.trigger("onSetDate", [{
					date: d,
					control: a
				}]);
				l(d);
				ea = false
			}
		});
		return Y
	}
})(jQuery, window, document);
(function(a, j) {
	var q = a.mobiscroll,
		t = q.datetime,
		k = new Date,
		h = {
			startYear: k.getFullYear() - 100,
			endYear: k.getFullYear() + 1,
			separator: " ",
			dateFormat: "mm/dd/yy",
			dateOrder: "mmddy",
			timeWheels: "hhiiA",
			timeFormat: "hh:ii A",
			dayText: "Day",
			yearText: "Year",
			hourText: "Hours",
			minuteText: "Minutes",
			ampmText: "&nbsp;",
			secText: "Seconds",
			nowText: "Now"
		},
		c = function(c) {
			function k(a, d, b) {
				return s[d] !== j ? +a[s[d]] : b !== j ? b : F[d](ca)
			}

			function w(a, d, b, c) {
				a.push({
					values: b,
					keys: d,
					label: c
				})
			}

			function g(a, d, b, c) {
				return Math.min(c, Math.floor(a / d) * d + b)
			}

			function n(a) {
				if (null === a) return a;
				var d = k(a, "h", 0);
				return e.getDate(k(a, "y"), k(a, "m"), k(a, "d", 1), k(a, "a", 0) ? d + 12 : d, k(a, "i", 0), k(a, "s", 0))
			}

			function N(a, d) {
				var c, e, i = !1,
					f = !1,
					g = 0,
					h = 0;
				if (b(a)) return a;
				a < ha && (a = ha);
				a > ba && (a = ba);
				e = c = a;
				if (2 !== d) for (i = b(c); !i && c < ba;) c = new Date(c.getTime() + 864E5), i = b(c), g++;
				if (1 !== d) for (f = b(e); !f && e > ha;) e = new Date(e.getTime() - 864E5), f = b(e), h++;
				return 1 === d && i ? c : 2 === d && f ? e : h < g && f ? e : c
			}

			function b(a) {
				return a < ha || a > ba ? !1 : O(a, X) ? !0 : O(a, Y) ? !1 : !0
			}

			function O(a, d) {
				var b, c, e;
				if (d) for (c = 0; c < d.length; c++) if (b = d[c], e = b + "", !b.start) if (b.getTime) {
					if (a.getFullYear() == b.getFullYear() && a.getMonth() == b.getMonth() && a.getDate() == b.getDate()) return !0
				} else if (e.match(/w/i)) {
					if (e = +e.replace("w", ""), e == a.getDay()) return !0
				} else if (e = e.split("/"), e[1]) {
					if (e[0] - 1 == a.getMonth() && e[1] == a.getDate()) return !0
				} else if (e[0] == a.getDate()) return !0;
				return !1
			}

			function z(a, d, b, c, i, f, g) {
				var h, j, k;
				if (a) for (h = 0; h < a.length; h++) if (j = a[h], k = j + "", !j.start) if (j.getTime) e.getYear(j) == d && e.getMonth(j) == b && (f[e.getDay(j) - 1] = g);
				else if (k.match(/w/i)) {
					k = +k.replace("w", "");
					for (U = k - c; U < i; U += 7) 0 <= U && (f[U] = g)
				} else k = k.split("/"), k[1] ? k[0] - 1 == b && (f[k[1] - 1] = g) : f[k[0] - 1] = g
			}

			function G(b, c, i, f, h, k, l, s, m) {
				var o, F, R, n, y, r, q, t, z, A, H, ca, x, w, G, P, da, O, C = {},
					S = {
						h: ea,
						i: D,
						s: la,
						a: 1
					},
					L = e.getDate(h, k, l),
					Q = ["a", "h", "i", "s"];
				b && (a.each(b, function(a, d) {
					if (d.start && (d.apply = !1, o = d.d, F = o + "", R = F.split("/"), o && (o.getTime && h == e.getYear(o) && k == e.getMonth(o) && l == e.getDay(o) || !F.match(/w/i) && (R[1] && l == R[1] && k == R[0] - 1 || !R[1] && l == R[0]) || F.match(/w/i) && L.getDay() == +F.replace("w", "")))) d.apply = !0, C[L] = !0
				}), a.each(b, function(b, e) {
					H = w = x = 0;
					ca = j;
					q = r = !0;
					G = !1;
					if (e.start && (e.apply || !e.d && !C[L])) {
						n = e.start.split(":");
						y = e.end.split(":");
						for (A = 0; 3 > A; A++) n[A] === j && (n[A] = 0), y[A] === j && (y[A] = 59), n[A] = +n[A], y[A] = +y[A];
						n.unshift(11 < n[0] ? 1 : 0);
						y.unshift(11 < y[0] ? 1 : 0);
						d && (12 <= n[1] && (n[1] -= 12), 12 <= y[1] && (y[1] -= 12));
						for (A = 0; A < c; A++) if ($[A] !== j) {
							t = g(n[A], S[Q[A]], p[Q[A]], B[Q[A]]);
							z = g(y[A], S[Q[A]], p[Q[A]], B[Q[A]]);
							O = da = P = 0;
							d && 1 == A && (P = n[0] ? 12 : 0, da = y[0] ? 12 : 0, O = $[0] ? 12 : 0);
							r || (t = 0);
							q || (z = B[Q[A]]);
							if ((r || q) && t + P < $[A] + O && $[A] + O < z + da) G = !0;
							$[A] != t && (r = !1);
							$[A] != z && (q = !1)
						}
						if (!m) for (A = c + 1; 4 > A; A++) 0 < n[A] && (x = S[i]), y[A] < B[Q[A]] && (w = S[i]);
						G || (t = g(n[c], S[i], p[i], B[i]) + x, z = g(y[c], S[i], p[i], B[i]) - w, r && (H = 0 > t ? 0 : t > B[i] ? a(".dw-li", s).length : v(s, t) + 0), q && (ca = 0 > z ? 0 : z > B[i] ? a(".dw-li", s).length : v(s, z) + 1));
						if (r || q || G) m ? a(".dw-li", s).slice(H, ca).addClass("dw-v") : a(".dw-li", s).slice(H, ca).removeClass("dw-v")
					}
				}))
			}

			function v(d, b) {
				return a(".dw-li", d).index(a('.dw-li[data-val="' + b + '"]', d))
			}

			function E(a) {
				var d, b = [];
				if (null === a || a === j) return a;
				for (d in s) b[s[d]] = F[d](a);
				return b
			}

			function x(a) {
				var d, b, c, e = [];
				if (a) {
					for (d = 0; d < a.length; d++) if (b = a[d], b.start && b.start.getTime) for (c = new Date(b.start); c <= b.end;) e.push(new Date(c.getFullYear(), c.getMonth(), c.getDate())), c.setDate(c.getDate() + 1);
					else e.push(b);
					return e
				}
				return a
			}
			var o = a(this),
				l = {},
				C;
			if (o.is("input")) {
				switch (o.attr("type")) {
				case "date":
					C = "yy-mm-dd";
					break;
				case "datetime":
					C = "yy-mm-ddTHH:ii:ssZ";
					break;
				case "datetime-local":
					C = "yy-mm-ddTHH:ii:ss";
					break;
				case "month":
					C = "yy-mm";
					l.dateOrder = "mmyy";
					break;
				case "time":
					C = "HH:ii:ss"
				}
				var I = o.attr("min"),
					o = o.attr("max");
				I && (l.minDate = t.parseDate(C, I));
				o && (l.maxDate = t.parseDate(C, o))
			}
			var J, U, r, K, M, A, T, p, B, I = a.extend({}, c.settings),
				e = a.extend(c.settings, q.datetime.defaults, h, l, I),
				y = 0,
				$ = [],
				l = [],
				P = [],
				s = {},
				F = {
					y: function(a) {
						return e.getYear(a)
					},
					m: function(a) {
						return e.getMonth(a)
					},
					d: function(a) {
						return e.getDay(a)
					},
					h: function(a) {
						a = a.getHours();
						a = d && 12 <= a ? a - 12 : a;
						return g(a, ea, fa, m)
					},
					i: function(a) {
						return g(a.getMinutes(), D, aa, ja)
					},
					s: function(a) {
						return g(a.getSeconds(), la, ta, da)
					},
					a: function(a) {
						return S && 11 < a.getHours() ? 1 : 0
					}
				},
				Y = e.invalid,
				X = e.valid,
				I = e.preset,
				L = e.dateOrder,
				i = e.timeWheels,
				Q = L.match(/D/),
				S = i.match(/a/i),
				d = i.match(/h/),
				R = "datetime" == I ? e.dateFormat + e.separator + e.timeFormat : "time" == I ? e.timeFormat : e.dateFormat,
				ca = new Date,
				o = e.steps || {},
				ea = o.hour || e.stepHour || 1,
				D = o.minute || e.stepMinute || 1,
				la = o.second || e.stepSecond || 1,
				o = o.zeroBased,
				ha = e.minDate || new Date(e.startYear, 0, 1),
				ba = e.maxDate || new Date(e.endYear, 11, 31, 23, 59, 59),
				fa = o ? 0 : ha.getHours() % ea,
				aa = o ? 0 : ha.getMinutes() % D,
				ta = o ? 0 : ha.getSeconds() % la,
				m = Math.floor(((d ? 11 : 23) - fa) / ea) * ea + fa,
				ja = Math.floor((59 - aa) / D) * D + aa,
				da = Math.floor((59 - aa) / D) * D + aa;
			C = C || R;
			if (I.match(/date/i)) {
				a.each(["y", "m", "d"], function(a, d) {
					J = L.search(RegExp(d, "i")); - 1 < J && P.push({
						o: J,
						v: d
					})
				});
				P.sort(function(a, d) {
					return a.o > d.o ? 1 : -1
				});
				a.each(P, function(a, d) {
					s[d.v] = a
				});
				o = [];
				for (U = 0; 3 > U; U++) if (U == s.y) {
					y++;
					K = [];
					r = [];
					M = e.getYear(ha);
					A = e.getYear(ba);
					for (J = M; J <= A; J++) r.push(J), K.push((L.match(/yy/i) ? J : (J + "").substr(2, 2)) + (e.yearSuffix || ""));
					w(o, r, K, e.yearText)
				} else if (U == s.m) {
					y++;
					K = [];
					r = [];
					for (J = 0; 12 > J; J++) M = L.replace(/[dy]/gi, "").replace(/mm/, (9 > J ? "0" + (J + 1) : J + 1) + (e.monthSuffix || "")).replace(/m/, J + 1 + (e.monthSuffix || "")), r.push(J), K.push(M.match(/MM/) ? M.replace(/MM/, '<span class="dw-mon">' + e.monthNames[J] + "</span>") : M.replace(/M/, '<span class="dw-mon">' + e.monthNamesShort[J] + "</span>"));
					w(o, r, K, e.monthText)
				} else if (U == s.d) {
					y++;
					K = [];
					r = [];
					for (J = 1; 32 > J; J++) r.push(J), K.push((L.match(/dd/i) && 10 > J ? "0" + J : J) + (e.daySuffix || ""));
					w(o, r, K, e.dayText)
				}
				l.push(o)
			}
			if (I.match(/time/i)) {
				T = !0;
				P = [];
				a.each(["h", "i", "s", "a"], function(a, d) {
					a = i.search(RegExp(d, "i")); - 1 < a && P.push({
						o: a,
						v: d
					})
				});
				P.sort(function(a, d) {
					return a.o > d.o ? 1 : -1
				});
				a.each(P, function(a, d) {
					s[d.v] = y + a
				});
				o = [];
				for (U = y; U < y + 4; U++) if (U == s.h) {
					y++;
					K = [];
					r = [];
					for (J = fa; J < (d ? 12 : 24); J += ea) r.push(J), K.push(d && 0 === J ? 12 : i.match(/hh/i) && 10 > J ? "0" + J : J);
					w(o, r, K, e.hourText)
				} else if (U == s.i) {
					y++;
					K = [];
					r = [];
					for (J = aa; 60 > J; J += D) r.push(J), K.push(i.match(/ii/) && 10 > J ? "0" + J : J);
					w(o, r, K, e.minuteText)
				} else if (U == s.s) {
					y++;
					K = [];
					r = [];
					for (J = ta; 60 > J; J += la) r.push(J), K.push(i.match(/ss/) && 10 > J ? "0" + J : J);
					w(o, r, K, e.secText)
				} else U == s.a && (y++, I = i.match(/A/), w(o, [0, 1], I ? [e.amText.toUpperCase(), e.pmText.toUpperCase()] : [e.amText, e.pmText], e.ampmText));
				l.push(o)
			}
			c.getVal = function(a) {
				return c._hasValue || a ? n(c.getArrayVal(a)) : null
			};
			c.setDate = function(a, d, b, e, i) {
				c.setArrayVal(E(a), d, i, e, b)
			};
			c.getDate = c.getVal;
			c.format = R;
			c.order = s;
			c.handlers.now = function() {
				c.setDate(new Date, !1, 0.3, !0, !0)
			};
			c.buttons.now = {
				text: e.nowText,
				handler: "now"
			};
			Y = x(Y);
			X = x(X);
			ha = n(E(ha));
			ba = n(E(ba));
			p = {
				y: ha.getFullYear(),
				m: 0,
				d: 1,
				h: fa,
				i: aa,
				s: ta,
				a: 0
			};
			B = {
				y: ba.getFullYear(),
				m: 11,
				d: 31,
				h: m,
				i: ja,
				s: da,
				a: 1
			};
			return {
				wheels: l,
				headerText: e.headerText ?
				function() {
					return t.formatDate(R, n(c.getArrayVal(!0)), e)
				} : !1,
				formatResult: function(a) {
					return t.formatDate(C, n(a), e)
				},
				parseValue: function(a) {
					return E(a ? t.parseDate(C, a, e) : e.defaultValue || new Date)
				},
				validate: function(d, b, i, g) {
					var b = N(n(c.getArrayVal(!0)), g),
						h = E(b),
						l = k(h, "y"),
						m = k(h, "m"),
						D = !0,
						o = !0;
					a.each("y,m,d,a,h,i,s".split(","), function(b, c) {
						if (s[c] !== j) {
							var i = p[c],
								f = B[c],
								g = 31,
								n = k(h, c),
								y = a(".dw-ul", d).eq(s[c]);
							if (c == "d") {
								f = g = e.getMaxDayOfMonth(l, m);
								Q && a(".dw-li", y).each(function() {
									var d = a(this),
										b = d.data("val"),
										c = e.getDate(l, m, b).getDay(),
										b = L.replace(/[my]/gi, "").replace(/dd/, (b < 10 ? "0" + b : b) + (e.daySuffix || "")).replace(/d/, b + (e.daySuffix || ""));
									a(".dw-i", d).html(b.match(/DD/) ? b.replace(/DD/, '<span class="dw-day">' + e.dayNames[c] + "</span>") : b.replace(/D/, '<span class="dw-day">' + e.dayNamesShort[c] + "</span>"))
								})
							}
							D && ha && (i = F[c](ha));
							o && ba && (f = F[c](ba));
							if (c != "y") {
								var R = v(y, i),
									r = v(y, f);
								a(".dw-li", y).removeClass("dw-v").slice(R, r + 1).addClass("dw-v");
								c == "d" && a(".dw-li", y).removeClass("dw-h").slice(g).addClass("dw-h")
							}
							n < i && (n = i);
							n > f && (n = f);
							D && (D = n == i);
							o && (o = n == f);
							if (c == "d") {
								i = e.getDate(l, m, 1).getDay();
								f = {};
								z(Y, l, m, i, g, f, 1);
								z(X, l, m, i, g, f, 0);
								a.each(f, function(d, b) {
									b && a(".dw-li", y).eq(d).removeClass("dw-v")
								})
							}
						}
					});
					T && a.each(["a", "h", "i", "s"], function(b, e) {
						var i = k(h, e),
							p = k(h, "d"),
							D = a(".dw-ul", d).eq(s[e]);
						s[e] !== j && (G(Y, b, e, h, l, m, p, D, 0), G(X, b, e, h, l, m, p, D, 1), $[b] = +c.getValidCell(i, D, g).val)
					});
					c._tempWheelArray = h
				}
			}
		};
	a.each(["date", "time", "datetime"], function(a, h) {
		q.presets.scroller[h] = c
	})
})(jQuery);
(function(a, j, q, t) {
	var k = a.mobiscroll,
		h = a.extend,
		c = k.util,
		f = k.datetime,
		H = k.presets.scroller,
		w = {
			firstSelectDay: 0,
			labelsShort: "Yrs,Mths,Days,Hrs,Mins,Secs".split(","),
			fromText: "Start",
			toText: "End",
			eventText: "event",
			eventsText: "events"
		};
	k.presetShort("calendar");
	H.calendar = function(g) {
		function n(d) {
			if (d) {
				if (e[d]) return e[d];
				var b = a('<div style="background-color:' + d + ';"></div>').appendTo("body"),
					c = (j.getComputedStyle ? getComputedStyle(b[0]) : b[0].style).backgroundColor.replace(/rgb|rgba|\(|\)|\s/g, "").split(","),
					c = 130 < 0.299 * c[0] + 0.587 * c[1] + 0.114 * c[2] ? "#000" : "#fff";
				b.remove();
				return e[d] = c
			}
		}

		function q(a) {
			return a.sort(function(a, d) {
				var b = a.d || a.start,
					c = d.d || d.start,
					b = !b.getTime ? 0 : a.start && a.end && a.start.toDateString() !== a.end.toDateString() ? 1 : b.getTime(),
					c = !c.getTime ? 0 : d.start && d.end && d.start.toDateString() !== d.end.toDateString() ? 1 : c.getTime();
				return b - c
			})
		}

		function b(d) {
			var b;
			b = a(".dw-cal-c", o).outerHeight();
			var c = d.outerHeight(),
				e = d.outerWidth(),
				f = d.offset().top - a(".dw-cal-c", o).offset().top,
				i = 2 > d.closest(".dw-cal-row").index();
			b = l.addClass("dw-cal-events-t").css({
				top: i ? f + c : "0",
				bottom: i ? "0" : b - f
			}).addClass("dw-cal-events-v").height();
			l.css(i ? "bottom" : "top", "auto").removeClass("dw-cal-events-t");
			U.css("max-height", b);
			J.refresh();
			J.scroll(0);
			i ? l.addClass("dw-cal-events-b") : l.removeClass("dw-cal-events-b");
			a(".dw-cal-events-arr", l).css("left", d.offset().left - l.offset().left + e / 2)
		}

		function O(d, c) {
			var e = I[d];
			if (e) {
				var f, i, h, j, o, p = '<ul class="dw-cal-event-list">';
				C = c;
				c.addClass($).find(".dw-i").addClass(s);
				c.hasClass(P) && c.attr("data-hl", "true").removeClass(P);
				q(e);
				a.each(e, function(a, b) {
					j = b.d || b.start;
					o = b.start && b.end && b.start.toDateString() !== b.end.toDateString();
					h = b.color;
					n(h);
					i = f = "";
					j.getTime && (f = k.datetime.formatDate((o ? "MM d yy " : "") + y.timeFormat, j));
					b.end && (i = k.datetime.formatDate((o ? "MM d yy " : "") + y.timeFormat, b.end));
					var d = p,
						c = '<li role="button" aria-label="' + b.text + (f ? ", " + y.fromText + " " + f : "") + (i ? ", " + y.toText + " " + i : "") + '" class="dw-cal-event"><div class="dw-cal-event-color" style="' + (h ? "background:" + h + ";" : "") + '"></div>' + (j.getTime && !o ? '<div class="dw-cal-event-time">' + k.datetime.formatDate(y.timeFormat, j) + "</div>" : "") + b.text,
						e;
					if (b.start && b.end) {
						e = y.labelsShort;
						var g = Math.abs(b.end - b.start) / 1E3,
							l = g / 60,
							s = l / 60,
							F = s / 24,
							r = F / 365;
						e = '<div class="dw-cal-event-dur">' + (45 > g && Math.round(g) + " " + e[5].toLowerCase() || 45 > l && Math.round(l) + " " + e[4].toLowerCase() || 24 > s && Math.round(s) + " " + e[3].toLowerCase() || 30 > F && Math.round(F) + " " + e[2].toLowerCase() || 365 > F && Math.round(F / 30) + " " + e[1].toLowerCase() || Math.round(r) + " " + e[0].toLowerCase()) + "</div>"
					} else e = "";
					p = d + (c + e + "</li>")
				});
				p += "</ul>";
				r.html(p);
				g.trigger("onEventBubbleShow", [C, l]);
				b(C);
				g.tap(a(".dw-cal-event", r), function(b) {
					J.scrolled || g.trigger("onEventSelect", [b, e[a(this).index()], d])
				});
				K = !0
			}
		}

		function z() {
			l && l.removeClass("dw-cal-events-v");
			C && (C.removeClass($).find(".dw-i").removeClass(s), C.attr("data-hl") && C.removeAttr("data-hl").addClass(P));
			K = !1
		}

		function G(a) {
			return new Date(a.getFullYear(), a.getMonth(), a.getDate())
		}

		function v(a) {
			i = {};
			if (a && a.length) for (M = 0; M < a.length; M++) i[G(a[M])] = a[M]
		}

		function E() {
			Q && z();
			g.refresh()
		}
		var x, o, l, C, I, J, U, r, K, M, A, T, p, B, e = {};
		p = h({}, g.settings);
		var y = h(g.settings, w, p),
			$ = "dw-sel dw-cal-day-ev",
			P = "dw-cal-day-hl",
			s = y.activeClass || "",
			F = y.multiSelect || "week" == y.selectType,
			Y = y.markedDisplay,
			X = !0 === y.events || !0 === y.markedText,
			L = 0,
			i = {},
			Q = a.isArray(y.events),
			S = Q ? h(!0, [], y.events) : [];
		p = H.calbase.call(this, g);
		x = h({}, p);
		if (y.selectedValues) for (M = 0; M < y.selectedValues.length; M++) i[G(y.selectedValues[M])] = y.selectedValues[M];
		Q && a.each(S, function(a, b) {
			b._id === t && (b._id = L++)
		});
		g.onGenMonth = function(a, b) {
			I = g.prepareObj(S, a, b);
			A = g.prepareObj(y.marked, a, b)
		};
		g.getDayProps = function(b) {
			for (var c = F ? i[b] !== t : Q ? b.getTime() === (new Date).setHours(0, 0, 0, 0) : t, e = A[b] ? A[b][0] : !1, f = I[b] ? I[b][0] : !1, g = e || f, e = e.text || (f ? I[b].length + " " + (1 < I[b].length ? y.eventsText : y.eventText) : 0), f = A[b] || I[b] || [], h = g.color, j = X && e ? n(h) : "", k = "", l = '<div class="dw-cal-day-m"' + (h ? ' style="background-color:' + h + ";border-color:" + h + " " + h + ' transparent transparent"' : "") + "></div>", b = 0; b < f.length; b++) f[b].icon && (k += '<span class="mbsc-ic mbsc-ic-' + f[b].icon + '"' + (f[b].text ? "" : f[b].color ? ' style="color:' + f[b].color + ';"' : "") + "></span>\n");
			if ("bottom" == Y) {
				l = '<div class="dw-cal-day-m"><div class="dw-cal-day-m-t">';
				for (b = 0; b < f.length; b++) l += '<div class="dw-cal-day-m-c"' + (f[b].color ? ' style="background:' + f[b].color + ';"' : "") + "></div>";
				l += "</div></div>"
			}
			return {
				marked: g,
				selected: Q ? !1 : c,
				cssClass: Q && c ? "dw-cal-day-hl" : "",
				ariaLabel: X || Q ? e : "",
				markup: X && e ? '<div class="dw-cal-day-txt-c"><div class="dw-cal-day-txt ' + (y.eventTextClass || "") + '" title="' + a("<div>" + e + "</div>").text() + '"' + (h ? ' style="background:' + h + ";color:" + j + ';text-shadow:none;"' : "") + ">" + k + e + "</div></div>" : X && k ? '<div class="dw-cal-day-ic-c">' + k + "</div>" : g ? l : ""
			}
		};
		g.addValue = function(a) {
			i[G(a)] = a;
			E()
		};
		g.removeValue = function(a) {
			delete i[G(a)];
			E()
		};
		g.setVal = function(a, b, c, e, f) {
			F && (v(a), a = a ? a[0] : null);
			g.setDate(a, b, f, e, c);
			E()
		};
		g.getVal = function(a) {
			return F ? c.objectToArray(i) : g.getDate(a)
		};
		g.setValues = function(a, b) {
			g.setDate(a ? a[0] : null, b);
			v(a);
			E()
		};
		g.getValues = function() {
			return F ? g.getVal() : [g.getDate()]
		};
		Q && (g.addEvent = function(b) {
			var c = [],
				b = h(!0, [], a.isArray(b) ? b : [b]);
			a.each(b, function(a, b) {
				b._id === t && (b._id = L++);
				S.push(b);
				c.push(b._id)
			});
			E();
			return c
		}, g.removeEvent = function(b) {
			b = a.isArray(b) ? b : [b];
			a.each(b, function(b, d) {
				a.each(S, function(a, b) {
					if (b._id === d) return S.splice(a, 1), !1
				})
			});
			E()
		}, g.getEvents = function(a) {
			var b;
			return a ? (a.setHours(0, 0, 0, 0), b = g.prepareObj(S, a.getFullYear(), a.getMonth()), b[a] ? q(b[a]) : []) : S
		}, g.setEvents = function(b) {
			var c = [];
			S = h(!0, [], b);
			a.each(S, function(a, b) {
				b._id === t && (b._id = L++);
				c.push(b._id)
			});
			E();
			return c
		});
		h(p, {
			highlight: !F && !Q,
			divergentDayChange: !F && !Q,
			buttons: Q && "inline" !== y.display ? ["cancel"] : y.buttons,
			parseValue: function(a) {
				var b, c;
				if (F && a) {
					i = {};
					a = a.split(",");
					for (b = 0; b < a.length; b++) {
						c = f.parseDate(g.format, a[b].replace(/^\s+|\s+$/g, ""), y);
						i[G(c)] = c
					}
					a = a[0]
				}
				return x.parseValue.call(this, a)
			},
			formatResult: function(a) {
				var b, c = [];
				if (F) {
					for (b in i) c.push(f.formatDate(g.format, i[b], y));
					return c.join(", ")
				}
				return x.formatResult.call(this, a)
			},
			onClear: function() {
				if (F) {
					i = {};
					g.refresh()
				}
			},
			onBeforeShow: function() {
				if (Q) y.headerText = false;
				if (y.closeOnSelect) y.divergentDayChange = false;
				if (y.counter && F) y.headerText = function() {
					var b = 0,
						c = y.selectType == "week" ? 7 : 1;
					a.each(i, function() {
						b++
					});
					return b / c + " " + y.selectedText
				}
			},
			onMarkupReady: function(b) {
				x.onMarkupReady.call(this, b);
				o = b;
				if (F) {
					a(".dwv", b).attr("aria-live", "off");
					T = h({}, i)
				}
				X && a(".dw-cal", b).addClass("dw-cal-ev");
				Y && a(".dw-cal", b).addClass("dw-cal-m-" + Y);
				if (Q) {
					b.addClass("dw-cal-em");
					l = a('<div class="dw-cal-events ' + (y.eventBubbleClass || "") + '"><div class="dw-cal-events-arr"></div><div class="dw-cal-events-i"><div class="dw-cal-events-sc"></div></div></div>').appendTo(a(".dw-cal-c", b));
					U = a(".dw-cal-events-i", l);
					r = a(".dw-cal-events-sc", l);
					J = new k.classes.ScrollView(U[0]);
					g.tap(U, function() {
						J.scrolled || z()
					})
				}
			},
			onMonthChange: function() {
				Q && z()
			},
			onSelectShow: function() {
				Q && z()
			},
			onMonthLoaded: function() {
				if (B) {
					O(B.d, a('.dw-cal-day-v[data-full="' + B.full + '"]:not(.dw-cal-day-diff)', o));
					B = false
				}
			},
			onDayChange: function(b) {
				var c = b.date,
					e = G(c),
					f = a(b.cell),
					b = b.selected;
				if (Q) {
					z();
					f.hasClass("dw-cal-day-ev") || setTimeout(function() {
						g.changing ? B = {
							d: e,
							full: f.attr("data-full")
						} : O(e, f)
					}, 10)
				} else if (F) if (y.selectType == "week") {
					var h, j, k = e.getDay() - y.firstSelectDay,
						k = k < 0 ? 7 + k : k;
					y.multiSelect || (i = {});
					for (h = 0; h < 7; h++) {
						j = new Date(e.getFullYear(), e.getMonth(), e.getDate() - k + h);
						b ? delete i[j] : i[j] = j
					}
					E()
				} else {
					h = a('.dw-cal .dw-cal-day[data-full="' + f.attr("data-full") + '"]', o);
					if (b) {
						h.removeClass("dw-sel").removeAttr("aria-selected").find(".dw-i").removeClass(s);
						delete i[e]
					} else {
						h.addClass("dw-sel").attr("aria-selected", "true").find(".dw-i").addClass(s);
						i[e] = e
					}
				}
				if (!Q && !y.multiSelect && y.closeOnSelect && y.display !== "inline") {
					g.needsSlide = false;
					g.setDate(c);
					g.select();
					return false
				}
			},
			onCalResize: function() {
				K && b(C)
			},
			onCancel: function() {
				!g.live && F && (i = h({}, T))
			}
		});
		return p
	}
})(jQuery, window, document);
(function($, j) {
	var k = $.mobiscroll;
	k.datetime = {
		defaults: {
			shortYearCutoff: '+10',
			monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
			dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
			dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
			dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			monthText: 'Month',
			amText: 'am',
			pmText: 'pm',
			getYear: function(d) {
				return d.getFullYear()
			},
			getMonth: function(d) {
				return d.getMonth()
			},
			getDay: function(d) {
				return d.getDate()
			},
			getDate: function(y, m, d, h, i, s) {
				return new Date(y, m, d, h || 0, i || 0, s || 0)
			},
			getMaxDayOfMonth: function(y, m) {
				return 32 - new Date(y, m, 32).getDate()
			},
			getWeekNumber: function(d) {
				d = new Date(d);
				d.setHours(0, 0, 0);
				d.setDate(d.getDate() + 4 - (d.getDay() || 7));
				var a = new Date(d.getFullYear(), 0, 1);
				return Math.ceil((((d - a) / 86400000) + 1) / 7)
			}
		},
		formatDate: function(c, d, e) {
			if (!d) {
				return null
			}
			var s = $.extend({}, k.datetime.defaults, e),
				look = function(m) {
					var n = 0;
					while (i + 1 < c.length && c.charAt(i + 1) == m) {
						n++;
						i++
					}
					return n
				},
				f1 = function(m, a, b) {
					var n = '' + a;
					if (look(m)) {
						while (n.length < b) {
							n = '0' + n
						}
					}
					return n
				},
				f2 = function(m, a, s, l) {
					return (look(m) ? l[a] : s[a])
				},
				i, year, output = '',
				literal = false;
			for (i = 0; i < c.length; i++) {
				if (literal) {
					if (c.charAt(i) == "'" && !look("'")) {
						literal = false
					} else {
						output += c.charAt(i)
					}
				} else {
					switch (c.charAt(i)) {
					case 'd':
						output += f1('d', s.getDay(d), 2);
						break;
					case 'D':
						output += f2('D', d.getDay(), s.dayNamesShort, s.dayNames);
						break;
					case 'o':
						output += f1('o', (d.getTime() - new Date(d.getFullYear(), 0, 0).getTime()) / 86400000, 3);
						break;
					case 'm':
						output += f1('m', s.getMonth(d) + 1, 2);
						break;
					case 'M':
						output += f2('M', s.getMonth(d), s.monthNamesShort, s.monthNames);
						break;
					case 'y':
						year = s.getYear(d);
						output += (look('y') ? year : (year % 100 < 10 ? '0' : '') + year % 100);
						break;
					case 'h':
						var h = d.getHours();
						output += f1('h', (h > 12 ? (h - 12) : (h === 0 ? 12 : h)), 2);
						break;
					case 'H':
						output += f1('H', d.getHours(), 2);
						break;
					case 'i':
						output += f1('i', d.getMinutes(), 2);
						break;
					case 's':
						output += f1('s', d.getSeconds(), 2);
						break;
					case 'a':
						output += d.getHours() > 11 ? s.pmText : s.amText;
						break;
					case 'A':
						output += d.getHours() > 11 ? s.pmText.toUpperCase() : s.amText.toUpperCase();
						break;
					case "'":
						if (look("'")) {
							output += "'"
						} else {
							literal = true
						}
						break;
					default:
						output += c.charAt(i)
					}
				}
			}
			return output
		},
		parseDate: function(c, d, e) {
			var s = $.extend({}, k.datetime.defaults, e),
				def = s.defaultValue || new Date();
			if (!c || !d) {
				return def
			}
			if (d.getTime) {
				return d
			}
			d = (typeof d == 'object' ? d.toString() : d + '');
			var f = s.shortYearCutoff,
				year = s.getYear(def),
				month = s.getMonth(def) + 1,
				day = s.getDay(def),
				doy = -1,
				hours = def.getHours(),
				minutes = def.getMinutes(),
				seconds = 0,
				ampm = -1,
				literal = false,
				lookAhead = function(a) {
					var b = (iFormat + 1 < c.length && c.charAt(iFormat + 1) == a);
					if (b) {
						iFormat++
					}
					return b
				},
				getNumber = function(a) {
					lookAhead(a);
					var b = (a == '@' ? 14 : (a == '!' ? 20 : (a == 'y' ? 4 : (a == 'o' ? 3 : 2)))),
						digits = new RegExp('^\\d{1,' + b + '}'),
						num = d.substr(iValue).match(digits);
					if (!num) {
						return 0
					}
					iValue += num[0].length;
					return parseInt(num[0], 10)
				},
				getName = function(a, s, l) {
					var b = (lookAhead(a) ? l : s),
						i;
					for (i = 0; i < b.length; i++) {
						if (d.substr(iValue, b[i].length).toLowerCase() == b[i].toLowerCase()) {
							iValue += b[i].length;
							return i + 1
						}
					}
					return 0
				},
				checkLiteral = function() {
					iValue++
				},
				iValue = 0,
				iFormat;
			for (iFormat = 0; iFormat < c.length; iFormat++) {
				if (literal) {
					if (c.charAt(iFormat) == "'" && !lookAhead("'")) {
						literal = false
					} else {
						checkLiteral()
					}
				} else {
					switch (c.charAt(iFormat)) {
					case 'd':
						day = getNumber('d');
						break;
					case 'D':
						getName('D', s.dayNamesShort, s.dayNames);
						break;
					case 'o':
						doy = getNumber('o');
						break;
					case 'm':
						month = getNumber('m');
						break;
					case 'M':
						month = getName('M', s.monthNamesShort, s.monthNames);
						break;
					case 'y':
						year = getNumber('y');
						break;
					case 'H':
						hours = getNumber('H');
						break;
					case 'h':
						hours = getNumber('h');
						break;
					case 'i':
						minutes = getNumber('i');
						break;
					case 's':
						seconds = getNumber('s');
						break;
					case 'a':
						ampm = getName('a', [s.amText, s.pmText], [s.amText, s.pmText]) - 1;
						break;
					case 'A':
						ampm = getName('A', [s.amText, s.pmText], [s.amText, s.pmText]) - 1;
						break;
					case "'":
						if (lookAhead("'")) {
							checkLiteral()
						} else {
							literal = true
						}
						break;
					default:
						checkLiteral()
					}
				}
			}
			if (year < 100) {
				year += new Date().getFullYear() - new Date().getFullYear() % 100 + (year <= (typeof f != 'string' ? f : new Date().getFullYear() % 100 + parseInt(f, 10)) ? 0 : -100)
			}
			if (doy > -1) {
				month = 1;
				day = doy;
				do {
					var g = 32 - new Date(year, month - 1, 32).getDate();
					if (day <= g) {
						break
					}
					month++;
					day -= g
				} while (true)
			}
			hours = (ampm == -1) ? hours : ((ampm && hours < 12) ? (hours + 12) : (!ampm && hours == 12 ? 0 : hours));
			var h = s.getDate(year, month - 1, day, hours, minutes, seconds);
			if (s.getYear(h) != year || s.getMonth(h) + 1 != month || s.getDay(h) != day) {
				return def
			}
			return h
		}
	};
	k.formatDate = k.datetime.formatDate;
	k.parseDate = k.datetime.parseDate
})(jQuery);
(function($, z) {
	var A = $.mobiscroll,
		datetime = A.datetime,
		date = new Date(),
		defaults = {
			startYear: date.getFullYear() - 100,
			endYear: date.getFullYear() + 1,
			separator: ' ',
			dateFormat: 'mm/dd/yy',
			dateOrder: 'mmddyy',
			timeWheels: 'hhiiA',
			timeFormat: 'hh:ii A',
			dayText: 'Day',
			yearText: 'Year',
			hourText: 'Hours',
			minuteText: 'Minutes',
			ampmText: '&nbsp;',
			secText: 'Seconds',
			nowText: 'Now'
		},
		preset = function(l) {
			var n = $(this),
				html5def = {},
				format;
			if (n.is('input')) {
				switch (n.attr('type')) {
				case 'date':
					format = 'yy-mm-dd';
					break;
				case 'datetime':
					format = 'yy-mm-ddTHH:ii:ssZ';
					break;
				case 'datetime-local':
					format = 'yy-mm-ddTHH:ii:ss';
					break;
				case 'month':
					format = 'yy-mm';
					html5def.dateOrder = 'mmyy';
					break;
				case 'time':
					format = 'HH:ii:ss';
					break
				}
				var q = n.attr('min'),
					max = n.attr('max');
				if (q) {
					html5def.minDate = datetime.parseDate(format, q)
				}
				if (max) {
					html5def.maxDate = datetime.parseDate(format, max)
				}
			}
			var i, k, keys, values, wg, start, end, hasTime, mins, maxs, orig = $.extend({}, l.settings),
				s = $.extend(l.settings, A.datetime.defaults, defaults, html5def, orig),
				offset = 0,
				validValues = [],
				wheels = [],
				ord = [],
				o = {},
				f = {
					y: getYear,
					m: getMonth,
					d: getDay,
					h: getHour,
					i: getMinute,
					s: getSecond,
					a: getAmPm
				},
				invalid = s.invalid,
				valid = s.valid,
				p = s.preset,
				dord = s.dateOrder,
				tord = s.timeWheels,
				regen = dord.match(/D/),
				ampm = tord.match(/a/i),
				hampm = tord.match(/h/),
				hformat = p == 'datetime' ? s.dateFormat + s.separator + s.timeFormat : p == 'time' ? s.timeFormat : s.dateFormat,
				defd = new Date(),
				steps = s.steps || {},
				stepH = steps.hour || s.stepHour || 1,
				stepM = steps.minute || s.stepMinute || 1,
				stepS = steps.second || s.stepSecond || 1,
				zeroBased = steps.zeroBased,
				mind = s.minDate || new Date(s.startYear, 0, 1),
				maxd = s.maxDate || new Date(s.endYear, 11, 31, 23, 59, 59),
				minH = zeroBased ? 0 : mind.getHours() % stepH,
				minM = zeroBased ? 0 : mind.getMinutes() % stepM,
				minS = zeroBased ? 0 : mind.getSeconds() % stepS,
				maxH = getMax(stepH, minH, (hampm ? 11 : 23)),
				maxM = getMax(stepM, minM, 59),
				maxS = getMax(stepM, minM, 59);
			format = format || hformat;
			if (p.match(/date/i)) {
				$.each(['y', 'm', 'd'], function(j, v) {
					i = dord.search(new RegExp(v, 'i'));
					if (i > -1) {
						ord.push({
							o: i,
							v: v
						})
					}
				});
				ord.sort(function(a, b) {
					return a.o > b.o ? 1 : -1
				});
				$.each(ord, function(i, v) {
					o[v.v] = i
				});
				wg = [];
				for (k = 0; k < 3; k++) {
					if (k == o.y) {
						offset++;
						values = [];
						keys = [];
						start = s.getYear(mind);
						end = s.getYear(maxd);
						for (i = start; i <= end; i++) {
							keys.push(i);
							values.push((dord.match(/yy/i) ? i : (i + '').substr(2, 2)) + (s.yearSuffix || ''))
						}
						addWheel(wg, keys, values, s.yearText)
					} else if (k == o.m) {
						offset++;
						values = [];
						keys = [];
						for (i = 0; i < 12; i++) {
							var r = dord.replace(/[dy]/gi, '').replace(/mm/, (i < 9 ? '0' + (i + 1) : i + 1) + (s.monthSuffix || '')).replace(/m/, i + 1 + (s.monthSuffix || ''));
							keys.push(i);
							values.push(r.match(/MM/) ? r.replace(/MM/, '<span class="dw-mon">' + s.monthNames[i] + '</span>') : r.replace(/M/, '<span class="dw-mon">' + s.monthNamesShort[i] + '</span>'))
						}
						addWheel(wg, keys, values, s.monthText)
					} else if (k == o.d) {
						offset++;
						values = [];
						keys = [];
						for (i = 1; i < 32; i++) {
							keys.push(i);
							values.push((dord.match(/dd/i) && i < 10 ? '0' + i : i) + (s.daySuffix || ''))
						}
						addWheel(wg, keys, values, s.dayText)
					}
				}
				wheels.push(wg)
			}
			if (p.match(/time/i)) {
				hasTime = true;
				ord = [];
				$.each(['h', 'i', 's', 'a'], function(i, v) {
					i = tord.search(new RegExp(v, 'i'));
					if (i > -1) {
						ord.push({
							o: i,
							v: v
						})
					}
				});
				ord.sort(function(a, b) {
					return a.o > b.o ? 1 : -1
				});
				$.each(ord, function(i, v) {
					o[v.v] = offset + i
				});
				wg = [];
				for (k = offset; k < offset + 4; k++) {
					if (k == o.h) {
						offset++;
						values = [];
						keys = [];
						for (i = minH; i < (hampm ? 12 : 24); i += stepH) {
							keys.push(i);
							values.push(hampm && i === 0 ? 12 : tord.match(/hh/i) && i < 10 ? '0' + i : i)
						}
						addWheel(wg, keys, values, s.hourText)
					} else if (k == o.i) {
						offset++;
						values = [];
						keys = [];
						for (i = minM; i < 60; i += stepM) {
							keys.push(i);
							values.push(tord.match(/ii/) && i < 10 ? '0' + i : i)
						}
						addWheel(wg, keys, values, s.minuteText)
					} else if (k == o.s) {
						offset++;
						values = [];
						keys = [];
						for (i = minS; i < 60; i += stepS) {
							keys.push(i);
							values.push(tord.match(/ss/) && i < 10 ? '0' + i : i)
						}
						addWheel(wg, keys, values, s.secText)
					} else if (k == o.a) {
						offset++;
						var u = tord.match(/A/);
						addWheel(wg, [0, 1], u ? [s.amText.toUpperCase(), s.pmText.toUpperCase()] : [s.amText, s.pmText], s.ampmText)
					}
				}
				wheels.push(wg)
			}

			function get(d, i, a) {
				if (o[i] !== z) {
					return +d[o[i]]
				}
				if (a !== z) {
					return a
				}
				return f[i](defd)
			}

			function addWheel(a, k, v, b) {
				a.push({
					values: v,
					keys: k,
					label: b
				})
			}

			function step(v, a, b, c) {
				return Math.min(c, Math.floor(v / a) * a + b)
			}

			function getYear(d) {
				return s.getYear(d)
			}

			function getMonth(d) {
				return s.getMonth(d)
			}

			function getDay(d) {
				return s.getDay(d)
			}

			function getHour(d) {
				var a = d.getHours();
				a = hampm && a >= 12 ? a - 12 : a;
				return step(a, stepH, minH, maxH)
			}

			function getMinute(d) {
				return step(d.getMinutes(), stepM, minM, maxM)
			}

			function getSecond(d) {
				return step(d.getSeconds(), stepS, minS, maxS)
			}

			function getAmPm(d) {
				return ampm && d.getHours() > 11 ? 1 : 0
			}

			function getDate(d) {
				if (d === null) {
					return d
				}
				var a = get(d, 'h', 0);
				return s.getDate(get(d, 'y'), get(d, 'm'), get(d, 'd', 1), get(d, 'a', 0) ? a + 12 : a, get(d, 'i', 0), get(d, 's', 0))
			}

			function getMax(a, b, c) {
				return Math.floor((c - b) / a) * a + b
			}

			function getClosestValidDate(d, a) {
				var b, prev, nextValid = false,
					prevValid = false,
					up = 0,
					down = 0;
				if (isValid(d)) {
					return d
				}
				if (d < mind) {
					d = mind
				}
				if (d > maxd) {
					d = maxd
				}
				b = d;
				prev = d;
				if (a !== 2) {
					nextValid = isValid(b);
					while (!nextValid && b < maxd) {
						b = new Date(b.getTime() + 1000 * 60 * 60 * 24);
						nextValid = isValid(b);
						up++
					}
				}
				if (a !== 1) {
					prevValid = isValid(prev);
					while (!prevValid && prev > mind) {
						prev = new Date(prev.getTime() - 1000 * 60 * 60 * 24);
						prevValid = isValid(prev);
						down++
					}
				}
				if (a === 1 && nextValid) {
					return b
				}
				if (a === 2 && prevValid) {
					return prev
				}
				return down < up && prevValid ? prev : b
			}

			function isValid(d) {
				if (d < mind) {
					return false
				}
				if (d > maxd) {
					return false
				}
				if (isInObj(d, valid)) {
					return true
				}
				if (isInObj(d, invalid)) {
					return false
				}
				return true
			}

			function isInObj(d, a) {
				var b, j, v;
				if (a) {
					for (j = 0; j < a.length; j++) {
						b = a[j];
						v = b + '';
						if (!b.start) {
							if (b.getTime) {
								if (d.getFullYear() == b.getFullYear() && d.getMonth() == b.getMonth() && d.getDate() == b.getDate()) {
									return true
								}
							} else if (!v.match(/w/i)) {
								v = v.split('/');
								if (v[1]) {
									if ((v[0] - 1) == d.getMonth() && v[1] == d.getDate()) {
										return true
									}
								} else if (v[0] == d.getDate()) {
									return true
								}
							} else {
								v = +v.replace('w', '');
								if (v == d.getDay()) {
									return true
								}
							}
						}
					}
				}
				return false
			}

			function validateDates(a, y, m, b, c, e, f) {
				var j, d, v;
				if (a) {
					for (j = 0; j < a.length; j++) {
						d = a[j];
						v = d + '';
						if (!d.start) {
							if (d.getTime) {
								if (s.getYear(d) == y && s.getMonth(d) == m) {
									e[s.getDay(d) - 1] = f
								}
							} else if (!v.match(/w/i)) {
								v = v.split('/');
								if (v[1]) {
									if (v[0] - 1 == m) {
										e[v[1] - 1] = f
									}
								} else {
									e[v[0] - 1] = f
								}
							} else {
								v = +v.replace('w', '');
								for (k = v - b; k < c; k += 7) {
									if (k >= 0) {
										e[k] = f
									}
								}
							}
						}
					}
				}
			}

			function validateTimes(b, i, v, c, y, m, d, e, f) {
				var g, ss, r, parts1, parts2, prop1, prop2, v1, v2, j, i1, i2, add, remove, all, hours1, hours2, hours3, spec = {},
					steps = {
						h: stepH,
						i: stepM,
						s: stepS,
						a: 1
					},
					day = s.getDate(y, m, d),
					w = ['a', 'h', 'i', 's'];
				if (b) {
					$.each(b, function(i, a) {
						if (a.start) {
							a.apply = false;
							g = a.d;
							ss = g + '';
							r = ss.split('/');
							if (g && ((g.getTime && y == s.getYear(g) && m == s.getMonth(g) && d == s.getDay(g)) || (!ss.match(/w/i) && ((r[1] && d == r[1] && m == r[0] - 1) || (!r[1] && d == r[0]))) || (ss.match(/w/i) && day.getDay() == +ss.replace('w', '')))) {
								a.apply = true;
								spec[day] = true
							}
						}
					});
					$.each(b, function(x, a) {
						add = 0;
						remove = 0;
						i1 = 0;
						i2 = z;
						prop1 = true;
						prop2 = true;
						all = false;
						if (a.start && (a.apply || (!a.d && !spec[day]))) {
							parts1 = a.start.split(':');
							parts2 = a.end.split(':');
							for (j = 0; j < 3; j++) {
								if (parts1[j] === z) {
									parts1[j] = 0
								}
								if (parts2[j] === z) {
									parts2[j] = 59
								}
								parts1[j] = +parts1[j];
								parts2[j] = +parts2[j]
							}
							parts1.unshift(parts1[0] > 11 ? 1 : 0);
							parts2.unshift(parts2[0] > 11 ? 1 : 0);
							if (hampm) {
								if (parts1[1] >= 12) {
									parts1[1] = parts1[1] - 12
								}
								if (parts2[1] >= 12) {
									parts2[1] = parts2[1] - 12
								}
							}
							for (j = 0; j < i; j++) {
								if (validValues[j] !== z) {
									v1 = step(parts1[j], steps[w[j]], mins[w[j]], maxs[w[j]]);
									v2 = step(parts2[j], steps[w[j]], mins[w[j]], maxs[w[j]]);
									hours1 = 0;
									hours2 = 0;
									hours3 = 0;
									if (hampm && j == 1) {
										hours1 = parts1[0] ? 12 : 0;
										hours2 = parts2[0] ? 12 : 0;
										hours3 = validValues[0] ? 12 : 0
									}
									if (!prop1) {
										v1 = 0
									}
									if (!prop2) {
										v2 = maxs[w[j]]
									}
									if ((prop1 || prop2) && (v1 + hours1 < validValues[j] + hours3 && validValues[j] + hours3 < v2 + hours2)) {
										all = true
									}
									if (validValues[j] != v1) {
										prop1 = false
									}
									if (validValues[j] != v2) {
										prop2 = false
									}
								}
							}
							if (!f) {
								for (j = i + 1; j < 4; j++) {
									if (parts1[j] > 0) {
										add = steps[v]
									}
									if (parts2[j] < maxs[w[j]]) {
										remove = steps[v]
									}
								}
							}
							if (!all) {
								v1 = step(parts1[i], steps[v], mins[v], maxs[v]) + add;
								v2 = step(parts2[i], steps[v], mins[v], maxs[v]) - remove;
								if (prop1) {
									i1 = getValidIndex(e, v1, maxs[v], 0)
								}
								if (prop2) {
									i2 = getValidIndex(e, v2, maxs[v], 1)
								}
							}
							if (prop1 || prop2 || all) {
								if (f) {
									$('.dw-li', e).slice(i1, i2).addClass('dw-v')
								} else {
									$('.dw-li', e).slice(i1, i2).removeClass('dw-v')
								}
							}
						}
					})
				}
			}

			function getIndex(t, v) {
				return $('.dw-li', t).index($('.dw-li[data-val="' + v + '"]', t))
			}

			function getValidIndex(t, v, a, b) {
				if (v < 0) {
					return 0
				}
				if (v > a) {
					return $('.dw-li', t).length
				}
				return getIndex(t, v) + b
			}

			function getArray(d) {
				var i, ret = [];
				if (d === null || d === z) {
					return d
				}
				for (i in o) {
					ret[o[i]] = f[i](d)
				}
				return ret
			}

			function convertRanges(a) {
				var i, v, start, ret = [];
				if (a) {
					for (i = 0; i < a.length; i++) {
						v = a[i];
						if (v.start && v.start.getTime) {
							start = new Date(v.start);
							while (start <= v.end) {
								ret.push(new Date(start.getFullYear(), start.getMonth(), start.getDate()));
								start.setDate(start.getDate() + 1)
							}
						} else {
							ret.push(v)
						}
					}
					return ret
				}
				return a
			}
			l.getVal = function(a) {
				return l._hasValue || a ? getDate(l.getArrayVal(a)) : null
			};
			l.setDate = function(d, a, b, c, e) {
				l.setArrayVal(getArray(d), a, e, c, b)
			};
			l.getDate = l.getVal;
			l.format = hformat;
			l.order = o;
			l.handlers.now = function() {
				l.setDate(new Date(), false, 0.3, true, true)
			};
			l.buttons.now = {
				text: s.nowText,
				handler: 'now'
			};
			invalid = convertRanges(invalid);
			valid = convertRanges(valid);
			mind = getDate(getArray(mind));
			maxd = getDate(getArray(maxd));
			mins = {
				y: mind.getFullYear(),
				m: 0,
				d: 1,
				h: minH,
				i: minM,
				s: minS,
				a: 0
			};
			maxs = {
				y: maxd.getFullYear(),
				m: 11,
				d: 31,
				h: maxH,
				i: maxM,
				s: maxS,
				a: 1
			};
			return {
				wheels: wheels,
				headerText: s.headerText ?
				function() {
					return datetime.formatDate(hformat, getDate(l.getArrayVal(true)), s)
				} : false,
				formatResult: function(d) {
					return datetime.formatDate(format, getDate(d), s)
				},
				parseValue: function(a) {
					return getArray(a ? datetime.parseDate(format, a, s) : (s.defaultValue || new Date()))
				},
				validate: function(g, i, h, j) {
					var k = getClosestValidDate(getDate(l.getArrayVal(true)), j),
						temp = getArray(k),
						y = get(temp, 'y'),
						m = get(temp, 'm'),
						minprop = true,
						maxprop = true;
					$.each(['y', 'm', 'd', 'a', 'h', 'i', 's'], function(x, i) {
						if (o[i] !== z) {
							var b = mins[i],
								max = maxs[i],
								maxdays = 31,
								val = get(temp, i),
								t = $('.dw-ul', g).eq(o[i]);
							if (i == 'd') {
								maxdays = s.getMaxDayOfMonth(y, m);
								max = maxdays;
								if (regen) {
									$('.dw-li', t).each(function() {
										var a = $(this),
											d = a.data('val'),
											w = s.getDate(y, m, d).getDay(),
											r = dord.replace(/[my]/gi, '').replace(/dd/, (d < 10 ? '0' + d : d) + (s.daySuffix || '')).replace(/d/, d + (s.daySuffix || ''));
										$('.dw-i', a).html(r.match(/DD/) ? r.replace(/DD/, '<span class="dw-day">' + s.dayNames[w] + '</span>') : r.replace(/D/, '<span class="dw-day">' + s.dayNamesShort[w] + '</span>'))
									})
								}
							}
							if (minprop && mind) {
								b = f[i](mind)
							}
							if (maxprop && maxd) {
								max = f[i](maxd)
							}
							if (i != 'y') {
								var c = getIndex(t, b),
									i2 = getIndex(t, max);
								$('.dw-li', t).removeClass('dw-v').slice(c, i2 + 1).addClass('dw-v');
								if (i == 'd') {
									$('.dw-li', t).removeClass('dw-h').slice(maxdays).addClass('dw-h')
								}
							}
							if (val < b) {
								val = b
							}
							if (val > max) {
								val = max
							}
							if (minprop) {
								minprop = val == b
							}
							if (maxprop) {
								maxprop = val == max
							}
							if (i == 'd') {
								var e = s.getDate(y, m, 1).getDay(),
									idx = {};
								validateDates(invalid, y, m, e, maxdays, idx, 1);
								validateDates(valid, y, m, e, maxdays, idx, 0);
								$.each(idx, function(i, v) {
									if (v) {
										$('.dw-li', t).eq(i).removeClass('dw-v')
									}
								})
							}
						}
					});
					if (hasTime) {
						$.each(['a', 'h', 'i', 's'], function(i, v) {
							var a = get(temp, v),
								d = get(temp, 'd'),
								t = $('.dw-ul', g).eq(o[v]);
							if (o[v] !== z) {
								validateTimes(invalid, i, v, temp, y, m, d, t, 0);
								validateTimes(valid, i, v, temp, y, m, d, t, 1);
								validValues[i] = +l.getValidCell(a, t, j).val
							}
						})
					}
					l._tempWheelArray = temp
				}
			}
		};
	$.each(['date', 'time', 'datetime'], function(i, v) {
		A.presets.scroller[v] = preset
	})
})(jQuery);
(function($) {
	$.each(['date', 'time', 'datetime'], function(i, v) {
		$.mobiscroll.presetShort(v)
	})
})(jQuery);
(function($) {
	$.mobiscroll.i18n.zh = $.extend($.mobiscroll.i18n.zh, {
		setText: '确定',
		cancelText: '取消',
		clearText: '明确',
		selectedText: '选',
		dateFormat: 'yy-mm-dd',
		dateOrder: 'yymmdd',
		dayNames: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
		dayNamesShort: ['日', '一', '二', '三', '四', '五', '六'],
		dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
		dayText: '日',
		hourText: '时',
		minuteText: '分',
		monthNamesShort: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
		monthNames: ['一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '十一', '十二'],
		monthText: '月',
		secText: '秒',
		timeFormat: 'HH:ii',
		timeWheels: 'HHii',
		yearText: '年',
		nowText: '现在',
		pmText: '下午',
		amText: '上午',
		dateText: '日',
		timeText: '时间',
		calendarText: '日历',
		closeText: '关闭',
		fromText: '开始时间',
		toText: '结束时间',
		wholeText: '合计',
		fractionText: '分数',
		unitText: '单位',
		labels: ['年', '月', '日', '小时', '分钟', '秒', ''],
		labelsShort: ['年', '月', '日', '点', '分', '秒', ''],
		startText: '开始',
		stopText: '停止',
		resetText: '重置',
		lapText: '圈',
		hideText: '隐藏'
	})
})(jQuery);