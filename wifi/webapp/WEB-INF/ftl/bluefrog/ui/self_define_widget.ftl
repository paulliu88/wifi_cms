<#--自定义组件
type："1":"字符串文本","2":"整型文本","3":"浮点型文本","4":"文本区","5":"日期","6":"下拉列表","7":"复选框","8":"单选框","9":"图片集"
name
label
help
value
size
rows
cols
colspan
width
-->
<#macro selfDefineWidget
	type="1" name="" label="" value="" help="" size="" rows="" cols="" list=""
	colspan="" width="100" helpPosition="2"
	>
<#if type=="1" || type=="2" || type=="3">
	<#if type=="1"><#local vld="{maxlength:255}"/>
	<#elseif type="2"><#local vld="{digits:true,maxlength:20}"/>
	<#elseif type="3"><#local vld="{number:true,maxlength:20}"/>
	</#if>
	<@p.text colspan=colspan width=width label=label   name=name value=value size=size help=help helpPosition=helpPosition vld=vld/>
<#elseif type=="4">
<@p.textarea colspan=colspan width=width label=label  name=name value=value help=help cols=cols?string rows=rows?string helpPosition=helpPosition maxlength="255"/>
<#elseif type=="5">
<@p.text colspan=colspan width=width label=label  name=name value=value readonly="readonly" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" class="Wdate required" required="true"  size=size?string help=help helpPosition=helpPosition/>
<#elseif type=="6">
<@p.select colspan=colspan width=width label=label  name=name value=value help=help list=list!?split(",") helpPosition=helpPosition/>
<#elseif type=="7">
<@p.checkboxlist colspan=colspan width=width label=label  name=name valueList=value!?split(",") list=list!?split(",") help=help helpPosition=helpPosition/>
<#elseif type=="8">
<@p.radio colspan=colspan width=width label=label  name=name value=value list=list!?split(",") help=help helpPosition=helpPosition/>
<#elseif type=="9">
	<script>
	var swfhd;
$(function(){
	var url="../common/o_swfPicsUpload.do";
	var jsessionid = $.cookie("JSESSIONID");
	if(jsessionid) {
		url += ";jsessionid="+jsessionid;
	}
	swfhd=new SWFUpload({
		upload_url : url,
		flash_url : "${base}/thirdparty/swfupload/swfupload.swf",
		file_size_limit : "20 MB",
		file_types : "*.jpg;*.gif;*.png;*.bmp",
		file_queue_limit : 0,
		custom_settings : {
			progressTarget : "fsUploadProgress",
			cancelButtonId : "btnCancel"
		},
		debug: false,
		
		button_image_url : "${base}/res/common/img/theme/menu_search.jpg",
		button_placeholder_id : "fileQueue_007",
		button_text: "<span class='btnText'>活动图</span>",
		button_width: 52,
		button_height: 19,
		button_text_top_padding: 2,
		button_text_left_padding: 0,
		button_text_style: '.btnText{color:#666666;}',
		file_dialog_complete_handler : fileDialogComplete,
		upload_success_handler : myuploadPicsSuccess
	});
	$("#images_div  input").click(function(){

		$(this).prev().remove();
		$(this).remove();
		var srcs="";
		$.each($("#images_div img"),function(){
			var src=$(this).attr("src");
				srcs+=src+",";
		});
			$("#myimageid").val(srcs);
	});
	
})
function myuploadPicsSuccess(file,data){
	var jsonData=eval("("+data+")");//转换为json对象 
	if(jsonData.error!=null){
		alert("<@s.m 'global.prompt'/>",jsonData.error);
	}else{
	   var imgValue=$("#myimageid").val();
	   if(imgValue){
	   		$("#myimageid").val(imgValue+","+jsonData.imgUrl);
	   }else{
	   	    $("#myimageid").val(jsonData.imgUrl);
	   }
	   $("#images_div").append("<img src="+jsonData.imgUrl+" width='150px' heigth='100px'/><input  type='button' value='删除'/>");
	   $("#images_div  input").click(function(){
			$(this).prev().remove();
			$(this).remove();
			var srcs="";
			$.each($("#images_div img"),function(){
				var src=$(this).attr("src");
					srcs+=src+",";
			});
				$("#myimageid").val(srcs);
		});
	}
}
	</script>
	&emsp;&emsp;&emsp;上传活动图片:<span id="fileQueue_007"></span><span style="display: none;"><input class="cancel" id="abtnCancel" type="button" value="取消" onclick="swfhd.cancelQueue();" disabled="disabled" /></span><input type="hidden" value="${value}" name="${name}" id="myimageid"/>
	<div id="images_div">
		<#list value?split(",") as imgName>
			<#if imgName?has_content>
				<img  src="${imgName}" width="150px" height="100px"/><input  type="button" value="删除"/>
			</#if>
		</#list>
	</div>
<#elseif type=="10">
	
<#else>
not support type: "${type}"
</#if>
</#macro>
