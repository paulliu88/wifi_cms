package com.bluefrog.core.dao;

import com.bluefrog.common.hibernate3.Updater;
import com.bluefrog.core.entity.CmsUserResume;

public interface CmsUserResumeDao {
	public CmsUserResume findById(Integer id);

	public CmsUserResume save(CmsUserResume bean);

	public CmsUserResume updateByUpdater(Updater<CmsUserResume> updater);
}