package com.bluefrog.core.dao;

import com.bluefrog.common.hibernate3.Updater;
import com.bluefrog.core.entity.CmsUserExt;

public interface CmsUserExtDao {
	public CmsUserExt findById(Integer id);

	public CmsUserExt save(CmsUserExt bean);

	public CmsUserExt updateByUpdater(Updater<CmsUserExt> updater);
}