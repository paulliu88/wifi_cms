package com.bluefrog.core.dao;

import com.bluefrog.common.hibernate3.Updater;
import com.bluefrog.core.entity.CmsSiteCompany;

public interface CmsSiteCompanyDao {

	public CmsSiteCompany save(CmsSiteCompany bean);

	public CmsSiteCompany updateByUpdater(Updater<CmsSiteCompany> updater);
}