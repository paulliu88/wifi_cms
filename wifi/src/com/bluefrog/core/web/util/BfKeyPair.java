package com.bluefrog.core.web.util;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Properties;

import javax.crypto.Cipher;
import javax.naming.ConfigurationException;

public class BfKeyPair {

	//公钥加密
	public byte[] PublicEncrypt(PublicKey key,String str)throws Exception {
	    Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	    cipher.init(Cipher.ENCRYPT_MODE, key);
	    return cipher.doFinal(str.getBytes("UTF8"));
	}
	
	//公钥解密
	public byte[] PublicDECRYPT (PublicKey key,byte[]  data)throws Exception {
	    Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	    cipher.init(Cipher.DECRYPT_MODE, key);
	    return cipher.doFinal(data);
	}
	
	//私钥加密
	public byte[] PrivateEncrypt (PrivateKey key,String str)throws Exception {
	    Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	    cipher.init(Cipher.ENCRYPT_MODE, key);
	    return cipher.doFinal(str.getBytes("UTF8"));
	}
	
	//私钥解密
	public byte[] PrivateDECRYPT(PrivateKey key,byte[]  data)throws Exception  {
	    Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
	    cipher.init(Cipher.DECRYPT_MODE, key);
	    return cipher.doFinal(data);
	}

    
	public static void main(String args[]) throws Exception {
		String str = "CTX#ec-55-f9-ef-68-a6#2015-10-10#100";
	    
	    PrivateKey privatek  = getPrivateKey("d:/wm/PRIVATEKEY.txt");
	    PublicKey publick = getPubKey("d:/wm/PULIICKEY.txt");
	    
	    BfKeyPair t = new BfKeyPair();
		
		
		
		byte[] data1 = t.PrivateEncrypt(privatek,str); 
		String  mi = BASE64.encode(data1);
		System.out.println("授权信息是:"+mi);
		
		byte[] midatda = BASE64.decode(mi.getBytes());
		System.out.println("公钥解密后:"+new String(t.PublicDECRYPT(publick,midatda)));
		
	}
	
	
	  public static void saveKey(KeyPair keyPair, String publicKeyFile,   
	            String privateKeyFile) throws ConfigurationException, IOException {   
	        PublicKey pubkey = keyPair.getPublic();   
	        PrivateKey prikey = keyPair.getPrivate();   
	  
	        // save public key   
	        Properties pop = new Properties();
	        pop.put("PULIICKEY",BASE64.encode(pubkey.getEncoded()));
	        pop.store(new FileWriter(publicKeyFile), "公钥");
	  
	        // save private key   
	        pop = new Properties();
	        pop.put("PRIVATEKEY",BASE64.encode(prikey.getEncoded()));
	        pop.store(new FileWriter(privateKeyFile), "私钥");
	        
	    } 
	  
	  /**
	   * 实例化公钥
	   * 
	   * @return
	   */
	  private static PublicKey getPubKey(String path) {
	   PublicKey publicKey = null;
	   try {
	    // 自己的公钥(测试)
	     String pubKey ="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCVRiDkEKXy/KBTe+UmkA+feq1zGWIgBxkgbz7aBJGb5+eMKKoiDRoEHzlGndwFKm4mQWNftuMOfNcogzYpGKSEfC7sqfBPDHsGPZixMWzL3J10zkMTWo6MDIXKKqMG1Pgeq1wENfJjcYSU/enYSZkg3rFTOaBSFId+rrPjPo7Y4wIDAQAB";
	     
	     Properties pop = new Properties();
	     pop.load(new FileInputStream(path));
	     pubKey = pop.getProperty("PULIICKEY");
	      java.security.spec.X509EncodedKeySpec bobPubKeySpec = new java.security.spec.X509EncodedKeySpec(
	      BASE64.decode(pubKey.getBytes()));
	    // RSA对称加密算法
	    java.security.KeyFactory keyFactory;
	    keyFactory = java.security.KeyFactory.getInstance("RSA");
	    // 取公钥匙对象
	    publicKey = keyFactory.generatePublic(bobPubKeySpec);
	   } catch (NoSuchAlgorithmException e) {
	    e.printStackTrace();
	   } catch (InvalidKeySpecException e) {
	    e.printStackTrace();
	   } catch (IOException e) {
	    e.printStackTrace();
	   }
	   return publicKey;
	  }
	  
	  
	  /**
	   * 实例化公钥
	   * 
	   * @return
	   */
	  public PublicKey getPubKeyByString(String keyStore) {
	   PublicKey publicKey = null;
	   try {
	    // 自己的公钥(测试)
	     String pubKey = keyStore;
	     
	      java.security.spec.X509EncodedKeySpec bobPubKeySpec = new java.security.spec.X509EncodedKeySpec(
	      BASE64.decode(pubKey.getBytes()));
	    // RSA对称加密算法
	    java.security.KeyFactory keyFactory;
	    keyFactory = java.security.KeyFactory.getInstance("RSA");
	    // 取公钥匙对象
	    publicKey = keyFactory.generatePublic(bobPubKeySpec);
	   } catch (NoSuchAlgorithmException e) {
	    e.printStackTrace();
	   } catch (InvalidKeySpecException e) {
	    e.printStackTrace();
	   } 
	   return publicKey;
	  }
	  
	  
	  /**
	   * 实例化私钥
	   * 
	   * @return
	   */
	  private static PrivateKey getPrivateKey(String path) {
	   PrivateKey privateKey = null;
	   String priKey = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAJVGIOQQpfL8oFN75SaQD596rXMZYiAHGSBvPtoEkZvn54woqiINGgQfOUad3AUqbiZBY1+24w581yiDNikYpIR8Luyp8E8MewY9mLExbMvcnXTOQxNajowMhcoqowbU+B6rXAQ18mNxhJT96dhJmSDesVM5oFIUh36us+M+jtjjAgMBAAECgYABtnxKIabF0wBD9Pf8KUsEmXPEDlaB55LyPFSMS+Ef2NlfUlgha+UQhwsxND6CEKqS5c0uG/se/2+4l0jXz+CTYBEh+USYB3gxcMKEo5XDFOGaM2Ncbc7FAKJIkYYN2DHmr4voSM5YkVibw5Lerw0kKdYyr0Xd0kmqTok3JLiLgQJBAOGZ1ao9oqWUzCKnpuTmXre8pZLmpWPhm6S1FU0vHjI0pZh/jusc8UXSRPnx1gLsgXq0ux30j968x/DmkESwxX8CQQCpY1+2p1aX2EzYO3UoTbBUTg7lCsopVNVf41xriek7XF1YyXOwEOSokp2SDQcRoKJ2PyPc2FJ/f54pigdsW0adAkAM8JTnydc9ZhZ7WmBhOrFuGnzoux/7ZaJWxSguoCg8OvbQk2hwJd3U4mWgbHWY/1XB4wHkivWBkhRpxd+6gOUjAkBH9qscS52zZzbGiwQsOk1Wk88qKdpXku4QDeUe3vmSuZwC85tNyu+KWrfM6/H74DYFbK/MzK7H8iz80uJye5jVAkAEqEB/LwlpXljFAxTID/SLZBb+bCIoV/kvg+2145F+CSSUjEWRhG/+OH0cQfqomfg36WrvHl0g/Xw06fg31HgK";
	   
	   
	     
	   PKCS8EncodedKeySpec priPKCS8;
	   try {
		   Properties pop = new Properties();
		     pop.load(new FileInputStream(path));
		     priKey = pop.getProperty("PRIVATEKEY");
	    priPKCS8 = new PKCS8EncodedKeySpec(
	    		BASE64.decode(priKey.getBytes()));
	    KeyFactory keyf = KeyFactory.getInstance("RSA");
	    privateKey = keyf.generatePrivate(priPKCS8);
	   } catch (IOException e) {
	    e.printStackTrace();
	   } catch (NoSuchAlgorithmException e) {
	    e.printStackTrace();
	   } catch (InvalidKeySpecException e) {
	    e.printStackTrace();
	   }
	   return privateKey;
	  }
	
}
