package com.bluefrog.core.web.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class MacAddress {
    static List<String> macList;
	
    public static String hexByte(byte b) {
        String s = "000000" + Integer.toHexString(b);
        return s.substring(s.length() - 2);
    }
 
    public static List<String> getMACList() {
    	if(macList==null||macList.size()==0)
    	{
    		macList = new ArrayList<String>();
	        Enumeration<NetworkInterface> el;
	        String mac_s = "";
	        try {
	            el = NetworkInterface.getNetworkInterfaces();
	            while (el.hasMoreElements()) {
	                byte[] mac = el.nextElement().getHardwareAddress();
	                if (mac == null||mac.length!=6)
	                    continue;
	                mac_s = hexByte(mac[0]) + "-" + hexByte(mac[1]) + "-"
	                        + hexByte(mac[2]) + "-" + hexByte(mac[3]) + "-"
	                        + hexByte(mac[4]) + "-" + hexByte(mac[5]);
	               
	                Enumeration<InetAddress> ipAddress = el.nextElement().getInetAddresses();
	                while(ipAddress.hasMoreElements())
	                {
	                	InetAddress ip = ipAddress.nextElement();
	                	mac_s += "@@" + ip.getHostAddress();
	                }
	                macList.add(mac_s);
	            }
	        } catch (SocketException e1) {
	            e1.printStackTrace();
	        }
    	}
        return macList;
    }
     
    public static void main(String[] args) {
    	List<String> aa = MacAddress.getMACList();
    	for(String a:aa)
    	{
    		System.out.println(a);
    	}
         
    }
}