package com.bluefrog.core.manager;

import java.util.Set;

import com.bluefrog.core.entity.CmsUser;
import com.bluefrog.core.entity.CmsWorkflowEvent;
import com.bluefrog.core.entity.CmsWorkflowEventUser;

public interface CmsWorkflowEventUserMng {
	
	public Set<CmsWorkflowEventUser> save(CmsWorkflowEvent event,Set<CmsUser>users);

	public Set<CmsWorkflowEventUser> update(CmsWorkflowEvent event,Set<CmsUser>users);
	
	public void deleteByEvent(Integer eventId);

}