package com.bluefrog.core.manager;

import com.bluefrog.core.entity.CmsSite;
import com.bluefrog.core.entity.CmsSiteCompany;

public interface CmsSiteCompanyMng {
	public CmsSiteCompany save(CmsSite site,CmsSiteCompany bean);

	public CmsSiteCompany update(CmsSiteCompany bean);
}