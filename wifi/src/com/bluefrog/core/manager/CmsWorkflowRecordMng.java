package com.bluefrog.core.manager;

import java.util.Date;
import java.util.List;

import com.bluefrog.core.entity.CmsSite;
import com.bluefrog.core.entity.CmsUser;
import com.bluefrog.core.entity.CmsWorkflowEvent;
import com.bluefrog.core.entity.CmsWorkflowRecord;

public interface CmsWorkflowRecordMng {
	
	public List<CmsWorkflowRecord> getList(Integer eventId,Integer userId);

	public CmsWorkflowRecord save(CmsSite site, CmsWorkflowEvent event,
			CmsUser user, String opinion,Date recordTime, Integer type);

}