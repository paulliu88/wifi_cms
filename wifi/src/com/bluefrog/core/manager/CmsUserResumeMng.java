package com.bluefrog.core.manager;

import com.bluefrog.core.entity.CmsUser;
import com.bluefrog.core.entity.CmsUserResume;

public interface CmsUserResumeMng {
	public CmsUserResume save(CmsUserResume ext, CmsUser user);

	public CmsUserResume update(CmsUserResume ext, CmsUser user);
}