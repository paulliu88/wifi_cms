package com.bluefrog.core.manager;

import com.bluefrog.core.entity.CmsUser;
import com.bluefrog.core.entity.CmsUserExt;

public interface CmsUserExtMng {
	public CmsUserExt save(CmsUserExt ext, CmsUser user);

	public CmsUserExt update(CmsUserExt ext, CmsUser user);
}