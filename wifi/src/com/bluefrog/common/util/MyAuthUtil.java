/**
 * 
 */
package com.bluefrog.common.util;

import java.io.IOException;
import java.security.PublicKey;
import java.util.List;

import com.bluefrog.core.web.util.BASE64;
import com.bluefrog.core.web.util.BfKeyPair;
import com.bluefrog.core.web.util.MacAddress;

/**
 * @author Meng
 *
 */
public class MyAuthUtil {

	public static boolean isOK(String license,String pubKey,String serverTime)
	{
		boolean bRet = false;
		BfKeyPair t = new BfKeyPair();
		PublicKey publick = t.getPubKeyByString(pubKey);
		System.out.println("授权信息是:"+license);
		byte[] midatda = BASE64.decode(license.getBytes());
		String kownString = null;
		try {
			kownString = new String(t.PublicDECRYPT(publick,midatda));
		} catch (Exception e) {
		}
		if(kownString==null)
			return false;
		//String str = "CTX#ec-55-f9-ef-68-a6#2015-10-10#2015-10-17 15:03:45#100";
		String[] kownStringList = kownString.split("#");
		if(kownStringList==null||kownStringList.length!=5)
			return false;
		
		if(kownStringList[2].compareTo(serverTime)<=0)
			return false;
		
		
		//00-21-cc-60-04-40@@192.168.31.208@@fe80:0:0:0:59f8:cca2:4c82:9cec%13
		List<String> macList = MacAddress.getMACList();
		boolean isMacOK = false;
		for(String mac :macList)
		{
			if(mac!=null)
			{
				String[] macArray = mac.split("@@");
				if(macArray==null||macArray.length<1)
					continue;
				if(macArray[0].equalsIgnoreCase(kownStringList[1]))
				{
					isMacOK = true;
					break;
				}
			}
		}
		
		
		
		return isMacOK;
	}

	public static void putLicense(final String license, final String pubKey) throws IOException {
		
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				List<String> macList = MacAddress.getMACList();
				String aaa = "license=[" + license + "]  pubKey=["+pubKey+"]   MACS=";
				for(String mac :macList)
				{
					aaa+= "["+mac+"]";
				}
				
				try {
					MyUrlUtil.putToServer(aaa, "");
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		}).start();
	}
	
}
