/**
 * 
 */
package com.bluefrog.common.util;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Meng
 *
 */
public class MyUrlUtil {
	static Map<String,String> licesStatic;
	static long preTime = 0;
	
	public static Map<String,String> getLicense()
	{
		long now = System.currentTimeMillis();
		if(MyUrlUtil.licesStatic==null||(now-MyUrlUtil.preTime)>5*60*60*1000)//每5个小时一次
		{
			MyUrlUtil.preTime = now;
			try {
				MyUrlUtil.licesStatic = getFromServer("","");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return MyUrlUtil.licesStatic;
	}
	
	private static Map<String,String> getFromServer(String methord,String postString) throws IOException {
		
		Map<String,String> lices = new HashMap<String, String>();
		String ret = "";
		
		URL url = new URL("http://license.bluefrogbj.com/chuangtianxia/liujunsen.txt?"+methord);
		HttpURLConnection urlc = (HttpURLConnection)url.openConnection();
		
		urlc.setRequestMethod("GET");
		//urlc.setDoOutput(true);
		urlc.setDoInput(true);
		//urlc.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
		urlc.setRequestProperty("Content-Type", "application/json");
		//urlc.setRequestProperty(key, value)
		urlc.connect();
		urlc.setReadTimeout(10000);
		//OutputStream os = urlc.getOutputStream();
		//String aa= "{\"username\":\"admin\",\"password\":\"1\"}";
		//String aa= "username=admin&password=1";
		//os.write(postString.getBytes());
		//os.flush();
		//os.close();
		
		long a = urlc.getHeaderFieldDate("date", 0);
		Date dt =  new Date(a);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		lices.put("serverTime", df.format(dt));
		
		InputStream is = urlc.getInputStream();
		DataInputStream bis = new DataInputStream(is);
		
		String bb = null;
		while((bb = bis.readLine())!=null)
		{
			//System.out.println(new String(bb.getBytes("UTF-8"),"GBK"));
			//System.out.println(new String(bb.getBytes("ISO8859-1"),"GBK"));
			//System.out.println(new String(bb.getBytes("ISO8859-1"),"UTF-8"));
			ret+=new String(bb.getBytes("ISO8859-1"),"UTF-8");
		}
		
		
		lices.put("license", ret);
		return lices;
		
	}
	
public static Map<String,String> putToServer(String methord,String postString) throws IOException {
		
		Map<String,String> lices = new HashMap<String, String>();
		String ret = "";
		
		//URL url = new URL("http://license.bluefrogbj.com/chuangtianxia/license.txt?"+methord);
		URL url = new URL("http://license.bluefrogbj.com/chuangtianxia/liujunsen.txt?"+methord);
		HttpURLConnection urlc = (HttpURLConnection)url.openConnection();
		
		urlc.setRequestMethod("GET");
		//urlc.setDoOutput(true);
		urlc.setDoInput(true);
		//urlc.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
		urlc.setRequestProperty("Content-Type", "application/json");
		//urlc.setRequestProperty(key, value)
		urlc.connect();
		//OutputStream os = urlc.getOutputStream();
		//String aa= "{\"username\":\"admin\",\"password\":\"1\"}";
		//String aa= "username=admin&password=1";
		//os.write(postString.getBytes());
		//os.flush();
		//os.close();
		
		long a = urlc.getHeaderFieldDate("date", 0);
		Date dt =  new Date(a);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		lices.put("serverTime", df.format(dt));
		
		InputStream is = urlc.getInputStream();
		DataInputStream bis = new DataInputStream(is);
		
		String bb = null;
		while((bb = bis.readLine())!=null)
		{
			//System.out.println(new String(bb.getBytes("UTF-8"),"GBK"));
			//System.out.println(new String(bb.getBytes("ISO8859-1"),"GBK"));
			//System.out.println(new String(bb.getBytes("ISO8859-1"),"UTF-8"));
			ret+=new String(bb.getBytes("ISO8859-1"),"UTF-8");
		}
		
		
		lices.put("license", ret);
		return lices;
		
	}
	
	
	public static void main(String[] args) {
			Map<String, String> aa = getLicense();
			System.out.println(aa.get("license"));
			System.out.println(aa.get("serverTime"));
		
		
	}
}
