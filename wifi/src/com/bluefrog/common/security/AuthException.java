package com.bluefrog.common.security;

import org.apache.shiro.authc.AccountException;

/**
 * 异常
 */
@SuppressWarnings("serial")
public class AuthException  extends AccountException {
	private Object extraInformation;

	public AuthException() {

	}

	public AuthException(String msg) {
		super(msg);
	}

	public AuthException(String msg, Object extraInformation) {
		super(msg);
		this.extraInformation = extraInformation;
	}

	/**
	 * Any additional information about the exception. Generally a
	 * <code>UserDetails</code> object.
	 * 
	 * @return extra information or <code>null</code>
	 */
	public Object getExtraInformation() {
		return extraInformation;
	}

	public void clearExtraInformation() {
		this.extraInformation = null;
	}
}
