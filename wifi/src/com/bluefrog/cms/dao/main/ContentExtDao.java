package com.bluefrog.cms.dao.main;

import com.bluefrog.cms.entity.main.ContentExt;
import com.bluefrog.common.hibernate3.Updater;

public interface ContentExtDao {
	public ContentExt findById(Integer id);

	public ContentExt save(ContentExt bean);

	public ContentExt updateByUpdater(Updater<ContentExt> updater);
}