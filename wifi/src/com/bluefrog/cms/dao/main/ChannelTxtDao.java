package com.bluefrog.cms.dao.main;

import com.bluefrog.cms.entity.main.ChannelTxt;
import com.bluefrog.common.hibernate3.Updater;

public interface ChannelTxtDao {
	public ChannelTxt findById(Integer id);

	public ChannelTxt save(ChannelTxt bean);

	public ChannelTxt updateByUpdater(Updater<ChannelTxt> updater);
}