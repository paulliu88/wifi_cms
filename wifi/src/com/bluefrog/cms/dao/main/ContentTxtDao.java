package com.bluefrog.cms.dao.main;

import com.bluefrog.cms.entity.main.ContentTxt;
import com.bluefrog.common.hibernate3.Updater;

public interface ContentTxtDao {
	public ContentTxt findById(Integer id);

	public ContentTxt save(ContentTxt bean);

	public ContentTxt updateByUpdater(Updater<ContentTxt> updater);
}