package com.bluefrog.cms.dao.main;

import com.bluefrog.cms.entity.main.ContentDoc;
import com.bluefrog.common.hibernate3.Updater;

public interface ContentDocDao {
	public ContentDoc findById(Integer id);

	public ContentDoc save(ContentDoc bean);

	public ContentDoc updateByUpdater(Updater<ContentDoc> updater);
}