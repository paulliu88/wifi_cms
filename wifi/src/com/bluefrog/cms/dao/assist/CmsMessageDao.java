package com.bluefrog.cms.dao.assist;

import java.util.Date;

import com.bluefrog.cms.entity.assist.CmsMessage;
import com.bluefrog.common.hibernate3.Updater;
import com.bluefrog.common.page.Pagination;

/**
 *蓝蛙（北京）科技有限公司bluefrog研发
 */
public interface CmsMessageDao {

	public Pagination getPage(Integer siteId, Integer sendUserId,
			Integer receiverUserId, String title, Date sendBeginTime,
			Date sendEndTime, Boolean status, Integer box, Boolean cacheable,
			int pageNo, int pageSize);

	public CmsMessage findById(Integer id);

	public CmsMessage save(CmsMessage bean);

	public CmsMessage updateByUpdater(Updater<CmsMessage> updater);
	
	public CmsMessage update(CmsMessage bean);

	public CmsMessage deleteById(Integer id);

	public CmsMessage[] deleteByIds(Integer[] ids);
}