package com.bluefrog.cms.dao.assist;

import java.util.List;

import com.bluefrog.cms.entity.assist.CmsUserMenu;
import com.bluefrog.common.hibernate3.Updater;
import com.bluefrog.common.page.Pagination;

public interface CmsUserMenuDao {
	public Pagination getPage(Integer userId,int pageNo, int pageSize);
	
	public List<CmsUserMenu> getList(Integer userId,int count);

	public CmsUserMenu findById(Integer id);

	public CmsUserMenu save(CmsUserMenu bean);

	public CmsUserMenu updateByUpdater(Updater<CmsUserMenu> updater);

	public CmsUserMenu deleteById(Integer id);
}