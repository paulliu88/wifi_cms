package com.bluefrog.cms.dao.assist;

import com.bluefrog.cms.entity.assist.CmsScoreGroup;
import com.bluefrog.common.hibernate3.Updater;
import com.bluefrog.common.page.Pagination;

public interface CmsScoreGroupDao {
	public Pagination getPage(int pageNo, int pageSize);

	public CmsScoreGroup findById(Integer id);
	
	public CmsScoreGroup findDefault(Integer siteId);

	public CmsScoreGroup save(CmsScoreGroup bean);

	public CmsScoreGroup updateByUpdater(Updater<CmsScoreGroup> updater);

	public CmsScoreGroup deleteById(Integer id);
}