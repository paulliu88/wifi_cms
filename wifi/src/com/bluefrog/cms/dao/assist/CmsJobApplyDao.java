package com.bluefrog.cms.dao.assist;

import com.bluefrog.cms.entity.assist.CmsJobApply;
import com.bluefrog.common.hibernate3.Updater;
import com.bluefrog.common.page.Pagination;

public interface CmsJobApplyDao {
	public Pagination getPage(Integer userId, Integer contentId,
			Integer siteId, boolean cacheable, int pageNo, int pageSize);

	public CmsJobApply findById(Integer id);

	public CmsJobApply save(CmsJobApply bean);

	public CmsJobApply updateByUpdater(Updater<CmsJobApply> updater);

	public CmsJobApply deleteById(Integer id);
}