package com.bluefrog.cms.manager.main;

import com.bluefrog.cms.entity.main.Channel;
import com.bluefrog.cms.entity.main.ChannelExt;

public interface ChannelExtMng {
	public ChannelExt save(ChannelExt ext, Channel channel);

	public ChannelExt update(ChannelExt ext);
}