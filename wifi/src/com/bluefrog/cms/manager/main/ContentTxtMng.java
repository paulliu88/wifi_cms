package com.bluefrog.cms.manager.main;

import com.bluefrog.cms.entity.main.Content;
import com.bluefrog.cms.entity.main.ContentTxt;

public interface ContentTxtMng {
	public ContentTxt save(ContentTxt txt, Content content);

	public ContentTxt update(ContentTxt txt, Content content);
}