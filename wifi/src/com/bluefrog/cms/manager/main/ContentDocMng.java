package com.bluefrog.cms.manager.main;

import com.bluefrog.cms.entity.main.Content;
import com.bluefrog.cms.entity.main.ContentDoc;
import com.bluefrog.core.entity.CmsUser;
public interface ContentDocMng {
	public ContentDoc save(ContentDoc doc, Content content);

	public ContentDoc update(ContentDoc doc, Content content);
	
	public ContentDoc operateDocGrain(CmsUser downUser, ContentDoc doc);
	
	public ContentDoc createSwfFile(ContentDoc doc);
}