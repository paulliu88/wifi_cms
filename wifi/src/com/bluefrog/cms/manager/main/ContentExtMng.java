package com.bluefrog.cms.manager.main;

import com.bluefrog.cms.entity.main.Content;
import com.bluefrog.cms.entity.main.ContentExt;

public interface ContentExtMng {
	public ContentExt save(ContentExt ext, Content content);

	public ContentExt update(ContentExt ext);
}