package com.bluefrog.cms.template;


public class CmsModuleGenerator {
	private static String packName = "com.bluefrog.cms.template";
	private static String fileName = "bluefrog.properties";

	public static void main(String[] args) {
		new ModuleGenerator(packName, fileName).generate();
	}
}
