package com.ctc.action;

import com.bluefrog.cms.entity.main.CmsModel;
import com.bluefrog.cms.entity.main.Content;
import com.bluefrog.cms.entity.main.ContentDoc;
import com.bluefrog.cms.entity.main.ContentExt;
import com.bluefrog.cms.entity.main.ContentTxt;
import com.bluefrog.cms.entity.main.ContentType;
import com.bluefrog.cms.manager.main.ContentMng;
import com.bluefrog.cms.staticpage.StaticPageSvc;
import com.bluefrog.common.security.encoder.PwdEncoder;
import com.bluefrog.common.web.ResponseUtils;
import com.bluefrog.core.entity.CmsSite;
import com.bluefrog.core.entity.CmsUser;
import com.bluefrog.core.entity.CmsUserExt;
import com.bluefrog.core.manager.CmsUserMng;
import com.ctc.utils.Api;
import com.ctc.utils.HttpRequest;
import com.ctc.utils.ImgCompress;
import com.ctc.utils.MD512138;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import freemarker.template.TemplateException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class UserCenterAct
{
  String[][] rd = { 
    { "专题报道", "头条热点", "九个头条" }, 
    { "娱乐八卦", "网易娱乐", "手机腾讯网娱乐" }, 
    { "科技频道", "知乎每日精选", "腾讯科技" }, 
    { "财经资讯", "新浪财经", "财神理财小报", "中国企业家", "股市达人精选" }, 
    { "汽车频道", "汽车之家", "豪车汇", "车维修网", "名车图片" }, 
    { "情爱频道", "亲子频道", "健康频道", "涨姿势", "大姨吗", "前瞻奇闻", 
    "心灵咖啡网", "快速问医生", "牛男网-两性", "家庭医生在线" }, 
    { "体育频道" }, 
    { "段子", "爆笑日常", "每日神帖", "冷笑话精选", "百思不得姐", 
    "内涵社", "我们爱讲冷笑话", "挖段子", "暴走漫画", "糗事百科" }, 
    { "花瓣美女", "V女郎", "妹子图", "美空模特", "蜂鸟模特", 
    "图片网-美女", "GQ美女", "搜麻豆", "车模", "妖色", "阳光正妹" }, 
    { "测试控", "星座运程", "命理相学", "风水玄学", "塔罗占卜", 
    "佛学禅语", "十二生肖", "血型物语", "每日一禅" } };

  Integer[] cids = { Integer.valueOf(151), Integer.valueOf(153), Integer.valueOf(155), Integer.valueOf(156), Integer.valueOf(157), Integer.valueOf(158), Integer.valueOf(159), Integer.valueOf(160), 
    Integer.valueOf(161), Integer.valueOf(162) };

  @Autowired
  private ContentMng manager;

  @Autowired
  private CmsUserMng usermanager;

  @Autowired
  private PwdEncoder pwdEncoder;
  
  @Autowired
  private StaticPageSvc staticPageSvc;

  @RequestMapping({"/user/testNews.htm"})
  public void testNews(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception { Thread t = addNews();
    t.start(); }

  public Thread addNews() {
    final String result = HttpRequest.sendPost("http://iphone.myzaker.com/zaker/apps_telecom.php?for=kttech", true);
    
    Thread t = new Thread(new Runnable()
    {
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
      public void run()
      {
        try
        {
          String url = "http://www.bobo.com/spe-data/api/anchors-hot.htm";
          String result = HttpRequest.sendHttp(url);
          JSONArray arr = new JSONArray(result);
          for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = (JSONObject)arr.get(i);
            Map map = new HashMap();
            String roomId = obj.getString("roomId");
            String name = obj.getString("roomName");
            String image = obj.getString("cover");
            map.put("boboId", roomId);
            map.put("url", "http://www.bobo.com/special/bobo_share/?roomId=" + roomId);
            map.put("origin", "bobo");
            int size = Db.find("select content_id from bf_content_attr where attr_value=? and attr_name=?", new Object[] { roomId, "boboId" }).size();
            if (size == 0) {
              Content bean = UserCenterAct.this.getContent(1, map);
              UserCenterAct.this.manager.save(bean, UserCenterAct.this.getContentExt(name, image), UserCenterAct.this.getContentTxt(), UserCenterAct.this.getContentDoc(), Integer.valueOf(195), Integer.valueOf(1), Boolean.valueOf(false), UserCenterAct.this.getUser(), true);
            }
          }
        }
        catch (Exception e)
        {
          e.printStackTrace();
        }
      }
    });
    return t;
  }
  @RequestMapping({"/user/weather.htm"})
  public void weather(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    BufferedReader reader = null;
    String result = null;
    StringBuffer sbf = new StringBuffer();
    try {
      URL url = new URL(
        "http://apis.baidu.com/showapi_open_bus/weather_showapi/ip2weather?ip=" + 
        getIpAddr(request) + "&needIndex=0&needMoreDay=0");
      HttpURLConnection connection = (HttpURLConnection)url
        .openConnection();
      connection.setRequestMethod("GET");

      connection.setRequestProperty("apikey", 
        "c7356adb006243fb0ac167f3b128634c");
      connection.connect();
      InputStream is = connection.getInputStream();
      reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
      String strRead = null;
      while ((strRead = reader.readLine()) != null) {
        sbf.append(strRead);
        sbf.append("\r\n");
      }
      reader.close();
      result = sbf.toString();
    } catch (Exception e) {
      e.printStackTrace();
    }
    ResponseUtils.renderJson(response, result);
  }

  @RequestMapping({"/user/appendLoadDataToLast.htm"})
  public void appendLoadDataToLast(HttpServletRequest request, HttpServletResponse response, ModelMap model, String path, Integer totalPage, Integer nextPage) throws Exception
  {
    String home = request.getSession().getServletContext().getRealPath("/") + 
      "home/" + path + "/";
    File file = new File(home + "index_" + nextPage + ".html");
    BufferedReader tBufferedReader = new BufferedReader(
      new InputStreamReader(new FileInputStream(file), "UTF-8"));
    StringBuilder sb = new StringBuilder();
    String sTempOneLine = new String("");
    while ((sTempOneLine = tBufferedReader.readLine()) != null) {
      sb.append(sTempOneLine);
    }
    String ul = request.getParameter("ul");
    int index = sb.toString().indexOf("<ul class=\"" + ul + "\">");
    String len = "<ul class=\"" + ul + "\">";
    String result = sb.toString().substring(index + len.length());
    String newStr = result.substring(0, result.indexOf("</ul>"));
    response.setCharacterEncoding("UTF-8");
    response.getWriter().print(newStr);
  }

  @RequestMapping({"/user/appendLoadDataToLastZb.htm"})
  public void appendLoadDataToLastZb(HttpServletRequest request, HttpServletResponse response, ModelMap model, String path, Integer totalPage, Integer nextPage) throws Exception
  {
    String home = request.getSession().getServletContext().getRealPath("/") + 
      "home/" + path + "/";
    File file = new File(home + "index_" + nextPage + ".html");
    BufferedReader tBufferedReader = new BufferedReader(
      new InputStreamReader(new FileInputStream(file), "UTF-8"));
    StringBuilder sb = new StringBuilder();
    String sTempOneLine = new String("");
    while ((sTempOneLine = tBufferedReader.readLine()) != null) {
      sb.append(sTempOneLine);
    }
    String ul = request.getParameter("ul");
    int index = sb.toString().indexOf("<div class=\"linexiuchang\" id=\"append\">");
    String len = "<div class=\"linexiuchang\" id=\"append\">";
    String result = sb.toString().substring(index + len.length());
    String newStr = result.substring(0, result.indexOf("</span>"));
    response.setCharacterEncoding("UTF-8");
    response.getWriter().print(newStr);
  }

  @RequestMapping({"/user/mv.htm"})
  public void movieTwo(HttpServletRequest request, HttpServletResponse response, Model model)
  {
    new Thread(new Runnable()
    {
      public void run()
      {
        try
        {
          String url = "http://api.laifeng.com/open/v1/anchors?appId=1000&sign=79550f5b810e73d75279ddc0ac4e4515&pageLen=100&pageNo=1&cpsId=000145";
          String result = HttpRequest.sendHttp(url);
          JSONArray arr = new JSONObject(result).getJSONArray("data");

          for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = (JSONObject)arr.get(i);
            String name = obj.getString("name");
            String cover = obj.getString("cover");
            String online = obj.getString("online");
            String link = obj.getString("link");
            String id = obj.getString("id");
            Map map = new HashMap();
            map.put("online", online);
            map.put("url", link);
            map.put("laifid", id);
            map.put("origin", "laifeng");
            int size = Db.find("select content_id from bf_content_attr where attr_value=? and attr_name=?", new Object[] { id, "laifid" }).size();
            if (size == 0) {
              UserCenterAct.this.manager.save(UserCenterAct.this.getContent(1, map), UserCenterAct.this.getContentExt(name, cover), UserCenterAct.this.getContentTxt(), UserCenterAct.this.getContentDoc(), Integer.valueOf(196), Integer.valueOf(1), Boolean.valueOf(false), UserCenterAct.this.getUser(), true);
            }
          }
        }
        catch (Exception e)
        {
          e.printStackTrace();
        }
      }
    }).start();
  }

  @RequestMapping({"/user/mv_two.htm"})
  public void mv_two(HttpServletRequest request, HttpServletResponse response, Model model)
  {
    new Thread(new Runnable()
    {
      public void run()
      {
        List list = Db.find("select content_id from bf_content_attr where attr_name=?", new Object[] { "needSaveId" });
        int size = list.size();
        for (int i = 0; i < size; i++) {
          Record record = new Record();
          record.set("content_id", ((Record)list.get(i)).getInt("content_id"));
          record.set("attr_name", "origin");
          record.set("attr_value", "zaker");
          Db.save("bf_content_attr", record);
          System.out.println(i);
        }
        System.out.println(size + "==========size");
      }
    }).start();
  }

  public static void main(String[] args)
  {
    System.out.println(MD512138.encode("18513993084"));
  }
  
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public Content getContent(int modelId, Map map)
  {
    Content bean = new Content();
    ContentType type = new ContentType();
    type.setId(Integer.valueOf(1));
    bean.setType(type);
    CmsSite site = new CmsSite();
    site.setRelativePath(Boolean.valueOf(false));
    site.setId(Integer.valueOf(3));
    site.setStaticIndex(Boolean.valueOf(false));
    site.setIndexToRoot(Boolean.valueOf(false));
    bean.setSite(site);
    CmsModel model = new CmsModel();
    model.setId(Integer.valueOf(modelId));
    bean.setModel(model);
    bean.setAttr(map);
    return bean;
  }

  public ContentExt getContentExt(String title, String titleImg) {
    ContentExt ext = new ContentExt();
    ext.setTitle(title);
    ext.setTitleImg(titleImg);
    return ext;
  }

  public ContentTxt getContentTxt() {
    ContentTxt txt = new ContentTxt();
    return txt;
  }

  public ContentDoc getContentDoc() {
    ContentDoc doc = new ContentDoc();
    return doc;
  }

  public CmsUser getUser() {
    CmsUser user = new CmsUser();
    user.setUsername("admin");
    user.setId(Integer.valueOf(1));
    return user;
  }

  @RequestMapping({"/user/wode.htm"})
  public String wode(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    request.setCharacterEncoding("UTF-8");
    String username = "";
    Cookie[] c = request.getCookies();
    if (c != null) {
      for (Cookie cookie : c) {
        if ("username".equals(cookie.getName())) {
          username = cookie.getValue();
        }
      }
    }
    if (!StringUtils.isBlank(username)) {
      String[] arr = username.split("＃");
      if (arr.length > 1) {
        username = arr[0];
      }
      CmsUser user1 = this.usermanager.findByUsername(username);
      if (!StringUtils.isBlank(user1.getPhone()))
        model.put("needLogin", "no");
      else {
        model.put("needLogin", "yes");
      }
      model.put("user", user1);
      model.put("resSys", "/r/cms");
    } else {
      return "redirect:http://m.kunteng.org";
    }

    if (!StringUtils.isBlank(username)) {
      Record mac = Db.findFirst("select * from x_user where user_id=?", new Object[] { 
        username.trim() });
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      String date = format.format(new Date());
      String dateTow = mac.getStr("date");
      if ((!StringUtils.isBlank(dateTow)) && (!dateTow.contains(date)))
        mac.set("sign", "今天已签到");
      else {
        mac.set("sign", "立即签到");
      }
      model.put("userjf", mac);
    }
    if (StringUtils.isBlank(username)) {
      model.put("userjf", new Record());
    }

    String cookieUser = request.getParameter("cookieUser");
    model.put("cookieUser", cookieUser);
    return "/WEB-INF/t/cms/tz/tz/jfinal_html/wode_index.html";
  }

  @RequestMapping({"/user/signIn.htm"})
  public void signIn(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    if (!StringUtils.isBlank(username)) {
      Record re = Db.findFirst("select * from x_user where user_id=?", new Object[] { 
        username.trim() });
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      String date = format.format(new Date());
      String dateTow = re.getStr("date");
      Record mac = Db.findFirst("select * from x_user where user_id=?", new Object[] { 
        username.trim() });
      if ((StringUtils.isBlank(dateTow)) || (!dateTow.contains(date))) {
        Db.update(
          "update x_user set coming=coming+1,date=now(),count=count+1 where user_id=?", new Object[] { 
          username.trim() });
        mac.set("jifen", Integer.valueOf(1));
        Db.save("x_user_si", 
          new Record().set("user_id", username.trim()).set(
          "date", new Date()));
      } else {
        mac.set("jifen", Integer.valueOf(0));
        mac.set("error", "今天已签到");
      }
      ResponseUtils.renderJson(response, JsonKit.toJson(mac));
    }
  }

  @RequestMapping({"/user/signInCheck.htm"})
  public void signInCheck(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    if (!StringUtils.isBlank(username)) {
      int status = checkTime(username);
      ResponseUtils.renderText(response, status + "");
    }
  }

  public int checkTime(String username) {
    Record re = Db.findFirst("select * from x_user where user_id=?", new Object[] { 
      username });
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    String date = format.format(new Date());
    String dateTow = re.getStr("date");
    if ((StringUtils.isBlank(dateTow)) || (!dateTow.contains(date))) {
      return 1;
    }
    return 0;
  }

  @RequestMapping({"/user/add.htm"})
  public void add(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    String count = request.getParameter("count");
    if ("1".equals(count))
      count = "1";
    else if ("2".equals(count))
      count = "1";
    else {
      return;
    }
    String origin = request.getParameter("origin");
    Record returnCount = new Record();
    if ((origin.equals("bobo")) || (origin.equals("laf"))) {
      origin = "zb";
    }
    int logSize = 
      Db.find("select user_id from x_user_jf_log where user_id=? and date_format(date,'%Y-%m-%d') =date_format(now(),'%Y-%m-%d') ", new Object[] { 
      username }).size();
    returnCount.set("status", Integer.valueOf(0));
    if (logSize < 9) {
      Record channle = 
        Db.findFirst(
        "select e.channel_name name,c.channel_path path  from bf_channel c join bf_channel_ext e on c.channel_id=e.channel_id where c.channel_path=?", new Object[] { 
        origin });
      int size = Db.find(
        "select id from x_user_jf where user_id=? and origin=? ", new Object[] { 
        username.trim(), channle.getStr("path") }).size();

      String contentId = request.getParameter("contentId");
      Record mac = 
        Db.findFirst(
        "select id,date from x_user_jf_log where content_id=? and user_id=?", new Object[] { 
        contentId, username.trim() });
      if (mac == null) {
        Db.save("x_user_jf_log", 
          new Record().set("content_id", contentId)
          .set("date", new Date())
          .set("user_id", username.trim()));
        if (size == 0)
          Db.save("x_user_jf", 
            new Record().set("origin", channle.getStr("path"))
            .set("name", channle.getStr("name"))
            .set("date", new Date())
            .set("count", count)
            .set("user_id", username.trim()));
        else {
          Db.update(
            "update x_user_jf set count=count+? where user_id=? and origin=? ", new Object[] { 
            count, username.trim(), channle.getStr("path") });
        }
        returnCount.set("status", Integer.valueOf(1));
        Db.update("update x_user set count=count+? where user_id=?", new Object[] { 
          count, username });
      } else {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date = format.format(new Date());
        String dateTow = mac.getStr("date");
        if (!dateTow.contains(date)) {
          if (size == 0)
            Db.save("x_user_jf", 
              new Record()
              .set("origin", channle.getStr("path"))
              .set("name", channle.getStr("name"))
              .set("date", new Date())
              .set("count", count)
              .set("user_id", username.trim()));
          else {
            Db.update(
              "update x_user_jf set count=count+? where user_id=? and origin=? ", new Object[] { 
              count, username.trim(), channle.getStr("path") });
          }
          returnCount.set("status", Integer.valueOf(1));
          Db.update(
            "update x_user set count=count+? where user_id=?", new Object[] { 
            count, username });
          Db.update("x_user_jf_log", mac.set("date", new Date()));
        }
      }
    }
    ResponseUtils.renderJson(response, JsonKit.toJson(returnCount));
  }

  @RequestMapping({"/user/user_upload.htm"})
  public void userUpload(HttpServletRequest request, HttpServletResponse response, ModelMap model, @RequestParam(value="file", required=false) MultipartFile file)
    throws Exception
  {
    InputStream input = file.getInputStream();
    byte[] by = new byte[1024];
    int len = 0;
    String path = request.getSession().getServletContext().getRealPath("/") + 
      "user/images/";
    String suxx = file.getOriginalFilename().substring(
      file.getOriginalFilename().indexOf("."));
    String uuid = UUID.randomUUID().toString();
    String fileName = uuid + suxx;
    SimpleDateFormat format = new SimpleDateFormat("YYYYMMDD");
    String date = format.format(new Date()) + "/";
    File outFile = new File(path + date);
    if (!outFile.exists()) {
      outFile.mkdirs();
    }

    File oFile = new File(path + date + fileName);
    FileOutputStream out = new FileOutputStream(oFile);
    while ((len = input.read(by)) != -1) {
      out.write(by, 0, len);
    }
    out.flush();
    out.close();
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    if (!StringUtils.isBlank(username)) {
      Record user = Db.findFirst(
        "select user_id from bf_user where username=?", new Object[] { 
        username.trim() });
      Db.update("update bf_user_ext set user_img=? where user_id=?", new Object[] { 
        "/user/images/" + date + fileName, user.getInt("user_id") });
    }
    ImgCompress imgCom = new ImgCompress(path + date + fileName);
    int w = imgCom.width;
    int h = imgCom.height;
    if (w > h)
      imgCom.resizeFix(112 * w / h, 112, path + date + fileName);
    else {
      imgCom.resizeFix(112, 112 * h / w, path + date + fileName);
    }
    imgCom.resize(w, h, path + date + fileName);
    ResponseUtils.renderText(response, "/user/images/" + date + fileName);
  }

  @RequestMapping({"/user/user_sign.htm"})
  public String userSign(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    List list = Db.find(
      "select date from x_user_si where user_id=?", new Object[] { username });
    model.put("userList", list);
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat format1 = new SimpleDateFormat("dd");
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < list.size(); i++) {
      Date date = format.parse(((Record)list.get(i)).getStr("date"));
      String ds = format1.format(date);
      int d = Integer.parseInt(ds);
      if (i < list.size() - 1)
        sb.append(d + ",");
      else {
        sb.append(d);
      }
    }
    model.put("dates", sb.toString());
    return "/WEB-INF/t/cms/tz/tz/jfinal_html/wode_signin.html";
  }

  @RequestMapping({"/user/wodejf.htm"})
  public String wodejf(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    String[] arr = username.split("＃");
    if (arr.length > 1) {
      username = arr[0];
    }
    Record user = Db.findFirst("select * from x_user where user_id=?", new Object[] { 
      username });
    model.put("user", user);
    CmsUser cmsUser = this.usermanager.findByUsername(username.trim());
    model.put("cmsUser", cmsUser);
    List jfList = 
      Db.find("select name,date, count from x_user_jf where user_id=? group by name order by date desc", new Object[] { 
      username.trim() });
    model.put("jfList", jfList);
    return "/WEB-INF/t/cms/tz/tz/jfinal_html/wodejf_index.html";
  }

  @RequestMapping({"/user/wode_center.htm"})
  public String wodeCenter(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    if (!StringUtils.isBlank(username)) {
      Record detail = Db.findFirst("select e.user_img,e.phone,e.birthday,e.gender,j.password pwd from bf_user_ext e inner join bf_user u inner join jo_user j on e.user_id=u.user_id and  j.user_id=u.user_id where u.username=?", new Object[] { username });
      if (detail == null) {
        detail = new Record();
      }

      String gender = request.getParameter("gender");
      String birthday = request.getParameter("birthday");
      if (!StringUtils.isBlank(gender)) {
        if ("1".equals(gender))
          detail.set("gender", Boolean.valueOf(true));
        else {
          detail.set("gender", Boolean.valueOf(false));
        }
      }
      if (!StringUtils.isBlank(birthday)) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(birthday);
        detail.set("birthday", date);
      }
      model.put("detail", detail);
      if (StringUtils.isBlank(detail.getStr("phone")))
        model.put("login", "yes");
      else {
        model.put("login", "no");
      }
      model.put("username", username);
      String pwd = request.getParameter("pwd");
      model.put("pwd", pwd);
    }
    return "/WEB-INF/t/cms/tz/tz/jfinal_html/wode_center_index.html";
  }

  @RequestMapping({"/user/wode_center_enohp.htm"})
  public String wodeCenterEnoph(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    String uncode = request.getParameter("uncode");
    if (!StringUtils.isBlank(uncode)) {
      username = MD512138.decode(uncode);
      String backUrl = request.getParameter("backUrl");
      model.put("uncode", "yes");
      model.put("backUrl", backUrl);
    } else {
      model.put("uncode", "no");
    }
    if (!StringUtils.isBlank(username)) {
      Record user = 
        Db.findFirst(
        "select e.phone from  bf_user_ext e inner join bf_user u on e.user_id=u.user_id where u.username=?", new Object[] { 
        username.trim() });
      if (StringUtils.isBlank(user.getStr("phone")))
        model.put("newPwd", Integer.valueOf(1));
      else {
        model.put("newPwd", Integer.valueOf(0));
      }
      model.put("user", user);
    }
    model.put("username", username);
    return "/WEB-INF/t/cms/tz/tz/jfinal_html/wode_center_phone.html";
  }

  @RequestMapping({"/user/wode_updateEnohp.htm"})
  public String wodeupdateEnohp(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    if (!StringUtils.isBlank(username)) {
      String phone = request.getParameter("phone");
      int i = Db.update(
        "update bf_user_ext e inner join bf_user u on e.user_id=u.user_id set e.phone=? where u.username=? ", new Object[] { 
        phone, username.trim() });
      if (i == 1) {
        String uncode = request.getParameter("uncode");
        if (!StringUtils.isBlank(uncode))
        {
          if ("yes".equals(uncode)) {
            String backUrl = request.getParameter("backUrl");
            if (!StringUtils.isBlank(backUrl)) {
              model.put("iframe", "http://" + backUrl);
              return "/WEB-INF/t/cms/tz/tz/jfinal_html/wodejfdh_index.html";
            }
          }
        }
      }
    }
    return "redirect:/user/wode_center.htm?username=" + username;
  }

  @RequestMapping({"/user/wode_checkEnohp.htm"})
  public void wodeCheckEnohp(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String phone = request.getParameter("phone");
    if (!StringUtils.isBlank(phone)) {
      int len = Db.find("select user_id from bf_user_ext where phone=?", new Object[] { 
        phone }).size();
      if (len >= 1)
        ResponseUtils.renderText(response, "in");
      else
        ResponseUtils.renderText(response, "zone");
    }
  }

  @RequestMapping({"/user/wode_updatePwd.htm"})
  public String wodeUpdatePwd(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception
  {
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    if (!StringUtils.isBlank(username)) {
      Record user = Db.findFirst("select password from jo_user where username=?", new Object[] { username.trim() });
      if ("9bc69afc31a1d252d53e5a7b8642f3f4".equals(user.getStr("password")))
        model.put("newPwd", Integer.valueOf(1));
      else {
        model.put("newPwd", Integer.valueOf(0));
      }
      model.put("user", user);
    }
    model.put("username", username);
    String error = request.getParameter("error");
    if (!StringUtils.isBlank(error)) {
      model.put("error", "原密码错误");
    }
    String gender = request.getParameter("gender");
    String birthday = request.getParameter("birthday");
    model.put("gender", gender);
    model.put("birthday", birthday);
    return "/WEB-INF/t/cms/tz/tz/jfinal_html/wode_center_pwd.html";
  }

  @RequestMapping({"/user/wode_savePwd.htm"})
  public String wodeSavePwd(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    if (!StringUtils.isBlank(username)) {
      Record user = Db.findFirst(
        "select password from jo_user where username=?", new Object[] { 
        username.trim() });
      if (!"9bc69afc31a1d252d53e5a7b8642f3f4".equals(user
        .getStr("password"))) {
        String oldphone = request.getParameter("oldphone");
        if (!this.pwdEncoder.encodePassword(oldphone).equals(
          user.getStr("password"))) {
          return "redirect:/user/wode_updatePwd.htm?username=" + 
            username + "&error=error";
        }
      }
      String phone = request.getParameter("newphone");
      Db.update("update jo_user set password=? where username=?", new Object[] { this.pwdEncoder.encodePassword(phone), username.trim() });
    }

    String phone = request.getParameter("newphone");
    String gender = request.getParameter("gender");
    String birthday = request.getParameter("birthday");

    return "redirect:/user/wode_center.htm?username=" + username + 
      "&gender=" + gender + "&birthday=" + birthday + "&pwd=" + phone;
  }

  @RequestMapping({"/user/wode_login.htm"})
  public String wodeLogin(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String phone = request.getParameter("phone");
    String password = request.getParameter("inputcode");
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
		if("username".equals(cookie.getName())){
			username=cookie.getValue();
		}
	}
    Record size;
    if ((!StringUtils.isBlank(phone)) && (!StringUtils.isBlank(password))) {
      String code = (String)request.getSession().getAttribute("usercode");
      size = Db.findFirst("select e.phone,u.username from bf_user_ext e inner join bf_user u on e.user_id=u.user_id where e.phone=?", new Object[] { phone });
      if (!password.equals(code)) {
        model.put("error", "验证码错误");
        model.put("phone", phone);
        model.put("username", username);
        return "/WEB-INF/t/cms/tz/tz/jfinal_html/wode_login.html";
      }

      if (size == null)
        Db.update("update bf_user_ext e inner join bf_user u on e.user_id=u.user_id  set phone=? where u.username=?", new Object[] { phone, username.trim() });
      else {
        username = size.getStr("username");
      }

    }

    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        cookie.setValue(username);
        cookie.setMaxAge(604800);
        response.addCookie(cookie);
      }
    }

    return "redirect:/user/wode.htm?#wode";
  }

  @RequestMapping({"/user/wode_center_update.htm"})
  public void wodeCenterUpdate(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String gender = request.getParameter("gender");
    if (StringUtils.isBlank(gender)) {
      gender = "1";
    }
    String birthday = request.getParameter("birthday");
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    StringBuilder sb = new StringBuilder();
    if (!StringUtils.isBlank(username)) {
      sb.append("gender=?,birthday=?");
      Db.update(
        "update bf_user_ext e inner join bf_user u on e.user_id=u.user_id set " + 
        sb.toString() + " where u.username=?", new Object[] { 
        gender, 
        birthday, username.trim() });
      Record user = Db.findFirst("select informationcount from x_user where user_id=?", new Object[] { username });
      if ((!StringUtils.isBlank(user.get("informationcount").toString())) && 
        (user.getInt("informationcount").intValue() == 0))
        Db.update("update x_user set informationcount=informationcount+3,count=count+3,informationdate=now() where user_id=?", new Object[] { username });
    }
  }

  @RequestMapping({"/user/wode_userLogin.htm"})
  public String wodeUserLogin(HttpServletRequest request, HttpServletResponse response, ModelMap model)
    throws Exception
  {
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    if (!StringUtils.isBlank(username)) {
      model.put("username", username);
    }
    return "/WEB-INF/t/cms/tz/tz/jfinal_html/wode_login.html";
  }

  @RequestMapping({"/user/wodejfdh.htm"})
  public String wodejfdh(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    String[] arr = username.split("＃");
    if (arr.length > 1) {
      username = arr[0];
    }
    Record user = Db.findFirst("select * from x_user where user_id=?", new Object[] { 
      username });
    model.put("user", user);
    model.put("username", username);

    List free = 
      Db.find("select e.title,e.title_img,e.author,c.content_id,a.attr_value dh from bf_content c inner join bf_content_ext e on c.content_id=e.content_id left join bf_content_attr a on c.content_id=a.content_id  where c.channel_id=193 and c.status<>3 and  a.attr_name='dh' order by e.release_date desc");
    List product = 
      Db.find("select e.title,e.title_img,e.author,c.content_id,a.attr_value dh from bf_content c inner join bf_content_ext e on c.content_id=e.content_id left join bf_content_attr a on c.content_id=a.content_id where c.channel_id=194 and c.status<>3 and a.attr_name='dh'  order by e.release_date desc");
    model.put("free", free);
    model.put("product", product);
    model.put("userencode", MD512138.encode(username.trim()));
    model.put("iframe", "http://jf.m.kunteng.org/user/points?userId=" + MD512138.encode(username.trim()));
    return "/WEB-INF/t/cms/tz/tz/jfinal_html/wodejfdh_index.html";
  }

  @RequestMapping({"/user/wodejfdh_detail.htm"})
  public String wodejfdhdetail(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String username = "";
    Cookie[] cs = request.getCookies();
    for (Cookie cookie : cs) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    if (!StringUtils.isBlank(username)) {
      String c = request.getParameter("c");
      Record re = 
        Db.findFirst(
        "select e.title,e.author,t.txt,t.txt1,e.content_img from bf_content_ext e inner join bf_content_attr a on e.content_id=a.content_id inner join bf_content_txt t on e.content_id=t.content_id where a.attr_name='dh'  and e.content_id=? and a.attr_value='立即兑换'", new Object[] { 
        c.trim() });
      model.put("re", re);
      if (re == null) {
        re = new Record();
      }
    }
    return "/WEB-INF/t/cms/tz/tz/jfinal_html/wodejfdh_detail_index.html";
  }

  @RequestMapping({"/user/jonSnowRegister.htm"})
  public void jonSnowRegister(HttpServletRequest request, HttpServletResponse response, ModelMap model)
    throws Exception
  {
    String code = request.getParameter("gw_id");
    String mac = request.getParameter("mac");
    String phone = request.getParameter("phone");
    String name = "";
    Cookie[] c = request.getCookies();
    int stauts = 0;
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        name = cookie.getValue();
        stauts = 1;
      }
    }
    CmsUser cmsUser = null;
    Record re = new Record();
    if (StringUtils.isBlank(name)) {
      String[] arr = name.split("＃");
      if (arr.length > 1) {
        name = arr[0];
      }
      if (!StringUtils.isBlank(phone)) {
        Object attr = new HashMap();
        CmsUserExt ext = new CmsUserExt();
        ((Map)attr).put("mac", mac);
        ((Map)attr).put("phone", phone);
        ((Map)attr).put("gw_id", code);
        ext.setPhone(phone);
        int size = Db.find("select user_id from bf_user where username=?", new Object[] { phone }).size();
        if (size == 0) {
          cmsUser = this.usermanager.registerMember(phone, null, 
            "zxx12138", getIpAddr(request), Integer.valueOf(1), Integer.valueOf(0), false, ext, 
            (Map)attr);
          Db.save("x_user", new Record().set("user_id", phone));
        }
        else {
          cmsUser = this.usermanager.findByUsername(phone);
        }
        request.getSession().setAttribute("userWode", cmsUser);
        name = phone;
      } else {
        Record record = 
          Db.findFirst("select max(user_id) user_id from bf_user");
        String username = (record.getInt("user_id") + 1) + "";
        CmsUserExt ext = new CmsUserExt();
        Map attr = new HashMap();
        cmsUser = this.usermanager.registerMember(username, null, 
          "zxx12138", getIpAddr(request), Integer.valueOf(1), Integer.valueOf(0), false, ext, attr);
        Db.save("x_user", new Record().set("user_id", username));
        name = username;
      }
    }
    else
    {
      String[] arr = name.split("＃");
      if (arr.length > 1) {
        name = arr[0];
      }
      cmsUser = this.usermanager.findByUsername(name);
    }

    if (stauts == 0) {
      Cookie cookie = new Cookie("username", cmsUser.getUsername());
      cookie.setMaxAge(604800);
      response.addCookie(cookie);
    }
    re.set("username", cmsUser.getUsername());
    String json = JsonKit.toJson(re);
    ResponseUtils.renderJson(response, json);
  }

  @RequestMapping({"/user/getJifen.htm"})
  public void jonSnowJifen(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
    String username = "";
    Cookie[] c = request.getCookies();
    for (Cookie cookie : c) {
      if ("username".equals(cookie.getName())) {
        username = cookie.getValue();
      }
    }
    if (!StringUtils.isBlank(username)) {
      Record record = Db.findFirst(
        "select count,coming from x_user where user_id=?", new Object[] { 
        username.trim() });
      String json = JsonKit.toJson(record);
      ResponseUtils.renderJson(response, json);
    }
  }

  public String getIpAddr(HttpServletRequest request)
  {
    String ip = request.getHeader("x-forwarded-for");
    if ((ip == null) || (ip.length() == 0) || ("unknown".equalsIgnoreCase(ip))) {
      ip = request.getHeader("Proxy-Client-IP");
    }
    if ((ip == null) || (ip.length() == 0) || ("unknown".equalsIgnoreCase(ip))) {
      ip = request.getHeader("WL-Proxy-Client-IP");
    }
    if ((ip == null) || (ip.length() == 0) || ("unknown".equalsIgnoreCase(ip))) {
      ip = request.getRemoteAddr();
    }
    
    int index = ip.indexOf(',');
	if (index != -1) {
		return ip.substring(0, index);
	} 
    return ip;
  }

  @RequestMapping({"/user/jonSnowSeeMovie.htm"})
  public void jonSnowUser(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    String uid = request.getParameter("uid");
    if (!StringUtils.isBlank(uid)) {
      String userId = MD512138.decode(uid);
      Record user = Db.findFirst("select u.count,e.* from bf_user_ext e inner join bf_user fu inner join x_user u on fu.username=u.user_id and fu.user_id=e.user_id where u.user_id=?", new Object[] { 
        userId });
      if (user != null)
        ResponseUtils.renderJson(response, JsonKit.toJson(user));
      else
        ResponseUtils.renderJson(response, 
          "{\"status\":\"-1\",\"user\":\"null\"}");
    }
    else {
      ResponseUtils.renderJson(response, "{'status':'-1','uid':'error'}");
    }
  }

  @RequestMapping({"/user/jonSnowConsumption.htm"})
  public void jonSnowUserConsumption(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    String uid = request.getParameter("uid");
    String count = request.getParameter("count");
    String ip = getIpAddr(request);
    int size = Db.find("select ip from x_user_ip where ip=?", new Object[] { ip }).size();
    if (size == 1) {
      if ((!StringUtils.isBlank(uid)) && (!StringUtils.isBlank(count))) {
        String userId = MD512138.decode(uid);
        int i = Db.update(
          "update x_user set count=count-? where user_id=?", new Object[] { 
          count, userId });
        if (i == 1)
        {
          ResponseUtils.renderJson(response, "{\"status\":\"ok\"}");
        }
        else ResponseUtils.renderJson(response, 
            "{\"status\":\"error\",\"userId\":\"error\"}"); 
      }
      else
      {
        ResponseUtils.renderJson(response, 
          "{\"status\":\"error\",\"uid\":\"null\"}");
      }
    }
    else ResponseUtils.renderJson(response, "{\"status\":\"error\"}");
  }

  @RequestMapping({"/user/jonSnowDetail.htm"})
  public void jonSnowUserDetail(HttpServletRequest request, HttpServletResponse response, ModelMap model)
  {
    String pageNo = request.getParameter("pageNo");
    String pageSize = request.getParameter("pageSize");
    if (StringUtils.isBlank(pageNo)) {
      pageNo = "1";
    }
    if (StringUtils.isBlank(pageSize)) {
      pageSize = "100";
    }
    String phone = request.getParameter("phone");
    StringBuilder sb = new StringBuilder();
    sb.append(" from bf_user_ext e inner join x_user u on e.user_id=u.user_id");
    if (!StringUtils.isBlank(phone)) {
      sb.append(" where e.phone='" + phone + "'");
    }
    sb.append(" order by u.count desc");
    Page page = Db.paginate(Integer.parseInt(pageNo), 
      Integer.parseInt(pageSize), "select u.count,e.* ", 
      sb.toString());
    ResponseUtils.renderJson(response, JsonKit.toJson(page));
  }

  @RequestMapping({"/user/jonSnowSms.htm"})
  public void jonSnowUserSms(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    String sms = request.getParameter("sms");
    if (!StringUtils.isBlank(sms)) {
      String type = request.getParameter("type");
      String result = Api.smsSend(request, sms, Integer.parseInt(type));
      try {
        JSONObject json = new JSONObject(result);
        System.out.println(json);
      } catch (JSONException e) {
        e.printStackTrace();
      }
    }
  }

  @RequestMapping({"/user/jonSnowSmsCheck.htm"})
  public void jonSnowUserSmsCheck(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    String inputcode = request.getParameter("inputcode");
    String code = (String)request.getSession().getAttribute("usercode");
    if (!code.equals(inputcode))
      ResponseUtils.renderJson(response, "{\"code\":\"error\"}");
    else
      ResponseUtils.renderJson(response, "{\"code\":\"ok\"}");
  }

  @RequestMapping({"/user/currentMac.htm"})
  public void jonSnowCurrenMak(HttpServletRequest request, HttpServletResponse response, ModelMap model)
  {
    String date = request.getParameter("date");
    List rlist = Db.find("select rtime,replace(routemac,\"gw_id=\",\"\") routemac,sum(pv) pv,count(distinct phonemac) uv from ipsumary where routemac like 'gw_id=%' and rtime =? group by rtime,routemac", new Object[] { date });
    ResponseUtils.renderJson(response, JsonKit.toJson(rlist));
  }
  @RequestMapping({"/user/jonSnowSumary.htm"})
  public String jonSnowSumary(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    String pageNo = request.getParameter("pageNo");
    String date = request.getParameter("date");
    String dateTime = request.getParameter("dateTime");
    if (StringUtils.isBlank(pageNo)) {
      pageNo = "1";
    }
    if (!StringUtils.isBlank(dateTime)) {
      StringBuilder sb = new StringBuilder();
      sb.append("  from ipsumary where routemac like 'gw_id=%' and rtime =? ");
      Page rpage = Db.paginate(Integer.parseInt(pageNo), 30, "select * ", sb.toString(), new Object[] { dateTime });
      model.put("rpage", rpage);
      model.put("dateTime", dateTime);
      return "/WEB-INF/t/cms/tz/tz/jfinal_html/sumary1.html";
    }
    StringBuilder sb = new StringBuilder();
    sb.append("from ipsumary  where routemac like 'gw_id=%'   ");
    if (!StringUtils.isBlank(date)) {
      sb.append(" and rtime='" + date + "'");
      model.put("date", date);
    }
    sb.append(" group by rtime order by rtime desc");
    Page rpage = Db.paginate(Integer.parseInt(pageNo), 30, "select rtime,count(distinct routemac) routemac,sum(pv) pv,count(distinct phonemac) uv ", sb.toString());
    model.put("rpage", rpage);
    return "/WEB-INF/t/cms/tz/tz/jfinal_html/sumary.html";
  }
  @RequestMapping({"/user/jonSnowContents.htm"})
  public String jonSnowContents(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
    if ((StringUtils.isBlank(startDate)) && (StringUtils.isBlank(endDate))) {
      Date d = new Date();
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      startDate = format.format(new Date(d.getTime() - 604800000L));
      endDate = format.format(d);
    }
    List clist = Db.find("select e.title,c.*,channelext.channel_name form,channelexttwo.channel_name whereHim from bf_content_ext  e inner join (select  contentid,sum(pv) pv, sum(routemac) rv,sum(phonemac) uv from ipurl  where rtime>=? and rtime<=? group by contentid order by sum(pv) desc limit 300 ) c inner join bf_content content inner join bf_channel  channel inner join bf_channel_ext channelext inner join bf_channel_ext channelexttwo on  e.content_id=c.contentid and content.content_id=e.content_id and content.channel_id=channel.channel_id and channelext.channel_id=channel.parent_id and content.channel_id=channelexttwo.channel_id ", new Object[] { startDate, endDate });
    model.put("clist", clist);
    model.put("startDate", startDate);
    model.put("endDate", endDate);
    return "/WEB-INF/t/cms/tz/tz/jfinal_html/sumary3.html";
  }

  @RequestMapping({"/user/ipindex.htm"})
  public String jonSnowIpindex(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    String date = request.getParameter("date");
    String pageNo = request.getParameter("pageNo");
    String type = request.getParameter("type");
    if (StringUtils.isBlank(pageNo)) {
      pageNo = "1";
    }
    StringBuilder sb = new StringBuilder();
    sb.append(" from  " + type);
    if (!StringUtils.isBlank(date)) {
      sb.append(" where rtime='" + date + "'");
      model.put("date", date);
    }
    sb.append(" group by rtime,routemac order by rtime desc ");
    Page rlist = Db.paginate(Integer.parseInt(pageNo), 50, "select cname,rtime,routemac routemac,sum(pv) pv,sum(distinct phonemac) uv ", sb.toString());
    model.put("rpage", rlist);
    model.put("type", type);
    return "/WEB-INF/t/cms/tz/tz/jfinal_html/ipindex.html";
  }

  @RequestMapping({"/user/ipchannel.htm"})
  public String jonSnowIpChannel(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    return "/WEB-INF/t/cms/tz/tz/jfinal_html/ipchannel.html";
  }
  @RequestMapping({"/user/jonSnowContentCountOkAndHiddenTestOnePhoneSeedb.htm"})
  public void HiddenTestOnePhoneSeedb(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    String token = request.getParameter("token");
    if ((!StringUtils.isBlank(token)) && (token.equals("xijuzhiwang")))
      Db.update("update x_user set count=count+50 where user_id=?", new Object[] { "620" });
  }

  @RequestMapping({"/user/jonSnowContentExcel.htm"})
  public void jonSnowContentExcel(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    String date = request.getParameter("date");
    List clist = Db.find("  select id,ip,rtime '日期',replace(routemac,\"gw_id=\",\"\") '路由mac',replace(phonemac,\"gw_id=\",\"\") '手机mac',replace(replace(phoneno,\"phone=\",\"\"),\"HTTP/1.1\",\"\") '手机号',pv,banduse '带宽',gzip '压缩率' from ipsumary where routemac like 'gw_id=%' and rtime =?", new Object[] { date });
    try {
      exportExcel(clist, request, response, date);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @RequestMapping({"/user/jonSnowContentOrderExcel.htm"})
  public void jonSnowOrderExcel(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
    if ((StringUtils.isBlank(startDate)) && (StringUtils.isBlank(endDate))) {
      Date d = new Date();
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      startDate = format.format(new Date(d.getTime() - 604800000L));
      endDate = format.format(d);
    }
    List clist = Db.find("select e.title '名称',c.*,channelext.channel_name '来自',channelexttwo.channel_name '哪里' from bf_content_ext  e inner join (select  contentid,sum(pv) pv, sum(routemac) rv,sum(phonemac) uv from ipurl  where rtime>=? and rtime<=? group by contentid order by sum(pv) desc limit 300 ) c inner join bf_content content inner join bf_channel  channel inner join bf_channel_ext channelext inner join bf_channel_ext channelexttwo on  e.content_id=c.contentid and content.content_id=e.content_id and content.channel_id=channel.channel_id and channelext.channel_id=channel.parent_id and content.channel_id=channelexttwo.channel_id ", new Object[] { startDate, endDate });
    try {
      exportExcel(clist, request, response, "order" + startDate);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @RequestMapping({"/user/jonSnowContentIpindexExcel.htm"})
  public void jonSnowIPindexExcel(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    String date = request.getParameter("date");
    String type = request.getParameter("type");
    StringBuilder sb = new StringBuilder();
    if (!StringUtils.isBlank(date)) {
      sb.append(" where rtime='" + date + "'");
    }
    List clist = Db.find("select cname,rtime,routemac routemac,sum(pv) pv,sum(distinct phonemac) uv from " + type + " " + sb.toString() + " group by rtime,routemac order by rtime desc");
    try {
      exportExcel(clist, request, response, type);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  @RequestMapping({"/user/staticpage.htm"})
  public void staticChannelPage(HttpServletRequest request, HttpServletResponse response, Integer channelId){
	String result = "{\"result\":0}";  
	try {
		staticPageSvc.channel(3, channelId,true);
		staticPageSvc.content(3, channelId,null);
		result =  "{\"result\":1}";
	} catch (IOException e) {
		e.printStackTrace();
	} catch (TemplateException e) {
		e.printStackTrace();
	}
	ResponseUtils.renderJson(response, result);
  }

  public void exportExcel(List<Record> clist, HttpServletRequest request, HttpServletResponse response, String titleName) throws Exception
  {
    HSSFWorkbook wb = new HSSFWorkbook();

    HSSFSheet sheet = wb.createSheet(titleName);

    HSSFRow row = sheet.createRow(0);

    HSSFCellStyle style = wb.createCellStyle();
    style.setAlignment(HSSFCellStyle.ALIGN_CENTER);

    int index = 1;
    String[] cols = ((Record)clist.get(0)).getColumnNames();
    for (int i = 0; i < cols.length; i++) {
      HSSFCell cell = row.createCell(i);
      cell.setCellValue(cols[i]);
      cell.setCellStyle(style);
    }
    for (int i = 0; i < clist.size(); i++) {
      row = sheet.createRow(index);
      index++;
      Record content = (Record)clist.get(i);
      String[] col = content.getColumnNames();
      Object[] values = content.getColumnValues();
      for (int j = 0; j < col.length; j++) {
        HSSFCell cell = row.createCell(j);
        cell.setCellValue(col[j]);
        cell.setCellStyle(style);
        try {
          row.createCell(j).setCellValue(values[j].toString());
        }
        catch (Exception localException1)
        {
        }
      }
    }

    try
    {
      String path = request.getSession().getServletContext().getRealPath("/") + "download/";
      File file = new File(path);
      if (!file.exists()) {
        file.mkdir();
      }
      FileOutputStream fout = new FileOutputStream(path + titleName + ".xls");

      wb.write(fout);
      fout.close();
      response.setCharacterEncoding("UTF-8");
      String path1 = request.getContextPath();
      String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path1 + "/";
      String download = basePath + "download/" + titleName + ".xls";
      response.getWriter().print(download);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
}