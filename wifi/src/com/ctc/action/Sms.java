package com.ctc.action;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import com.ctc.utils.SmsConfig;

public class Sms {
	public static String PHONE_MSG="您的创天下验证码是";
	public static String KEY="captchaSms";
	public static Integer sendSms(HttpServletRequest request,String msg,String phone,String key) throws Exception{
		boolean isAjax=AjaxRequest(request);
		String line="0";
		if(isAjax){
			int viewnum=getViewNum();
			SmsConfig sms=SmsConfig.getInstance();
			String port=sms.getPort();
			String comid=sms.getComid();
			String username=sms.getUsername();
			String pwd=sms.getUserpwd();
			String smsnumber=sms.getSmsnumber();
			/*URL url = new URL(
					"http://jiekou.56dxw.com/sms/HttpInterface.aspx?comid=2281&username=chuangtianxia"
					+ "&userpwd=it2b0b8n&handtel="+phone+"&sendcontent="+URLEncoder.encode(msg,"GBK")+":"+viewnum+""+URLEncoder.encode("【创天下】", "GBK")
					+ "&smsnumber=10690");*/
			URL url=new URL(port+"?comid="+comid+"&username="+username+"&userpwd="+pwd+"&handtel="+phone+
					"&sendcontent="+URLEncoder.encode(msg,"GBK")+":"+viewnum+""+URLEncoder.encode("【创天下】", "GBK")+"&smsnumber="+smsnumber);
					HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
					httpCon.connect();
					BufferedReader in=null;
					in = new BufferedReader(new InputStreamReader(
					httpCon.getInputStream()));
					line = in.readLine();
					System.out.println(" </p>     result:   " + line);
					int i_ret = httpCon.getResponseCode();
					String sRet = httpCon.getResponseMessage();
					System.out.println("sRet   is:   " + sRet);
					System.out.println("i_ret   is:   " + i_ret);
					if("1".equals(line)){
						request.getSession().setAttribute(key, viewnum+"");
					}
		}
			return Integer.parseInt(line);
	}
	public static boolean AjaxRequest(HttpServletRequest request)
    {
		String sheader = request.getHeader("X-Requested-With");
        boolean isAjaxRequest = (sheader != null &&"XMLHttpRequest".equals(sheader)) ? true : false;
        if (isAjaxRequest)   //判断请求是否为ajax
            return true;
        else
            return false;
    }
	public static int getViewNum(){
		int max=50000;
        int min=1000;
        Random random = new Random();
        int viewnum = random.nextInt(max)%(max-min+1) + min;
        return viewnum;
	}
}
