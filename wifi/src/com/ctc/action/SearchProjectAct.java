package com.ctc.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bluefrog.common.web.ResponseUtils;
import com.bluefrog.core.entity.CmsSite;
import com.bluefrog.core.web.util.CmsUtils;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

@Controller
public class SearchProjectAct {

	
	@RequestMapping(value = "/project/search_result.htm")
	public String searchResult(HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		String pname=request.getParameter("pname");
		CmsSite site = CmsUtils.getSite(request);
		String nm=request.getParameter("nm");
		StringBuilder sb=new StringBuilder();
		if(StringUtils.isBlank(nm)){
			nm="1";
		}
		sb.append(" from bf_content c,bf_content_attr a,bf_channel n,bf_content_ext e where c.content_id=a.content_id and a.attr_name='project_type' and c.channel_id=n.channel_id and c.content_id=e.content_id and c.status=2 ");
		if(!StringUtils.isBlank(pname)){
				sb.append(" and a.attr_value like '%"+pname+"%'");
				model.put("pname", pname);
		}
		sb.append(" order by c.top_level desc");
		Page<Record> plist=Db.paginate(Integer.parseInt(nm), 10, "select c.content_id,date_format(c.sort_date,'%Y%m%d') date,e.title_img titleImg,e.content_img contentImg,e.title,e.description,a.attr_value project_type,n.channel_path", sb.toString());
		model.put("plist", plist);
		model.put("resSys", "/r/cms");
		model.put("site", site);
		model.put("plist", plist);
		String requestType = request.getHeader("X-Requested-With");
		if(StringUtils.isBlank(requestType)){
			return "/WEB-INF/t/cms/tz/tz/jfinal_html/project_search.html";
		}else{
			String json=JsonKit.toJson(plist.getList());
			ResponseUtils.renderJson(response, json);
		}
		return null;
	}
	@RequestMapping(value = "/project/search_navi.htm")
	public String searchNavi(HttpServletRequest request,
			HttpServletResponse response, ModelMap model){	
		String searchName=request.getParameter("searchName");
		CmsSite site = CmsUtils.getSite(request);
		model.put("resSys", "/r/cms");
		model.put("site", site);
		String pageNo=request.getParameter("pageNo");
		if(StringUtils.isBlank(pageNo)){
			pageNo="1";
		}
		StringBuilder sb=new StringBuilder();
		sb.append(" from bf_content c,bf_channel n,bf_content_ext e,bf_content_txt t where c.content_id=e.content_id and c.content_id=t.content_id and c.channel_id=n.channel_id and c.status=2");
		if(!StringUtils.isBlank(searchName)){
			sb.append(" and e.title like '%"+searchName+"%'");
		}
		String type=request.getParameter("type");
		if(!StringUtils.isBlank(type)){
			sb.append(" and "+type+"(e.release_date) = "+type+"(now()) ");
		}else{
			type=null;
		}
		Page<Record> page=Db.paginate(Integer.parseInt(pageNo), 5, "select c.content_id ,e.description,e.title,n.channel_path,date_format(c.sort_date,'%Y%m%d') date,e.release_date  ",sb.toString());
		model.put("key", searchName);
		model.put("page", page);
		model.put("type", type);
		return "/WEB-INF/t/cms/tz/tz/jfinal_html/search_navi.html";
	}
}
