package com.ctc.action;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.baidu.ueditor.ActionEnter;
import com.bluefrog.common.image.ImageScale;
import com.bluefrog.common.upload.FileRepository;
import com.bluefrog.common.web.ResponseUtils;
import com.bluefrog.common.web.springmvc.RealPathResolver;
import com.bluefrog.core.entity.CmsSite;
import com.bluefrog.core.entity.Ftp;
import com.bluefrog.core.entity.MarkConfig;
import com.bluefrog.core.web.util.CmsUtils;

@Controller
public class SwfImgUploadAct{

		@RequestMapping(value = "/img/swfimgupload.htm")
		@ResponseBody
		public void imgUpload(String root,Integer uploadNum,
				@RequestParam(value = "Filedata", required = false) MultipartFile file,
				HttpServletRequest request, HttpServletResponse response,
				ModelMap model) throws Exception{
				JSONObject data=imgJson(request, file);
				ResponseUtils.renderText(response, data.toString());
		}
		@Autowired
		protected RealPathResolver realPathResolver;
		@Autowired
		protected ImageScale imageScale;
		@Autowired
		protected FileRepository fileRepository;
		public File mark(MultipartFile file, MarkConfig conf) throws Exception {
			String path = System.getProperty("java.io.tmpdir");
			File tempFile = new File(path, String.valueOf(System
					.currentTimeMillis()));
			file.transferTo(tempFile);
			boolean imgMark = !StringUtils.isBlank(conf.getImagePath());
			if (imgMark) {
				File markImg = new File(realPathResolver.get(conf.getImagePath()));
				imageScale.imageMark(tempFile, tempFile, conf.getMinWidth(), conf
						.getMinHeight(), conf.getPos(), conf.getOffsetX(), conf
						.getOffsetY(), markImg);
			} else {
				imageScale.imageMark(tempFile, tempFile, conf.getMinWidth(), conf
						.getMinHeight(), conf.getPos(), conf.getOffsetX(), conf
						.getOffsetY(), conf.getContent(), Color.decode(conf
						.getColor()), conf.getSize(), conf.getAlpha());
			}
			return tempFile;
		}
		
		@RequestMapping(value = "/img/upload.htm")
		public void imgUploadUE(HttpServletRequest request, HttpServletResponse response,@RequestParam(value = "upfile", required = false) MultipartFile file) throws Exception{
			request.setCharacterEncoding( "utf-8" );
			response.setHeader("Content-Type" , "text/html");
			String rootPath = request.getSession().getServletContext().getRealPath("/");
			String uploadImge=request.getParameter("action");
			String name="";
			if("uploadimage".equals(uploadImge)){
				JSONObject json=imgJson(request, file);
				//{"state": "SUCCESS","title": "1428033215303009745.jpg","original": "123.jpg","type": ".jpg","url": "/ueditor/jsp/upload/image/20150403/1428033215303009745.jpg","size": "9330"}
				JSONObject rjson=new JSONObject();
				rjson.put("state", "SUCCESS");
				rjson.put("title","1224435345345.jpg").put("original", file.getOriginalFilename());
				rjson.put("type", file.getContentType());
				rjson.put("url", json.get("imgUrl"));
				rjson.put("size", file.getSize());
				name=rjson.toString();
			}else{
				 name=new ActionEnter( request, rootPath ).exec();
			}
			response.getWriter().print(name);
		}
		
		public JSONObject imgJson(HttpServletRequest request,MultipartFile file){
			try {
				JSONObject data=new JSONObject();
				CmsSite site = CmsUtils.getSite(request);
				String ctx = request.getContextPath();
				MarkConfig conf = site.getConfig().getMarkConfig();
				Boolean mark = conf.getOn();
				String origName = file.getOriginalFilename();
				String ext = FilenameUtils.getExtension(origName).toLowerCase(
						Locale.ENGLISH);
				String fileUrl = null;
				if (site.getUploadFtp() != null) {
					Ftp ftp = site.getUploadFtp();
					String ftpUrl = ftp.getUrl();
					if (mark) {
						File tempFile = mark(file, conf);
						fileUrl = ftp.storeByExt(site.getUploadPath(), ext,
								new FileInputStream(tempFile));
						tempFile.delete();
					} else {
						fileUrl = ftp.storeByExt(site.getUploadPath(), ext, file
								.getInputStream());
					}
					// 加上url前缀
					fileUrl = ftpUrl + fileUrl;
				} else {
					if (mark) {
						File tempFile = mark(file, conf);
						fileUrl = fileRepository.storeByExt(site.getUploadPath(), ext,
								tempFile);
						tempFile.delete();
					} else {
						fileUrl = fileRepository.storeByExt(site.getUploadPath(), ext,
								file);
					}
				}
				// 加上部署路径
				data.put("imgUrl", fileUrl);
				data.put("origName", origName);
				return data;
			} catch (Exception e) {
				// TODO: handle exception
			}
			return null;
		}
}
