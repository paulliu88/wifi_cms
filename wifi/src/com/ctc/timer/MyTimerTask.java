package com.ctc.timer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.bluefrog.cms.entity.main.Channel;
import com.bluefrog.cms.entity.main.ChannelExt;
import com.bluefrog.cms.entity.main.ChannelTxt;
import com.bluefrog.cms.entity.main.CmsModel;
import com.bluefrog.cms.entity.main.Content;
import com.bluefrog.cms.entity.main.ContentDoc;
import com.bluefrog.cms.entity.main.ContentExt;
import com.bluefrog.cms.entity.main.ContentTxt;
import com.bluefrog.cms.entity.main.ContentType;
import com.bluefrog.cms.manager.main.ChannelMng;
import com.bluefrog.cms.manager.main.ContentMng;
import com.bluefrog.cms.staticpage.StaticPageSvc;
import com.bluefrog.core.entity.CmsSite;
import com.bluefrog.core.entity.CmsUser;
import com.ctc.model.Data;
import com.ctc.model.NewItem;
import com.ctc.utils.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import freemarker.template.TemplateException;

public class MyTimerTask {
	@Autowired
	private ContentMng manager;
	@Autowired
	private ChannelMng channelMng;
	Integer[] cids = new Integer[] { 151, 153, 155, 156, 157, 158, 159, 160,
			161, 162 };
	String[][] rd = new String[][] {
			{ "专题报道", "头条新闻", "今日看点" , "九个头条" },
			{ "娱乐八卦", "网易娱乐", "手机腾讯网娱乐" },
			{ "科技频道", "知乎每日精选", "腾讯科技" },
			{ "财经资讯", "新浪财经", "财神理财小报", "中国企业家", "股市达人精选" },
			{ "汽车频道", "汽车之家", "豪车汇", "车维修网", "名车图片" },
			{ "情爱频道", "亲子频道", "健康频道", "涨姿势", "大姨吗", "前瞻奇闻",
				"心灵咖啡网", "快速问医生", "牛男网-两性", "家庭医生在线" },
			{ "体育频道"},
			{ "段子", "爆笑日常", "每日神帖", "冷笑话精选", "百思不得姐",
				"内涵社", "我们爱讲冷笑话", "挖段子", "暴走漫画", "糗事百科" },
			{ "花瓣美女", "V女郎", "妹子图", "美空模特", "蜂鸟模特",
					"图片网-美女", "GQ美女", "搜麻豆", "车模", "妖色", "阳光正妹"},
			{ "测试控", "星座运程", "命理相学", "风水玄学", "塔罗占卜",
						"佛学禅语", "十二生肖", "血型物语", "每日一禅"}		
	};
	
	public void work() {
		
		try {
			Thread t1=addNews();
				t1.start();
				t1.join();
			/*Thread t2=addMovies();
				t2.start();
				t2.join();*/
			/*Thread t3=addMovieTwo();
				t3.start();
				t3.join();*/
			System.out.println("=============================news end");
			Thread t4=addXiaoshuo();
				t4.start();
				t4.join();
				deleteData();
			System.out.println("=============================xiaoshuo end");
			Thread t5=staticPage();
			t5.start();
			t5.join();
			System.out.println("=============================static page end");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	private Thread addXiaoshuo() {
		// TODO Auto-generated method stub
		Thread t=new Thread(new Runnable() {
							
							@Override
							public void run() {
								System.out.println("=====make xs buffer start");
								HashMap<String, String> idmap = new HashMap<String, String>();
								List<Record> resultlist = Db.find("select attr_value from bf_content_attr where attr_name='xiaoshuo'");
								System.out.println("=====get total " + resultlist.size() + " records");
								for (int i=0; i<resultlist.size(); i++){
									idmap.put(resultlist.get(i).get("attr_value").toString(), "id");
								}
								System.out.println("=====make xs buffer end");
								
								String[] str=new String[]{"都市","玄幻","武侠","游戏","历史","科幻","古代"};
								Integer[] channelId=new Integer[]{182,183,175,184,185,186,187};
								String url="http://m.ireader.com/app/app.php?ca=Api_Hiwifi.Search&p2=104076&p5=16&pageSize=100&keyWord=";
								//String url="http://m.ireader.com/app/app.php?ca=Api_Hiwifi.Hot&p2=104076&p5=16&pageSize=100&keyWord=";
								for (int i = 0; i < str.length; i++) {
									String json=HttpRequest.sendHttp(url+str[i]);
									try {
										JSONArray data=new JSONArray(new JSONObject(json).getString("data"));
										//JSONArray data=new JSONArray(new JSONObject(json).getString("bookList"));
										for (int j = 0; j < data.length(); j++) {
											JSONObject xs=(JSONObject) data.get(j);
											String urllink=xs.getString("url");
											String id=urllink.substring(urllink.indexOf("key=")+4);
											String name=xs.getString("displayBookName");
											String image=xs.getString("picUrl");
											Map map=new HashMap();
											map.put("xiaoshuo", id);
											map.put("origin","ireader");
											map.put("url", urllink);
											map.put("bookAuthor", xs.getString("bookAuthor"));
											map.put("categoryNameV6", xs.getString("categoryNameV6"));
											map.put("bookDescription", xs.getString("bookDescription"));
											//int size=Db.find("select content_id from bf_content_attr where attr_value=? and attr_name=?",id,"xiaoshuo").size();
											map.put("static", "no");
											
											int size = 1;
											String temp = idmap.get(id);
											if (temp != null && temp.equals("id"))
												size = 1;
											else 
												size = 0;
											
											if(size==0){
												manager.save(getContent(1, map), getContentExt(name, image), getContentTxt(), getContentDoc(), channelId[i], 1, false, getUser(), true);
											}
										
										}
									} catch (JSONException e) {
										e.printStackTrace();
									}
									idmap.clear();
								}
							}
						});
		return t;
	}
	public Thread addNews(){
		final String result=HttpRequest.sendPost("http://iphone.myzaker.com/zaker/apps_telecom.php?for=kttech",true);
		 Thread t=new Thread(new Runnable() {
			@Override
			public void run() {
				HashMap<String, String> idmap = new HashMap<String, String>();
				try {
					System.out.println("=====make news buffer start");
					
					List<Record> resultlist = Db.find("select attr_value from bf_content_attr where attr_name='needSaveId'");
					System.out.println("=====get total " + resultlist.size() + " records");
					for (int i=0; i<resultlist.size(); i++){
						idmap.put(resultlist.get(i).get("attr_value").toString(), "id");
					}
					System.out.println("=====make news buffer end");
					Gson gson = new GsonBuilder().create();
					Data data=gson.fromJson(result,Data.class);
					List<NewItem> items=data.getData().getList();
					for (int i = 0; i < rd.length; i++) {
						for (int j = 0; j < rd[i].length; j++) {
							String name=rd[i][j];
							for (NewItem newItem : items) {
								if(newItem.getTitle().equals(name)){
									String json=HttpRequest.sendPost(newItem.getApp_url(),true);
									JSONObject obj=new JSONObject(json);
									JSONArray list=(JSONArray) ((JSONObject)obj.get("data")).get("list");
									for (int k = 0; k < list.length(); k++) {
										JSONObject news=(JSONObject)list.get(k);
										System.out.println(i+name+":"+news.get("url"));
										int cid=cids[i];
										//int size=Db.find("select content_id from bf_content_attr where attr_value=?",news.getString("pk")).size();
										int size = 1;
										String temp = idmap.get(news.getString("pk").toString());
										if (temp != null && temp.equals("id"))
											size = 1;
										else 
											size = 0;
										if(size==0){
											String titleImg="";
											if(!news.isNull("thumbnail_pic")){
												titleImg=news.getString("thumbnail_pic");
											}else if(!news.isNull("thumbnail_pic_m")){
												titleImg=news.getString("thumbnail_pic_m");
											}else if(!news.isNull("thumbnail_pic_s")){
												titleImg=news.getString("thumbnail_pic_s");
											};
											Map map = new HashMap();
											map.put("needSaveId", news.getString("pk"));
											map.put("url", news.getString("url"));
											map.put("origin","zaker");
											map.put("static", "no");
											ContentExt ext=new ContentExt();
											ext.setTitle(news.getString("title"));
											ext.setTitleImg(titleImg);
											SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:ss:mm");//小写的mm表示的是分钟  
											String dstr=news.getString("date");
											Date date=sdf.parse(dstr);  
											ext.setReleaseDate(date);
											manager.save(getContent(1,map), ext, getContentTxt(),
												 getContentDoc(), cid, 1, false, getUser(), true);
										}
									}
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				idmap.clear();
			}
		});
		 return t;
	}
	
	public Thread addMovies(){
		final Integer[] cids=new Integer[]{166,167,168,169,170,171,172,173};
		final String[] movies=new String[]{"电影","电视","娱乐","搞笑","原创","动漫","美女","攻略"};
		Thread t=new Thread(new Runnable() {
			
			@Override
			public void run() {
				for (int i = 0; i < movies.length; i++) {
					String result=HttpRequest.sendHttp("http://kx.56show.com/kuaixiu/openapi/openi/findvideo?kw="+movies[i]+"&page=1&pagesize=100");
					try {
						JSONObject data=new JSONObject(result);
						JSONArray mlist=(JSONArray) ((JSONObject) data.get("data")).get("videolist");
						for (int j = 0; j < mlist.length(); j++) {
							JSONObject movie=(JSONObject) mlist.get(j);
							String title=movie.getString("videotitle");
							String titleImg=movie.getString("slt1");
							String vid=movie.getString("vid");
							String flashUrl=movie.getString("webplayurl");
							String updateTime=movie.getString("updatetime");
							Map map=new HashMap();
							map.put("movieId", vid);
							map.put("falshUrl", flashUrl);
							map.put("origin","kuaixiu");
							map.put("static", "no");
							int size=Db.find("select content_id from bf_content_attr where attr_value=?",vid).size();
							if(size==0){
								manager.save(getContent(1, map), getContentExt(title, titleImg), getContentTxt(), getContentDoc(), cids[i], 1, false, getUser(), true);
							}
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}
				System.out.println("=====================");
			}
		});
		return t;
	}
	
	public Thread addMovieTwo(){
		final String url="http://www.fun.tv/attachment/editor/upload/open_xml/miui_sync_video_all.shtml";
		Thread t=new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					String result=HttpRequest.sendHttp(url);
					JSONArray allurl=new JSONArray(result);
					for (int i = 0; i < allurl.length(); i++) {
						JSONObject data=(JSONObject) allurl.get(i);
						String movieResult=HttpRequest.sendHttp(data.getString("page"));
						JSONArray movieList=new JSONArray(movieResult);
						for (int j = 0; j < movieList.length(); j++) {
								JSONObject movie=(JSONObject) movieList.get(j);
								String name=movie.getString("name");
								name=name.substring(name.indexOf("】")+1);
								String image=movie.getString("posterurl");
								String vid=movie.getString("vid");
								String date=movie.getString("updatetime");
								Map map=new HashMap();
								map.put("movieTwo", vid);
								map.put("origin","fengxing");
								map.put("static", "no");
								String url=movie.getString("playurl");
								url=url.substring(0,url.lastIndexOf("?"))+"/?malliance=2207";
								map.put("url", url);
								Integer channelId = 166;
								if(movie.getString("category").contains("电影")){
									channelId=166;
								}
								if(movie.getString("category").contains("电视")){
									channelId=167;
								}
								if(movie.getString("category").contains("娱乐")){
									channelId=168;
								}
								if(movie.getString("category").contains("搞笑")){
									channelId=169;
								}
								if(movie.getString("category").contains("原创")){
									channelId=170;
								}
								if(movie.getString("category").contains("动漫")){
									channelId=171;
								}
								if(movie.getString("category").contains("美女")){
									channelId=172;
								}
								if(movie.getString("category").contains("攻略")){
									channelId=173;
								}
								int size=Db.find("select content_id from bf_content_attr where attr_value=? and attr_name=?",vid,"movieTwo").size();
								if(size==0){
									manager.save(getContent(1, map),getContentExt(name, image),getContentTxt(),getContentDoc(),channelId, 1, false,getUser(), true);
								}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		return t;
	}
	public Channel getChannel() {
		Channel channel = new Channel();
		/*
		 * CmsModel model=new CmsModel(); model.setId(1);
		 * channel.setModel(model);
		 */
		CmsSite site = new CmsSite();
		site.setId(3);
		channel.setSite(site);

		/*
		 * Channel parent=new Channel(); parent.setId(150);
		 * channel.setParent(parent);
		 */
		channel.setPath(getStringRandom(6));
		return channel;
	}

	
	public ChannelExt getChannelExt(String name, String channelPath) {
		ChannelExt ext = new ChannelExt();
		ext.setName(name);
		ext.setStaticChannel(true);
		ext.setStaticContent(true);
		// /WEB-INF/t/cms/tz/tz/channel/app_wxw.html
		ext.setTplChannel(channelPath);
		return ext;
	}

	public ChannelTxt getChannelTxt() {
		ChannelTxt txt = new ChannelTxt();
		return txt;
	}

	public Content getContent(int modelId,Map map) {
		Content bean = new Content();
		ContentType type = new ContentType();
		type.setId(1);
		bean.setType(type);
		CmsSite site = new CmsSite();
		site.setRelativePath(false);
		site.setId(3);
		site.setStaticIndex(false);
		site.setIndexToRoot(false);
		bean.setSite(site);
		CmsModel model = new CmsModel();
		model.setId(modelId);
		bean.setModel(model);
		
		bean.setAttr(map);
		return bean;
	}

	public ContentExt getContentExt(String title,String titleImg) {
		ContentExt ext = new ContentExt();
		ext.setTitle(title);
		ext.setTitleImg(titleImg);
		return ext;
	}

	public ContentTxt getContentTxt() {
		ContentTxt txt = new ContentTxt();
		return txt;
	}

	public ContentDoc getContentDoc() {
		ContentDoc doc = new ContentDoc();
		return doc;
	}

	public CmsUser getUser() {
		CmsUser user = new CmsUser();
		user.setUsername("admin");
		user.setId(1);
		return user;
	}

	// 生成随机数字和字母,
	public static String getStringRandom(int length) {

		String val = "";
		Random random = new Random();

		// 参数length，表示生成几位随机数
		for (int i = 0; i < length; i++) {

			String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
			// 输出字母还是数字
			if ("char".equalsIgnoreCase(charOrNum)) {
				// 输出是大写字母还是小写字母
				int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;
				val += (char) (random.nextInt(26) + temp);
			} else if ("num".equalsIgnoreCase(charOrNum)) {
				val += String.valueOf(random.nextInt(10));
			}
		}
		return val;
	}
	public String formatDate(Long time){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(new Date(time*1000L));
		return date;
	}
	@Autowired
	private StaticPageSvc staticPageSvc;
	public Thread staticPage(){
		Thread t=new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Integer[] cids=new Integer[]{151,153,155,156,157,158,159,160,161,162,182,183,175,184,185,186,187};
					for (int i = 0; i < cids.length; i++) {
						System.out.println("========static channel:" + cids[i]);
						int count = staticPageSvc.channel(3, cids[i],true);
						Record c=new Record();
						c.set("date", new Date());
						c.set("count", count);
						c.set("type", "channel");
						Db.save("x_count_log",c);
						System.out.println("========static content:" + cids[i]);
						int content = staticPageSvc.content(3, cids[i],null);
						//System.out.println(count);
						Record c1=new Record();
						c.set("date", new Date());
						c.set("count", content);
						c.set("type", "content");
						Db.save("x_count_log",c1);
					}
					
				} catch (IOException e) {
				} catch (TemplateException e) {
				}
				/*
				Db.update("delete from bf_content_txt where  content_id in(select content_id from bf_content where status=3)");
				Db.update("delete from bf_content_topic where  content_id in(select content_id from bf_content where status=3)");
				Db.update("delete from bf_content_channel where content_id in(select content_id from bf_content where status=3)");
				Db.update("delete from bf_content_attr where  content_id in(select content_id from bf_content where status=3)");
				Db.update("delete from bf_content_attachment where  content_id in(select content_id from bf_content where status=3)");
				Db.update("delete from bf_content_ext where  content_id in(select content_id from bf_content where status=3)");
				Db.update("delete from bf_content_check where  content_id in(select content_id from bf_content where status=3)");
				Db.update("delete from bf_content_count where  content_id in(select content_id from bf_content where status=3)");
				Db.update("delete from bf_contenttag where  content_id in(select content_id from bf_content where status=3)");
				Db.update("delete from bf_content where  status=3");*/
			}
		});
		return t;
	}
	public static void main(String[] args) {
		/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(new Date(1433907118*1000L));
		try {
			Date newDate=sdf.parse(date);
			System.out.println(newDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}*/
		String url="http://www.fun.tv/vplay/v-3752473?malliance=2180";
		url=url.substring(0,url.lastIndexOf("?"))+"/?malliance=2207";
		System.out.println(url);
	}
	public void deleteData(){
		Integer[] cid=new Integer[]{151,153,155,156,157,158,159,160,161,162,166,167,168,169,170,171,172,173,182,183,175,184,185,186,187};
		for (int i = 0; i < cid.length; i++) {
			int size=Db.find("select content_id from bf_content where channel_id=? and status=2 order by content_id desc",cid[i]).size();
			if(size>500){
				int num=Db.update("update bf_content set status=3 where content_id in(select * from (select content_id from bf_content where channel_id=? and status=2 order by top_level desc,content_id desc limit 500,"+size+") t)",cid[i]);
				System.out.println(num+"===============================================");
			}
		}
	}
}
