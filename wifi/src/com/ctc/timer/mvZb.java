package com.ctc.timer;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.bluefrog.cms.entity.main.Channel;
import com.bluefrog.cms.entity.main.ChannelExt;
import com.bluefrog.cms.entity.main.ChannelTxt;
import com.bluefrog.cms.entity.main.CmsModel;
import com.bluefrog.cms.entity.main.Content;
import com.bluefrog.cms.entity.main.ContentDoc;
import com.bluefrog.cms.entity.main.ContentExt;
import com.bluefrog.cms.entity.main.ContentTxt;
import com.bluefrog.cms.entity.main.ContentType;
import com.bluefrog.cms.manager.main.ContentMng;
import com.bluefrog.cms.staticpage.StaticPageSvc;
import com.bluefrog.core.entity.CmsSite;
import com.bluefrog.core.entity.CmsUser;
import com.ctc.utils.HttpRequest;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import freemarker.template.TemplateException;

public class mvZb {
	@Autowired
	private ContentMng manager;
		public void updateZb(){
			try {
				Thread bobo=bobo();
				bobo.start();
				bobo.join();
				System.out.println("======================bobo");
				Thread laf=laf();
				laf.start();
				laf.join();
				System.out.println("======================laf");
				Thread staticPage=staticPage();
				staticPage.start();
				staticPage.join();
				System.out.println("======================static");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		public Thread bobo(){
			Thread thread=new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					try {
						Db.update("update  bf_content set status=3 where  channel_id=195 and status =2");
						String url = "http://www.bobo.com/spe-data/api/anchors-hot.htm";
						String result = HttpRequest.sendHttp(url);
						JSONArray arr = new JSONArray(result);
						for (int i = 0; i < arr.length(); i++) {
							JSONObject obj = (JSONObject) arr.get(i);
							Map map = new HashMap();
							String roomId = obj.getString("roomId");
							String name = obj.getString("roomName");
							String image = obj.getString("cover");
							map.put("boboId", roomId);
							map.put("url","http://www.bobo.com/special/liveshare/?ct=276&roomId="+roomId);
							map.put("origin", "bobo");
							Content bean = getContent(1, map);
							map.put("static", "no");
							manager.save(bean, getContentExt(name, image),getContentTxt(), getContentDoc(), 195, 1,false, getUser(), true);
						}
                         System.out.println("===============boboend");
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}

				}
			});
			return thread;
		}
		
		public Thread laf(){
			Thread thread=new Thread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					try {
						Db.update("update  bf_content set status=3 where  channel_id=196 and status =2");
						String url = "http://api.laifeng.com/open/v1/anchors?appId=1000&sign=79550f5b810e73d75279ddc0ac4e4515&pageLen=30&pageNo=1&cpsId=000145";
						String result = HttpRequest.sendHttp(url);
						JSONArray arr = new JSONObject(result).getJSONArray("data");
						Map map;
						for (int i = 0; i < arr.length(); i++) {
							JSONObject obj = (JSONObject) arr.get(i);
							String name = obj.getString("name");
							String cover = obj.getString("cover");
							String online = obj.getString("online");
							String link = obj.getString("link");
							String id = obj.getString("id");
							map = new HashMap();
							map.put("online", online);
							map.put("url", link);
							map.put("laifid", id);
							map.put("origin", "laifeng");
							map.put("static", "no");
								manager.save(getContent(1, map),
										getContentExt(name, cover),
										getContentTxt(), getContentDoc(), 196, 1,
										false, getUser(), true);

						}
						System.out.println("===============lafend");
					} catch (Exception e) {
						// TODO: handle exception1, false,getUser(), true
						e.printStackTrace();
					}

				}
			});
			return thread;
		}
		@Autowired
		private StaticPageSvc staticPageSvc;
		public Thread staticPage(){
			Thread t=new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
							int content = staticPageSvc.content(3, 176,null);
							//System.out.println(count);
							Record c1=new Record();
							c1.set("date", new Date());
							c1.set("count", content);
							c1.set("type", "content");
							Db.save("x_count_log",c1);
							int count = staticPageSvc.channel(3, 176,true);
							Record c=new Record();
							c.set("date", new Date());
							c.set("count", count);
							c.set("type", "channel");
							Db.save("x_count_log",c);
					} catch (IOException e) {
						
					} catch (TemplateException e) {
					}
				}
			});
			return t;
		}
		public Channel getChannel() {
			Channel channel = new Channel();
			/*
			 * CmsModel model=new CmsModel(); model.setId(1);
			 * channel.setModel(model);
			 */
			CmsSite site = new CmsSite();
			site.setId(3);
			channel.setSite(site);

			/*
			 * Channel parent=new Channel(); parent.setId(150);
			 * channel.setParent(parent);
			 */
			return channel;
		}

		
		public ChannelExt getChannelExt(String name, String channelPath) {
			ChannelExt ext = new ChannelExt();
			ext.setName(name);
			ext.setStaticChannel(true);
			ext.setStaticContent(true);
			// /WEB-INF/t/cms/tz/tz/channel/app_wxw.html
			ext.setTplChannel(channelPath);
			return ext;
		}

		public ChannelTxt getChannelTxt() {
			ChannelTxt txt = new ChannelTxt();
			return txt;
		}

		public Content getContent(int modelId,Map map) {
			Content bean = new Content();
			ContentType type = new ContentType();
			type.setId(1);
			bean.setType(type);
			CmsSite site = new CmsSite();
			site.setRelativePath(false);
			site.setId(3);
			site.setStaticIndex(false);
			site.setIndexToRoot(false);
			bean.setSite(site);
			CmsModel model = new CmsModel();
			model.setId(modelId);
			bean.setModel(model);
			
			bean.setAttr(map);
			return bean;
		}

		public ContentExt getContentExt(String title,String titleImg) {
			ContentExt ext = new ContentExt();
			ext.setTitle(title);
			ext.setTitleImg(titleImg);
			return ext;
		}

		public ContentTxt getContentTxt() {
			ContentTxt txt = new ContentTxt();
			return txt;
		}

		public ContentDoc getContentDoc() {
			ContentDoc doc = new ContentDoc();
			return doc;
		}

		public CmsUser getUser() {
			CmsUser user = new CmsUser();
			user.setUsername("admin");
			user.setId(1);
			return user;
		}
}
