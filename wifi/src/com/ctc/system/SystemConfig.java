package com.ctc.system;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.ctc.model.JfinalContent;
import com.ctc.model.JfinalUser;
import com.ctc.utils.SmsConfig;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class SystemConfig extends JFinalConfig{

	@Override
	public void configConstant(Constants me) {
		// TODO Auto-generated method stub
		loadPropertyFile("jfinal_jdbc.properties");
		Properties p=new Properties();
		InputStream stream=Thread.currentThread().getContextClassLoader().getResourceAsStream("sms.properties");  
		try {
			p.load(stream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String port=p.getProperty("port");
		String comid=p.getProperty("comid");
		String username=p.getProperty("username");
		String userpwd=p.getProperty("userpwd");
		String smsnumber=p.getProperty("smsnumber");
		SmsConfig.getInstance().setPort(port).setComid(comid).setUsername(username).setUserpwd(userpwd).setSmsnumber(smsnumber);
	}

	@Override
	public void configHandler(Handlers me) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void configInterceptor(Interceptors me) {
		// TODO Auto-generated method stub
	}

	@Override
	public void configPlugin(Plugins me) {
		// TODO Auto-generated method stub
		// 配置C3p0数据库连接池插件\
		System.out.println(getProperty("jdbcUrl"));
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbcUrl"), getProperty("user"), getProperty("password").trim(),"com.mysql.jdbc.Driver");
		me.add(c3p0Plugin);
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		me.add(arp);
		arp.addMapping("bf_user", "user_id",JfinalUser.class);
		arp.addMapping("bf_content", "content_id",JfinalContent.class);
		
	}

	@Override
	public void configRoute(Routes me) {
	}
	public static void main(String[] args) {
		InputStream in=Thread.currentThread().getContextClassLoader().getResourceAsStream("jfinal_jdbc.properties");
		System.out.println(in);
	}
}
