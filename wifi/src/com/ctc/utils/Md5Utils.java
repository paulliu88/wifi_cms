package com.ctc.utils;


import java.security.MessageDigest;

public class Md5Utils {

	public static String md5(String str) {
		MessageDigest md = null;
		String signature = "";
		try {
			md = MessageDigest.getInstance("MD5");
			byte[] results = md.digest(str.getBytes("UTF-8"));
			for (int i = 0; i < results.length; i++)
				signature = signature + byteToHex(results[i], 2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return signature;
	}

	private static String byteToHex(byte value, int minlength) {
		String s = Integer.toHexString(value & 0xff);
		if (s.length() < minlength) {
			for (int i = 0; i < (minlength - s.length()); i++)
				s = "0" + s;
		}
		return s;
	}

}
