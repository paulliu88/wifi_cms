package com.ctc.utils;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;



public class Api {
	/*public static void main(String[] args) {
        Api api = new Api();
        String httpReponse =  api.smsSend(null);
         try {
            JSONObject jsonObj = new JSONObject( httpReponse );
            int error_code = jsonObj.getInt("error");
            String error_msg = jsonObj.getString("msg");
            if(error_code==0){
                System.out.println("Send message success.");
            }else{
                System.out.println("Send message failed,code is "+error_code+",msg is "+error_msg);
            }
        } catch (JSONException ex) {
            //Logger.getLogger(Api.class.getName()).log(Level.SEVERE, null, ex);
        }

        httpReponse =  api.testStatus();
        try {
            JSONObject jsonObj = new JSONObject( httpReponse );
            int error_code = jsonObj.getInt("error");
            if( error_code == 0 ){
                int deposit = jsonObj.getInt("deposit");
                System.out.println("Fetch deposit success :"+deposit);
            }else{
                String error_msg = jsonObj.getString("msg");
                System.out.println("Fetch deposit failed,code is "+error_code+",msg is "+error_msg);
            }
        } catch (JSONException ex) {
            //Logger.getLogger(Api.class.getName()).log(Level.SEVERE, null, ex);
        }


    }*/

    public static String smsSend(HttpServletRequest request,String phone,int type){
        // just replace key here
        Client client = Client.create();
        client.addFilter(new HTTPBasicAuthFilter(
            "api","key-ebc5519d34c4cbb8fb83252695ac950c"));
        WebResource webResource = client.resource(
            "http://sms-api.luosimao.com/v1/send.json");
        MultivaluedMapImpl formData = new MultivaluedMapImpl();
        formData.add("mobile", phone);
        int code=getViewNum();
        String msg="绑定手机号，验证码："+code+"。今日门户，Wi-Fi上网第一门户【今日门户】";
        if(type==2){
        	   msg="验证码："+code+"。今日门户，Wi-Fi上网第一门户。【今日门户】";
        }
        
        formData.add("message", msg);
        ClientResponse response =  webResource.type( MediaType.APPLICATION_FORM_URLENCODED ).
        post(ClientResponse.class, formData);
        String textEntity = response.getEntity(String.class);
        int status = response.getStatus();
        //System.out.print(textEntity);
        if(status==200){
        	request.getSession().setAttribute("usercode",code+"");
        }
        return textEntity;
    }
    public static int getViewNum(){
		int max=50000;
        int min=1000;
        Random random = new Random();
        int viewnum = random.nextInt(max)%(max-min+1) + min;
        return viewnum;
	}
    private String testStatus(){
        Client client = Client.create();
        client.addFilter(new HTTPBasicAuthFilter(
            "api","key-d609b769db914a4d959bae3414ed1f7X"));
        WebResource webResource = client.resource( "http://sms-api.luosimao.com/v1/status.json" );
        MultivaluedMapImpl formData = new MultivaluedMapImpl();
        ClientResponse response =  webResource.get( ClientResponse.class );
        String textEntity = response.getEntity(String.class);
        int status = response.getStatus();
        //System.out.print(status);
        //System.out.print(textEntity);
        return textEntity;
    }
}
