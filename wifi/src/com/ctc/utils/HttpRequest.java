package com.ctc.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;




public class HttpRequest {
	
	public static String sendPost(String url,boolean state)  {
		StringBuilder result = new StringBuilder(); //用来接受返回值
        URL httpUrl = null; //HTTP URL类 用这个类来创建连接
        URLConnection connection = null; //创建的http连接
        BufferedReader bufferedReader = null; //接受连接受的参数
        try {
        	//创建URL
        	httpUrl = new URL(url);
        	//建立连接
        	connection = httpUrl.openConnection();
        	connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        	connection.connect();
        	//接受连接返回参数
        	if(state){
        		bufferedReader = new BufferedReader(new InputStreamReader(new GZIPInputStream(connection.getInputStream()),"UTF-8"));
        	}else{
        		bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
        	}
        	String line;
        	while ((line = bufferedReader.readLine()) != null) {
        		result.append(line);
        	}
        	bufferedReader.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
        return result.toString();
}
	public static String sendHttp(String url){
		 // 创建HttpClient实例     
        HttpClient httpclient = new DefaultHttpClient();  
        String result="";
     // 创建Get方法实例     
        try {
        	HttpGet httpgets = new HttpGet(url);    
        	HttpResponse response = httpclient.execute(httpgets);    
        	HttpEntity entity = response.getEntity();    
        	if (entity != null) {    
        		InputStream instreams = entity.getContent();    
        		String str = convertStreamToString(instreams);  
        		result=str;
        		httpgets.abort();    
        	}  
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}
	public static String convertStreamToString(InputStream is) throws UnsupportedEncodingException {      
        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"UTF-8"));      
        StringBuilder sb = new StringBuilder();      
       
        String line = null;      
        try {      
            while ((line = reader.readLine()) != null) {  
                sb.append(line + "\n");      
            }      
        } catch (IOException e) {      
            e.printStackTrace();      
        } finally {      
            try {      
                is.close();      
            } catch (IOException e) {      
               e.printStackTrace();      
            }      
        }      
        return sb.toString();      
    }  
		public static void main(String[] args) {
			String result = null;
				result = sendHttp("http://kx.56show.com/kuaixiu/openapi/openi/findvideo?kw=电影&page=1&pagesize=20");
			System.out.println(result);
		}
}
