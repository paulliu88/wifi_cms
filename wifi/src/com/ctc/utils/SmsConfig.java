package com.ctc.utils;

public class SmsConfig {
			private SmsConfig(){}
			
			private static SmsConfig single=null;
		    //静态工厂方法   
		    public static SmsConfig getInstance() {  
		         if (single == null) {    
		        	 single = new SmsConfig();  
		         }    
		        return single;  
		    }  
		    
		    private String port;
		    private String comid;
		    private String username;
		    private String userpwd;
		    private String smsnumber;
			public String getPort() {
				return port;
			}

			public SmsConfig setPort(String port) {
				this.port = port;
				return single;
			}

			public String getComid() {
				return comid;
			}

			public SmsConfig setComid(String comid) {
				this.comid = comid;
				return single;
			}

			public String getUsername() {
				return username;
			}

			public SmsConfig setUsername(String username) {
				this.username = username;
				return single;
			}

			public String getUserpwd() {
				return userpwd;
			}

			public SmsConfig setUserpwd(String userpwd) {
				this.userpwd = userpwd;
				return single;
			}

			public String getSmsnumber() {
				return smsnumber;
			}

			public SmsConfig setSmsnumber(String smsnumber) {
				this.smsnumber = smsnumber;
				return single;
			}
			
}
