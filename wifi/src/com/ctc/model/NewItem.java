package com.ctc.model;

public class NewItem {

		private int pk;
		private String title;
		private String icon;
		private String api_url;
		private String url;
		public int getPk() {
			return pk;
		}
		public void setPk(int pk) {
			this.pk = pk;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getIcon() {
			return icon;
		}
		public void setIcon(String icon) {
			this.icon = icon;
		}
		public String getApp_url() {
			return api_url;
		}
		public void setApp_url(String app_url) {
			this.api_url = app_url;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		
}
