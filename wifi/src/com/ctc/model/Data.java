package com.ctc.model;



public class Data {
	private int stat;
	private DataList data;
	public int getStat() {
		return stat;
	}
	public void setStat(int stat) {
		this.stat = stat;
	}
	public DataList getData() {
		return data;
	}
	public void setData(DataList data) {
		this.data = data;
	}
	
}
