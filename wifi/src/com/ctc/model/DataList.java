package com.ctc.model;

import java.util.List;

public class DataList {
	private List<NewItem> list;

	public List<NewItem> getList() {
		return list;
	}

	public void setList(List<NewItem> list) {
		this.list = list;
	}
	
	
}
